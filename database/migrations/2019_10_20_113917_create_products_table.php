<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('cat_id')->unsigned();
            $table->string('product_code');
            $table->text('image1');
            $table->text('image2')->nullable();
            $table->text('description');
            $table->integer('product_stock')->unsigned()->default(0);
            $table->integer('minimum_weight')->unsigned()->default(0);
            $table->foreign('cat_id')->references('id')->on('productcategories')->onDelete('cascade');
            
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
