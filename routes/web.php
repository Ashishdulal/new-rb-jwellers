<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::fallback(function() {
    return view('nopage');
});

Route::get('/test', function () {
    return view('test');
});

Route::get('/clear-session', 'MainPageController@clearSession');

Route::get('/', 'MainPageController@index');
Route::get('/collections', 'MainPageController@collection');
Route::get('/products/all', 'MainPageController@allProductsPage');

Route::get('/allevents', 'MainPageController@totalEvents');
Route::get('/allethnics', 'MainPageController@totalEthnics');

Route::get('/ethnic/{id}', 'MainPageController@allProductsByEthnics');
Route::get('/ethnics/{id}', 'MainPageController@totalEthnicProduct');

Route::get('/products/{id}', 'MainPageController@productsPage');
Route::get('/product-detail/{id}', 'MainPageController@productsDetail');
Route::get('/about', 'MainPageController@aboutUs');
Route::get('/blogs', 'MainPageController@blogsPage');
Route::get('/blog-cat/{id}', 'MainPageController@blogsCategory');
Route::get('/blog-detail/{id}', 'MainPageController@blogsDetail');
Route::get('/contact', 'MainPageController@contact');
Route::get('/privacy-policy', 'MainPageController@privacyPolicy');
Route::get('/privacy-policy', 'MainPageController@privacyPolicy');
Route::get('/return-policy', 'MainPageController@returnPolicy');
Route::get('/terms-condition', 'MainPageController@termsCondition');


Route::post('/manyCart', 'MainPageController@manyCartPage');
Route::get('/cart/clear', 'MainPageController@clearCarts');
Route::post('/cart', 'MainPageController@cartPage');
Route::get('/cart', 'MainPageController@viewCartPage');
Route::post('/cart/update/{id}', 'MainPageController@updateCart');
Route::get('/cart/remove/{id}', 'MainPageController@removeCart');

Route::post('/custom-order/{id}', 'SendMailController@customOrder');

Route::get('/checkoutLogin','CheckoutController@redirectLogin');

Route::get('/user','HomeController@userIndex');
Route::get('/user/edit/{id}','HomeController@userUpdate');
Route::get('/user/order-history','HomeController@orderHistory');
Route::get('/user/profile','HomeController@userProfile');
Route::post('/user/changePassword/{id}','HomeController@passwordUpdate')->name('change.password');

// Route::get('/login1', function () {
//     return view('homepage.login');
// });
// Route::get('/signup1', function () {
//     return view('homepage.signup');
// });
// Route::get('/carts', function () {
//     return view('homepage.cart');
// });
Route::get('/checkout', 'CheckoutController@checkout')->name('add_to_checkout');

Route::post('/checkout/confirm', 'SendMailController@store');
Route::get('/weight-search/', 'MainPageController@weightSearch');
Route::get('/weight-verify/', 'MainPageController@weightVerify');

// paypal routes

Route::post('/checkout/payPal', 'PaymentController@payWithPaypal');
Route::get('/paypal-checkout-status','PaymentController@getPaymentStatus')->name('status');
// ends paypal routes



Auth::routes();

Route::get('/sendsms', 'SendMailController@sendSms');

Route::get('/susbcriber', 'SendMailController@subscribe');

Route::group(['middleware' => ['auth', 'admin']], function() {

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/sliders', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');

Route::get('/home/product-category', 'ProductcategoryController@index');
// Route::get('/home/product-category/create', 'ProductcategoryController@create');
Route::post('/home/product-category/create', 'ProductcategoryController@store');
Route::get('/home/product-category/edit/{id}', 'ProductcategoryController@edit')->name('product-category.edit');
Route::post('/home/product-category/edit/{id}', 'ProductcategoryController@update')->name('product-category.update');
Route::delete('/home/product-category/destroy/{id}', 'ProductcategoryController@destroy')->name('product-category.delete');

Route::get('/home/products', 'ProductController@index');
Route::get('/home/product/create', 'ProductController@create');
Route::post('/home/product/create', 'ProductController@store');
Route::get('/home/product/edit/{id}', 'ProductController@edit')->name('product.edit');
Route::post('/home/product/edit/{id}', 'ProductController@update')->name('product.update');
Route::delete('/home/product/destroy/{id}', 'ProductController@destroy')->name('product.delete');

Route::get('/home/menu', 'MenuController@index');
Route::get('/home/menu/create', 'MenuController@create');
Route::post('/home/menu/create', 'MenuController@store');
Route::get('/home/menu/edit/{id}', 'MenuController@edit');
Route::post('/home/menu/edit/{id}', 'MenuController@update');
Route::get('/home/menu/delete/{id}', 'MenuController@destroy');

Route::get('/home/ethnicity', 'EthnicityController@index');
// Route::get('/home/ethnicity/create', 'EthnicityController@create');
Route::post('/home/ethnicity/create', 'EthnicityController@store');
Route::get('/home/ethnicity/edit/{id}', 'EthnicityController@edit')->name('ethnicity.edit');
Route::post('/home/ethnicity/edit/{id}', 'EthnicityController@update')->name('ethnicity.update');
Route::delete('/home/ethnicity/delete/{id}', 'EthnicityController@destroy')->name('ethnicity.delete');

Route::get('/home/events', 'EventsController@index');
// Route::get('/home/event/create', 'EventsController@create');
Route::post('/home/event/create', 'EventsController@store');
Route::get('/home/event/edit/{id}', 'EventsController@edit')->name('event.edit');
Route::post('/home/event/edit/{id}', 'EventsController@update')->name('event.update');
Route::delete('/home/event/delete/{id}', 'EventsController@destroy')->name('event.delete');

Route::get('/home/testimonials', 'TestimonialController@index');
Route::get('/home/testimonial/create', 'TestimonialController@create');
Route::post('/home/testimonial/create', 'TestimonialController@store');
Route::get('/home/testimonial/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/home/testimonial/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/home/testimonial/delete/{id}', 'TestimonialController@destroy')->name('testimonial.delete');

Route::get('/home/blog-category', 'BlogCategoryController@index');
// Route::get('/home/blog-category/create', 'BlogCategoryController@create');
Route::post('/home/blog-category/create', 'BlogCategoryController@store');
Route::get('/home/blog-category/edit/{id}', 'BlogCategoryController@edit')->name('blog-category.edit');
Route::post('/home/blog-category/edit/{id}', 'BlogCategoryController@update')->name('blog-category.update');
Route::delete('/home/blog-category/destroy/{id}', 'BlogCategoryController@destroy')->name('blog-category.delete');

Route::get('/home/blogs', 'BlogController@index');
Route::get('/home/blog/create', 'BlogController@create');
Route::post('/home/blog/create', 'BlogController@store');
Route::get('/home/blog/edit/{id}', 'BlogController@edit')->name('blog.edit');
Route::post('/home/blog/edit/{id}', 'BlogController@update')->name('blog.update');
Route::delete('/home/blog/destroy/{id}', 'BlogController@destroy')->name('blog.delete');

Route::get('/home/productVideos', 'ProductVideoController@index');
// Route::get('/home/productVideo/create', 'ProductVideoController@create');
Route::post('/home/productVideo/create', 'ProductVideoController@store');
Route::get('/home/productVideo/edit/{id}', 'ProductVideoController@edit')->name('productVideo.edit');
Route::post('/home/productVideo/edit/{id}', 'ProductVideoController@update')->name('productVideo.update');
Route::delete('/home/productVideo/delete/{id}', 'ProductVideoController@destroy')->name('productVideo.delete');
Route::delete('/home/productVideo/deleteall', 'ProductVideoController@destroyAll');

});

Route::get('/stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');

