<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomOrder extends Model
{
    protected $fillable=[
    	'name','phone','mail','weight','size','product_name',
    ];
}
