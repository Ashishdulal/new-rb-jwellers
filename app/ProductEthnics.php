<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductEthnics extends Model
{
    protected $fillable=['product_id','ethnic_id'];


    public function product(){
        return $this->belongsTo('App\Product');
    }
}
