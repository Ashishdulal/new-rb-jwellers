<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
    	'name', 'cat_id', 'eth_id', 'event_id', 'product_code', 'image1', 'image2', 'description', 'product_stock', 'featured',
    ];

    public function productEthnics(){
        return $this->hasMany('App\ProductEthnics');
    }
}
