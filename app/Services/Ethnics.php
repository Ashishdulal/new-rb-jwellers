<?php 
namespace App\Services;
use App\ethnicity;
use App\events;
use App\ProductEthnics;
use App\ProductEvents;
use App\EventsEthnics;



class Ethnics{
	public function findCheckedEthnics($event_id,$ethnic_id){

		$data=EventsEthnics::where('event_id',$event_id)
			->where('ethnic_id',$ethnic_id)->first();

	
			if(isset($data)){
				return 'checked';
			}else{
				return 'unchecked';
			}

			
	}

	public function findCheckedProducts($product_id,$ethnic_id){
		$data=ProductEthnics::where('product_id',$product_id)
		->where('ethnic_id',$ethnic_id)->first();
		if(isset($data)){
				return 'checked';
			}else{
				return 'unchecked';
			}
	}

	public function findCheckedEvents($product_id,$event_id){
		$data=ProductEvents::where('product_id',$product_id)
		->where('event_id',$event_id)->first();
		if(isset($data)){
				return 'checked';
			}else{
				return 'unchecked';
			}
	}
}

?>