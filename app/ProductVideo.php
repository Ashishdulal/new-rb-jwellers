<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVideo extends Model
{
    protected $fillable=[
    	'product_id','video_name','video','video_description'
    ];
}
