<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendMail extends Model
{
    protected $fillable=[
        'product_id', 'product_quantity', 'phone', 'product_price', 'email', 'first_name', 'last_name', 'address', 'apartment', 'city', 'postal_code',
    ];
}
