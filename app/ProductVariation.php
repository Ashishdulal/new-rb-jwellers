<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    //
    protected $fillable=['product_id','weight','rate','product_length','product_breadth','product_width','product_diameter'];
}
