<?php

namespace App\Http\Controllers;
use App\blogCategory;
use App\productcategory;
use App\events;
use App\ethnicity;
use App\Product;
use App\ProductVideo;
use App\ProductEthnics;
use App\ProductEvents;
use App\ProductVariation;
use App\slider;
use App\blog;
use App\EventsEthnics;
use Cart;
use Darryldecode\Cart\CartCondition;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainPageController extends Controller
{
    public function clearSession(Request $request)
    {
        $request->session()->flush();
    }
    public function index(Request $request){
        $sliders = slider::latest()->get();
        $allproducts = Product::latest()->get();
        $productCat = productcategory::latest()->get();
        $ornamentProd = DB::table('products')
        ->leftjoin('product_variations','products.id','=','product_variations.product_id')
        ->where('cat_id','=','2')->get();
        $allCat = DB::table('products')
        ->leftjoin('productcategories','products.cat_id','=','productcategories.id')
        ->select("productcategories.id","productcategories.name")
        ->groupBy('id','name')
        ->get();
        $allWeight = DB::table('products')
        ->leftjoin('product_variations','products.id','=','product_variations.product_id')
        ->select("products.id","weight","rate")
        ->groupBy('id','weight','rate')
        ->get();
        $allethenticity = ethnicity::latest()->get();
        $allEvent = events::all();
        $allEvents = events::all();
        $featuredProduct = product::where('featured','=','1')->get();
        return view('welcome',compact('sliders','productCat','allproducts','allethenticity','ornamentProd','allEvents','allCat','allWeight','featuredProduct','allEvent'));
    }
    public function collection(){
        $totalProduct = Product::all()->count();
        $productTotal = Product::all();
        $productcat = productcategory::all();
        $cat_count = productcategory::count();
        return view('homepage.collections', compact('productcat','totalProduct','productTotal'));
    }
    public function aboutUs(){
    	return view('homepage.aboutus');
    }
    public function blogsPage(){
        $blogcat = blogCategory::latest()->get();
        $blogs = blog::latest()->get();
        return view ('homepage.blogs',compact('blogcat','blogs'));
    }
    public function blogsCategory($id){
        $blogCatName = blogCategory::findOrFail($id)->name;
        $blogcat = blogCategory::latest()->get();
        $blogs = blog::latest()->get();
        return view ('homepage.blog-cat',compact('blogcat','blogs','blogCatName','id'));
    }
    public function blogsDetail($id){
        $blogs = blog::findOrFail($id);
        $allblogs = blog::latest()->get();
        $blogcategories = blogCategory::latest()->get();
        return view ('homepage.blog-detail',compact('blogs','allblogs','blogcategories'));
    }
    public function contact(){
    	return view ('homepage.contact');
    }
    public function allProductsPage(){
        $allProducts = Product::all();
        $cat_name = 0;
        $allCat = productcategory::latest()->get();
        return view ('homepage.products', compact('allProducts','cat_name','allCat'));
    }
    public function totalEthnics(){
        $allethnics = ethnicity::latest()->get();
        $alleve = DB::table('events_ethnics')
        ->leftjoin('ethnicities','events_ethnics.ethnic_id','=','ethnicities.id')
        ->get();
        return view ('homepage.allethnics',compact('allethnics','alleve'));
    }

    public function totalEvents(){
        $allevents = events::latest()->get();
        $alleth = DB::table('product_events')
        ->leftjoin('products','product_events.product_id','=','products.id')
        ->get();
        return view ('homepage.allevents',compact('allevents','alleth'));
    }

    public function totalEthnicProduct($id){

        $allEvents= DB::table('events')
        ->leftJoin('events_ethnics','events.id','=','events_ethnics.event_id')
        ->select('name','image','events.id')
        ->where('ethnic_id',$id)->get();
        $ethnicName = ethnicity::where('id', $id)->first();
        $alleth = DB::table('product_events')
        ->leftjoin('products','product_events.product_id','=','products.id')
        ->get();
        return view('homepage.ethnicevents', compact('allEvents','ethnicName','alleth'));
    }

        public function allProductsByEthnics($id){
            $allProducts =DB::table('products')
            ->leftJoin('product_events','products.id','=','product_events.product_id')->
            leftjoin('product_variations','products.id','=','product_variations.product_id')
           -> where('product_events.event_id',$id)

            ->select("products.id","name","cat_id","weight","rate","product_code","image1","image2","description","product_stock","product_events.product_id"
                ,"event_id")->get();

            $ethnicity_all = events::where('id', $id)->first();
            $ethnic_name = $ethnicity_all['name'];
            $cat_name = 1;
            $allCat = productcategory::latest()->get();
            return view ('homepage.ethnics', compact('allProducts','cat_name','allCat','ethnic_name','ethnicity_all'));
        }
        public function productsPage($id){
            $allProducts = Product::where('cat_id',$id)->get();
            $cat_name = productcategory::where('id',$id)->first();
            $allCat = productcategory::latest()->get();
        // dd($allProducts);
            return view ('homepage.products', compact('allProducts','id','cat_name','allCat'));
        }
        public function productsDetail($id,Request $request){
            $productVideo = ProductVideo::where('product_id','=',$id)->get();
            $productvarient = ProductVariation::all();
            $product = Product::findOrFail($id);
            $prodEtnicities = DB::table('ethnicities')
                ->leftjoin('product_ethnics','ethnicities.id','=','product_ethnics.ethnic_id')
                ->where('product_ethnics.product_id', $id)->get();
            $prodEvents = DB::table('events')
                ->leftjoin('product_events','events.id','=','product_events.event_id')
                ->where('product_events.product_id', $id)->get();
            $category_name = productcategory::where('id', $product->cat_id)->first();
            $product = Product::findOrFail($id);
            $allProducts = Product::where('cat_id', $category_name->id)->where('id','!=',$id)->get();
            // return($allProducts);

            $priceForFirstWeightInfo = ProductVariation::where('product_id',$id)->get();
            $priceForFirstWeight=$priceForFirstWeightInfo[0]->rate;

            return view ('homepage.product-details',compact('product','productvarient', 'category_name','priceForFirstWeight','allProducts','priceForFirstWeightInfo','prodEtnicities','prodEvents','productVideo'));
        }
        public function weightSearch(Request $request){
           $weight=$request->model_weight;
           $product_id=$request->product_id;
           $productInfo=ProductVariation::where('weight',$weight)->where('product_id',$product_id)->first();
          // $amount=$productInfo->rate;
           return $productInfo;
       }
       public function weightVerify(Request $request){
           $weight=$request->model_weight;
           $product_id=$request->product_id;
           $productInfo=Product::where('id',$product_id)->get();
           $myWeight= $productInfo[0]->minimum_weight;
           return $myWeight;
       }
       public function privacyPolicy(){
         return view ('homepage.privacyPolicy');
     }
     public function returnPolicy(){
         return view ('homepage.return-policy');
     }
     public function termsCondition(){
         return view ('homepage.terms-condition');
     }

     public function viewCartPage() {
        $cartContents = Cart::getContent();
        // dd($cartContents);
        return view ('homepage.cart', compact('cartContents'));
    }

    Public function clearCarts(Request $request){
        Cart::clear();
        $cartContents = Cart::getContent();
        return redirect('/cart');
    }

    public function manyCartPage(Request $request){
        // $request->validate([
        //             'selected_products' => 'required',

        //         ]);
       // return $request->all();
        $products = Product::all();
        $count = sizeof($request->product_id);
        for($i = 0; $i < $count; $i++ ){
            if($request->selected_products[$i]==1){
                $request->validate([
                    'product_id' => 'required',
                    'name' => 'required',
                    'subTotal' => 'required',
                ]);
                $cartItem[] = Cart::add(
                    (int)$request->product_id[$i], 
                    (string)$request->name[$i], 
                    (float)$request->subTotal[$i],
                    1,
                    array(
                        (float)$request->weight[$i],
                        $request->images[$i]
                    )
                );
            }   
        }
        $cartContents = Cart::getContent();
        return view ('homepage.cart', compact('cartContents', 'products'));
    }


    public function cartPage(Request $request){
        $request->validate([
            'weight' => 'required',
            'productPrice' => 'required',
            'product_id' => 'required',
            'namedName' => 'nullable',
            'fontFamily' => 'nullable'
        ]);
        $product = Product::findOrFail($request->product_id);
        $quantity = 1;
        $cartItem = Cart::add(
            $request->product_id, 
            $product->name, 
            $request->productPrice, 
            $quantity,
            array(
                $request->weight,
                $product->image1,
                $request->namedName,
                $request->fontFamily
        ));
        $cartContents = Cart::getContent();
        return view ('homepage.cart', compact('cartContents','product'));
    }

    public function updateCart(Request $request,$id){
        // dd($request);
        $newQty = $request->updates[0];
        $product = Cart::get($id);
        $oldQty = $product->quantity;
        if($newQty > $oldQty){
            $addedQty = $newQty - $oldQty;
            $cartItem = Cart::update(
                $id, 
                array(
                    'quantity' => $addedQty
            ));
        }
        else{
            $deductedQty = $oldQty - $newQty ;
            $cartItem = Cart::update(
                $id, 
                array(
                    'quantity' => -$deductedQty
            ));
        }
        $cartContents = Cart::getContent();
        return view ('homepage.cart', compact('cartContents'));
    }

    public function removeCart($id){
        Cart::remove($id);
        $cartContents = Cart::getContent();
        return redirect()->back();


    }

}
