<?php

namespace App\Http\Controllers;

use App\productcategory;
use App\events;
use App\ethnicity;
use App\Product;
use App\ProductEthnics;
use App\ProductEvents;
use App\ProductVariation;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evtProduct = ProductEvents::all();
        $events = events::latest()->get();
        $ethnicities = ethnicity::latest()->get();
        $ethProduct = ProductEthnics::all();
        $productvarient = ProductVariation::all();
        $productcat = productcategory::latest()->get();
        $products = Product::latest()->get();
        return view ('dashboard.product.index',compact('products','productcat','productvarient','ethProduct','ethnicities','events','evtProduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ethnicities = ethnicity::latest()->get();
        $events = events::latest()->get();
        $productcat = productcategory::all();
        return view ('dashboard.product.create',compact('productcat','ethnicities','events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->featured == 1){
            Product::query()->update(['featured'=>0]);
        }
         $products = new Product();
         $request->validate([
            'name' => ['required', 'min:3'],
            'description' => ['required', 'min:10'],
            'product_code' => 'required',
            'product_stock' => 'required',
            'cat_id' => 'required',
            'weight' => 'required',
            'minimum_weight' => 'required',
            'rate' => 'required',
            'image1' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $products->cat_id = request('cat_id');
        $products->featured = request('featured');
        $products->minimum_weight = request('minimum_weight');
        $products->name = request('name');
        $products->description = request('description');
        $products->product_stock = request('product_stock');
        $products->product_code = request('product_code');
        
        if(file_exists($request->file('image1'))){
            $image1 = "products".time().'.'.$request->file('image1')->getclientOriginalName();
            $location = public_path('uploads/products');
            $request->file('image1')->move($location, $image1);
            $products->image1 = $image1;
        }
        else{
            $products->image1 = 'Placeholder.jpg';
        }
        if(file_exists($request->file('image2'))){
            $image2 = "products".time().'.'.$request->file('image2')->getclientOriginalName();
            $location = public_path('uploads/products');
            $request->file('image2')->move($location, $image2);
            $products->image2 = $image2;
        }
        else{
            $products->image2 = 'Placeholder.jpg';
        }
      $products->save();
      $myProductId = $products->latest()->first()->id;
        $weightVariations= $request->weight;
        $ethnicVariations= $request->ethnicity;
        $eventVariations= $request->event;
        $rateVariations= $request->rate;
        $productLengthVariations = $request->product_length;
        $productBreadthVariations = $request->product_breadth;
        $productWidthVariations = $request->product_width;
        $productDiameterVariations = $request->product_diameter;

        if($ethnicVariations){
        foreach($ethnicVariations as $key=> $eth){

            ProductEthnics::create([
                'product_id'=>$myProductId,
                'ethnic_id'=>$ethnicVariations[$key]
            ]);

        }
    }
        if($eventVariations){
        foreach($eventVariations as $key=> $evt){

            ProductEvents::create([
                'product_id'=>$myProductId,
                'event_id'=>$eventVariations[$key]
            ]);

        }
    }

        foreach($weightVariations as $key=> $wt){

            ProductVariation::create([
                'product_id'=>$myProductId,
                'weight'=>$weightVariations[$key],
                'product_length'=> $productLengthVariations[$key],
                'product_breadth'=> $productBreadthVariations[$key],
                'product_width'=> $productWidthVariations[$key],
                'product_diameter'=> $productDiameterVariations[$key],
                'rate'=>$rateVariations[$key]
            ]);

        }

        return redirect('/home/products')->with('success','New Product Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product,$id)
    {
        $ethnicities = ethnicity::latest()->get();
        $ethProduct = ProductEthnics::all();
        $evtProduct = ProductEvents::all();
        $events = events::latest()->get();
        $productcat = productcategory::all();
        $products = product::findOrFail($id);
        $productvarient = ProductVariation::all();
        return view ('dashboard.product.edit',compact('productcat','ethnicities','events','products','productvarient','ethProduct'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product,$id)
    {
        if ($request->featured == 1){
            Product::query()->update(['featured'=>0]);
        }
        $productEth = ProductEthnics::where('product_id',$id)->delete();
        $productEvt = ProductEvents::where('product_id',$id)->delete();
        $productVar = ProductVariation::where('product_id',$id)->delete();
        $products = Product::findOrfail($id);
         $request->validate([
            'name' => ['required', 'min:3'],
            'description' => ['required', 'min:10'],
            'product_code' => 'required',
            'product_stock' => 'required',
            'minimum_weight' => 'required',
            'cat_id' => 'required',
            'image1' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $products->featured = request('featured');
        $products->cat_id = request('cat_id');
        $products->minimum_weight = request('minimum_weight');
        $products->name = request('name');
        $products->description = request('description');
        $products->product_stock = request('product_stock');
        $products->product_code = request('product_code');
        
        if(file_exists($request->file('image1'))){
            $image1 = "products".time().'.'.$request->file('image1')->getclientOriginalName();
            $location = public_path('uploads/products');
            $request->file('image1')->move($location, $image1);
            $products->image1 = $image1;
        }
        else{
            $products->image1 = $products->image1;
        }
        if(file_exists($request->file('image2'))){
            $image2 = "products".time().'.'.$request->file('image2')->getclientOriginalName();
            $location = public_path('uploads/products');
            $request->file('image2')->move($location, $image2);
            $products->image2 = $image2;
        }
        else{
            $products->image2 = $products->image2;
        }
      $products->save();
        $myProductId = $id;

        $weightVariations= $request->weight;
        $ethnicVariations= $request->ethnicity;
        $eventVariations= $request->event;
        $rateVariations= $request->rate;
        $productLengthVariations = $request->product_length;
        $productBreadthVariations = $request->product_breadth;
        $productWidthVariations = $request->product_width;
        $productDiameterVariations = $request->product_diameter;

        if($ethnicVariations){
        foreach($ethnicVariations as $key=> $eth){

            ProductEthnics::create([
                'product_id'=>$myProductId,
                'ethnic_id'=>$ethnicVariations[$key]
            ]);

        }
    }
    
        if($eventVariations){
        foreach($eventVariations as $key=> $evt){

            ProductEvents::create([
                'product_id'=>$myProductId,
                'event_id'=>$eventVariations[$key]
            ]);

        }
    }

        foreach($weightVariations as $key=> $wt){

            ProductVariation::create([
                'product_id'=>$myProductId,
                'weight'=>$weightVariations[$key],
                'product_length'=> $productLengthVariations[$key],
                'product_breadth'=> $productBreadthVariations[$key],
                'product_width'=> $productWidthVariations[$key],
                'product_diameter'=> $productDiameterVariations[$key],
                'rate'=>$rateVariations[$key]
            ]);

        }
        return redirect('/home/products')->with('success','Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product,$id)
    {
        $products = product::findOrFail($id)->delete();
        return redirect()->back()->with('success',' Product Deleted Successfully. To add a new product, go to Add New product.');
    }
}
