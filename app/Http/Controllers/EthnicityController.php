<?php

namespace App\Http\Controllers;

use App\ethnicity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EthnicityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ethnicities = ethnicity::latest()->get();
        return view ('dashboard.ethnicity.index', compact('ethnicities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ethnicities = new ethnicity();
        $request->validate([
            'name' => 'required',
        ]);
        $ethnicities->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "ethnicity".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads/products/ethnicity');
            $request->file('image')->move($location, $image);
            $ethnicities->image = $image;
        }
        else{
            $ethnicities->image = 'Placeholder.jpg';
        }
        $ethnicities->save();
        return redirect('/home/ethnicity');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\ethnicity  $ethnicity
     * @return \Illuminate\Http\Response
     */
    public function show(ethnicity $ethnicity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ethnicity  $ethnicity
     * @return \Illuminate\Http\Response
     */
    public function edit(ethnicity $ethnicity,$id)
    {
        $ethnicities = ethnicity::findOrfail($id);
        return view ('dashboard.ethnicity.edit',compact('ethnicities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ethnicity  $ethnicity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ethnicity $ethnicity,$id)
    {
        $ethnicities = ethnicity::findOrfail($id);
        $request->validate([
            'name' => 'required',
        ]);
        $ethnicities->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "ethnicity".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads/products/ethnicity');
            $request->file('image')->move($location, $image);
            $ethnicities->image = $image;
        }
        else{
            $ethnicities->image = $ethnicities->image;
        }
        $ethnicities->save();
        return redirect('/home/ethnicity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ethnicity  $ethnicity
     * @return \Illuminate\Http\Response
     */
    public function destroy(ethnicity $ethnicity,$id)
    {
        $ethnicities = ethnicity::findOrFail($id)->delete();
        return redirect()->back();
    }
}
