<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Cart;
use Auth;
use Darryldecode\Cart\CartCondition;


class CheckoutController extends Controller
{
    public function redirectLogin(){
        
        dd('Herer');
    }

    public function checkout(){
        $user = Auth::user();
        $total = Cart::getTotal();
        $allProducts = Product::all();
        $cartContents = Cart::getContent();
        // return $cartContents;
        return view('homepage.checkout', compact('total','cartContents','allProducts','user'));
    }
}
