<?php

namespace App\Http\Controllers;

use App\blogCategory;
use App\blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogcat = blogCategory::latest()->get();
        $blogs = blog::latest()->get();
        return view ('dashboard.blog.index',compact('blogs','blogcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogcat = blogCategory::latest()->get();
        return view ('dashboard.blog.create', compact('blogcat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blogs = new blog();
        $blogs->title = request('title');
        $blogs->cat_id = request('cat_id');
        $blogs->description = request('description');
        $request->validate([
            'title' => ['required', 'min:5'],
            'description' => ['required', 'min:10'],
            'f_image' => 'image|mimes:jpg,png,jpeg|',
            'i_image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        if(file_exists($request->file('f_image'))){
            $f_image = "blogs".time().'.'.$request->file('f_image')->getclientOriginalName();
            $location = public_path('uploads/blogs');
            $request->file('f_image')->move($location, $f_image);
            $blogs->f_image = $f_image;
        }
        else{
            $blogs->f_image = 'default-image.png';
        }
        if(file_exists($request->file('i_image'))){
            $i_image = "blogs".time().'.'.$request->file('i_image')->getclientOriginalName();
            $location = public_path('uploads/blogs');
            $request->file('i_image')->move($location, $i_image);
            $blogs->i_image = $i_image;
        }
        else{
            $blogs->i_image = 'default-image.png';
        }
        $blogs->save();
        return redirect('/home/blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(blog $blog,$id)
    {
        $blogcat = blogCategory::latest()->get();
        $blogs = blog::findOrFail($id);
        return view ('dashboard.blog.edit',compact('blogs','blogcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, blog $blog,$id)
    {
        $blogs = blog::findOrFail($id);
        $blogs->title = request('title');
        $blogs->cat_id = request('cat_id');
        $blogs->description = request('description');
        $request->validate([
            'title' => ['required', 'min:5'],
            'description' => ['required', 'min:10'],
            'f_image' => 'image|mimes:jpg,png,jpeg|',
            'i_image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        if(file_exists($request->file('f_image'))){
            $f_image = "blogs".time().'.'.$request->file('f_image')->getclientOriginalName();
            $location = public_path('uploads/blogs');
            $request->file('f_image')->move($location, $f_image);
            $blogs->f_image = $f_image;
        }
        else{
            $blogs->f_image = $blogs->f_image;
        }
        if(file_exists($request->file('i_image'))){
            $i_image = "blogs".time().'.'.$request->file('i_image')->getclientOriginalName();
            $location = public_path('uploads/blogs');
            $request->file('i_image')->move($location, $i_image);
            $blogs->i_image = $i_image;
        }
        else{
            $blogs->i_image = $blogs->i_image;
        }
        $blogs->save();
        return redirect('/home/blogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogs = blog::findOrFail($id)->delete();
        return redirect()->back();
    }
}
