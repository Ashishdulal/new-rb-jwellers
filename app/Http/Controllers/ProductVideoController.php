<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductVideo;
use Illuminate\Http\Request;

class ProductVideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productVideo = ProductVideo::latest()->get();
        $productcat = Product::all();
        return view('dashboard.product-video.index',compact('productVideo','productcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request->validate([
                'video_name' => 'required',
                'video' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt | max:500000 ',
            ]);
        $videoProds = $request->product_id;
        if($videoProds){
            foreach($videoProds as $key=>$prod){
            $pvideo = new ProductVideo();
            $pvideo->video_name = $request->video_name;
            $pvideo->product_id = $request->product_id[$key];
            if(file_exists($request->file('video'))){
                $video = "pvideo".time().'.'.$request->file('video')->getclientOriginalName();
                $location = public_path('uploads/video');
                $request->file('video')->move($location, $video);
                $pvideo->video = $video;
            }
            else{
                $pvideo->video = $pvideo->latest()->first()->video;
            }        
            $pvideo->save();
        }
        }
        return redirect('/home/productVideos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductVideo  $productVideo
     * @return \Illuminate\Http\Response
     */
    public function show(ProductVideo $productVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductVideo  $productVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductVideo $productVideo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductVideo  $productVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductVideo $productVideo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductVideo  $productVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductVideo $productVideo,$id)
    {
        $pvideo = ProductVideo::findOrFail($id)->delete();
        return redirect()->back();
    }

    public function destroyAll(Request $request,ProductVideo $productVideo)
    {
        $pvideo = ProductVideo::truncate();
        return redirect()->back();
    }
}
