<?php

namespace App\Http\Controllers;

use App\SendMail;
use App\CustomOrder;
use Illuminate\Http\Request;
use App\Traits\sendSMS;
use Cart;
use Auth;
use Darryldecode\Cart\CartCondition;

class SendMailController extends Controller
{
    use sendSMS;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $this->validate($request, [
            'email'=> 'required',
            'first_name'=> 'required',
            'last_name'=> 'required',
            'address'=> 'required',
            'apartment'=> 'required',
            'city'=> 'required',
            'postal_code'=> 'required',
            'phone'=> 'required',
        ]);
        $data = array(
            'email'=> $request->email,
            'first_name'=> $request->first_name,
            'last_name'=> $request->last_name,
            'address'=> $request->address,
            'apartment'=> $request->apartment,
            'city'=> $request->city,
            'postal_code'=> $request->postal_code,
            'user_id'=> $user_id,
            'phone'=> $request->phone
        );
        $cartItems = Cart::getContent();
        $entry = new SendMail();
        foreach($cartItems as $cartItem){
            $entry->product_id = $cartItem->id;
            $entry->product_quantity = $cartItem->quantity;
            $entry->product_price = $cartItem->price;
            $entry->email = $request->email;
            $entry->first_name = $request->first_name;
            $entry->last_name = $request->last_name;
            $entry->address = $request->address;
            $entry->apartment = $request->apartment;
            $entry->city = $request->city;
            $entry->postal_code = $request->postal_code;
            $entry->phone = $request->phone;
            $entry->user_id = $user_id;
            $entry->save();
        }
        session()->put('price', Cart::getTotal());
        session()->put('data', $data);
        return view('stripe');
        // return redirect('http://jwellery.nepgeeks.com#successmessage')->with('success','Thanks for contacting us!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function customOrder(Request $request)
    {
        $this->validate($request, [
            'name'=> 'required',
            'mail'=> 'required',
            'weight'=> 'required',
            'phone'=> 'required',
            'size'=> 'required',
            'product_name'=> 'required',
        ]);
        $data = array(
            'name'=> $request->name,
            'mail'=> $request->mail,
            'weight'=> $request->weight,
            'size'=> $request->size,
            'product_name'=> $request->product_name,
            'phone'=> $request->phone,
        );
        $txt1 = '<html>
        <head>  
        </head>
        <body>
        <p>Hi, This is '. $data['name'] .' </p>
        <p>Email:'. $data['mail'] .'<br><br>Phone:'. $data['phone'] .'</p>
        <p>Weight :'. $data['weight'] .'</p>
        <p>Size:'. $data['size'] .'</p>
        <p>product Name:'. $data['product_name'] .'</p>
        <p>It would be appriciative, if i receive the custom order details soon.</p>
        </body>
        </html>';       

        $to = "sunilmaharjan@gmail.com";
        $subject = "Custom Order Inquiry";

        $headers = "From:jewellery.nepgeeks.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        CustomOrder::create($request->all());
        $this::sendSms($request->product_name,$request->size, $request->weight,$request->phone,$request->name);
        $result=   mail($to,$subject,$txt1,$headers);
        return redirect('/')->with('success','Thanks for contacting us! We will get to you with your order soon.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function sendSms($product_name,$weight,$size,$phone,$name)
    {
        $customer_name = preg_replace('/\s+/', '%20', $name);
        $prod_name = preg_replace('/\s+/', '%20', $product_name);
        $data = array(
            'name'=> $customer_name,
            'weight'=> $weight,
            'size'=> $size ,
            'product_name'=> $prod_name,
            'phone'=> $phone,
        );
        $message =  $data['name'] .'%20has%20made%20custom%20order%20of%20'. $data['product_name'] .'%20with%20weight%20of%20'.$data['weight'].'%20and%20size%20'.$data['size'].'%20We%20will%20reach%20out%20to%20you%20as%20soon%20as%20possible.';
        // dd($message);
        $phone = $phone;
        $this->sendMessage($message);
  }
      public function customerOrder()
    {

        $message ='Thank%20you%20for%20your%20order.%20We%20will%20reach%20out%20to%20you%20as%20soon%20as%20possible.-Sanskar%20Silver';
        // dd($message);
        $this->sendMessage($message);
  }

    function subscribe(Request $request)
  {
    $this->validate($request, [
      'email'  =>  ['required', 'email'],
    ]);

    $sdata = array(
      'email'   =>   $request->email
    );

    $txt2 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is '. $sdata['email'] .'</p>
    <p>I want to subscribe for the newsletter.</p>
    <p>It would be appriciative, if i receive the updates.</p>
    </body>
    </html>';       
    $to = "azizdulal.ad@gmail.com";
    $subject = "Subscription Inquiry";

    $headers = "From:sanskarsilver.com \r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt2,$headers);
    return back()->with('success','You have successfully applied for our Newsletter!');
  }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SendMail $sendMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(SendMail $sendMail)
    {
        //
    }
}
