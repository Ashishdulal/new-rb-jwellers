<?php

namespace App\Http\Controllers;

use App\blogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogcat = blogCategory::all();
        return view('dashboard.blog-category.index', compact('blogcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.blog-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blogcat = new blogCategory();
        $request->validate([
            'name' => 'required',
        ]);
        $blogcat->name = $request->name;
        $blogcat->save();
        return redirect('/home/blog-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\blogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(blogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\blogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(blogCategory $blogCategory,$id)
    {
        $blogcat = blogCategory::findOrFail($id);
        return view('dashboard.blog-category.edit',compact('blogcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\blogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, blogCategory $blogCategory,$id)
    {
        $blogcat = blogCategory::findOrFail($id);
        $request->validate([
            'name' => 'required',
        ]);
        $blogcat->name = $request->name;
        $blogcat->save();
        return redirect('/home/blog-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\blogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogcat = blogCategory::findOrFail($id)->delete();
        return redirect()->back();
    }
}
