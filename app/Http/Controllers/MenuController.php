<?php

namespace App\Http\Controllers;

use App\menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $menu = Menu::all();
        return view('dashboard.menu.index', compact('menu'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('dashboard.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'menuName' => 'required|max:30',
            ));

        $menu = new Menu();
        $menu->menuName = $request->input('menuName');
        if(file_exists($request->image)){
            $filename = 'image'.time().'.'.$request->image->getClientOriginalExtension();
            $location = public_path('images/categoryImages');
            $request->image->move($location, $filename);
            $menu->image = $filename;
        }
        else{
            $menu->image = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $menu->save();
        
        return redirect('/home/menu')->with('Success, ', 'A new menu is added successfully.');
    }
     

    /**
     * Display the specified resource.
     *
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(menu $menu , $id)
    {
         $menu = Menu::findorFail($id);
        return view('dashboard.menu.edit', compact('menu', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);
        $this->validate($request, array(
            'menuName' => 'required|max:30',
            ));

        $menu = Menu::where('id', $id)->first();
        $menu->menuName = $request->input('menuName');
        if(file_exists($request->image)){
            $filename = 'image'.time().'.'.$request->image->getClientOriginalExtension();
            $location = public_path('images/');
            $request->image->move($location, $filename);
            $menu->image = $filename;
        }
        else{
            $menu->image = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $menu->save();
        return redirect('/home/menu')->with('Success, ', 'Menu is updated successfully.');
    }

        
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(menu $menu)
    {
        //
    }
}
