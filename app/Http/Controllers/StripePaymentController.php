<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Stripe;
use App\SendMail;
use App\Traits\sendSMS;
use Cart;
use Darryldecode\Cart\CartCondition;

class StripePaymentController extends Controller
{
    use sendSMS;
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        Stripe\Charge::create ([
                "amount" => session()->get('price') * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from Sanskar." 
        ]);
        $data = session()->get('data');
        $txt1 = '<html>
        <head>  
        </head>
        <body>
        <p>Hi, This is '. $data['first_name'] .' '. $data['last_name'] .'.</p>
        <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
        <p>Address:'. $data['address'] .'</p>
        <p>Apartment:'. $data['apartment'] .'</p>
        <p>City:'. $data['city'] .'</p>
        <p>Postal Code:'. $data['postal_code'] .'</p>
        <p>It would be appriciative, if i receive the order soon.</p>
        </body>
        </html>';       

        $to = "sunilmaharjan@gmail.com";
        $subject = "Payment for Order";

        $headers = "From:jewellery.nepgeeks.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        $product_name = 'demo';
        $size = '22';
        $weight = '21';
        $name = $data['first_name'];
        $message = 'Thank%20you%20for%20your%20order.%20We%20will%20reach%20out%20to%20you%20as%20soon%20as%20possible.-Sanskar%20Silver';
        $this->sendMessage($message);
        Cart::clear();
        $result=   mail($to,$subject,$txt1,$headers);
        Session::flash('success', 'Payment successful!');
        return redirect('http://jwellery.nepgeeks.com#successmessage')->with('success','Payment with Stripe is successfull!!!');
        // return back();
    }
}