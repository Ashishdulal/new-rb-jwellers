@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




  <nav class="breadcrumb" aria-label="breadcrumbs">



    <h1>{{$product->name}}</h1>

    <a href="/" title="Back to the frontpage">Home</a> 
    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>


    <a href="/products/{{$category_name->id}}" title="">{{$category_name->name}}</a>  



    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    <span>{{$product->name}}</span>



  </nav>




  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">

    <div class="grid__item">         
      <div itemscope itemtype="http://schema.org/Product">
        <meta itemprop="url" content="http://jwellery.nepgeeks.com/product-detail/1">  
        <meta itemprop="name" content="{{$product->name}}" />
        <meta itemprop="sku" content="UKL656"/>
        <meta itemprop="gtin14" content=""/>
        <meta itemprop="brand" content="CaratLane"/>
        <meta itemprop="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official."/>
        <meta itemprop="image" content="https://cdn.shopify.com/s/files/1/1811/9385/products/4_grande.jpg?v=1496145230"/>
        <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
          <meta itemprop="priceCurrency" content="USD">
          <meta itemprop="price" content="199.00">
          <meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
          <meta itemprop="availability" content="http://schema.org/InStock"/>
        </div>
      </div>


      <div class="product-single">

        <div class="grid__item wide--one-half post-large--one-half large--one-half product-img-box">
          @if($product->cat_id != 6)
          <div class="product-photo-container">

            <a href="/uploads/products/{{$product->image1}}" >
              <img id="product-featured-image" src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}" data-zoom-image="/uploads/products/{{$product->image1}}"/>
            </a>
<!--          <a href="/uploads/products/{{$product->image2}}" >
                <img id="product-featured-image" src="/uploads/products/{{$product->image2}}" alt="{{$product->name}}" data-zoom-image="/uploads/products/{{$product->image2}}"/>
              </a> -->
          </div>
        @else
        <div class="row">
          <div class="col-sm-2" >
            <div class="sideImages">
              <div class="selectDisplay activeImage">
                <img src="/images/sumanjwellery.png" alt="">
              </div>
              <div class="selectDisplay">
                <img src="/images/JWLN0300.jpg" alt="">
              </div>
              <div class="selectDisplay">
                <img src="/images/JWLN03001.jpg" alt="">
              </div>
              <div class="selectDisplay">
                <img src="/images/JWLN03002.jpg" alt="">
              </div>
              <div class="selectDisplay">
                <img src="/images/JWLN03003.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="col-sm-10 dimension">
            <canvas id="myCanvas" class="mainImages show" width="500" height="500">
              <p>Your browser doesn't support the content! Please switch the browser to view the content.</p>
            </canvas>
            <div class="mainImages hide">
              <img src="/images/JWLN0300.jpg" alt="">
            </div>
            <div class="mainImages hide">
              <img src="/images/JWLN03001.jpg" alt="">
            </div>
            <div class="mainImages hide">
              <img src="/images/JWLN03002.jpg" alt="">
            </div>
            <div class="mainImages hide">
              <img src="/images/JWLN03003.jpg" alt="">
            </div>
          </div>
        </div>
        @endif
      <div class="more-view-wrapper-owlslider">
        <ul id="ProductThumbs" class="product-photo-thumbs owl-carousel owl-theme thumbs">

          <li class="">
            <a href="javascript:void(0)" data-image="/uploads/products/{{$product->image1}}" data-zoom-image="/uploads/products/{{$product->image1}}">
              <img src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}">
            </a>
          </li>
          <li class="">
            <a href="javascript:void(0)" data-image="/uploads/products/{{$product->image2}}" data-zoom-image="/uploads/products/{{$product->image2}}">
              <img src="/uploads/products/{{$product->image2}}" alt="{{$product->name}}">
            </a>
          </li>
          
        </ul>
      </div>
      
      
    </div>

    <div class="product_single_detail_section grid__item wide--one-half post-large--one-half large--one-half">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      @if (Session::has('success'))
      <div class="alert alert-success text-center">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <p>{{ Session::get('success') }}</p>
      </div>
      @endif
      <h2 class="product-single__title">{{$product->name}}</h2>

      <div class="product_single_price">

        <div class="product_price">

          <div class="grid-link__org_price" id="ProductPrice">
            <!-- <span class=money>$199.00</span> -->
          </div>

          
        </div>
      </div>
      <span class="shopify-product-reviews-badge" data-id="10109317582"></span>
      
      <div class="product-description rte" >
        <?php echo ($product->description )?>
      </div>
      @if($product->cat_id == 6)
                    <div class="form-group">
                        <label for="name">Enter the name</label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group imgTag">
                        <label for="">Select the font</label>
                        <div class="imgSelect" data="Cookie, Cursive">
                            <img src="/images/cookie.jpg"  alt="Cookie, Cursive">
                        </div>
                        <div class="imgSelect" data="'Dancing Script', Cursive">
                            <img src="/images/dancingscript.jpg" alt="'Dancing Script', Cursive">
                        </div>
                        <div class="imgSelect" data="'Great Vibes', Cursive">
                            <img src="/images/greatvibes.jpg"  alt="'Great Vibes', Cursive">
                        </div>
                        <div class="imgSelect" data="'Alex Brush', cursive">
                            <img src="/images/alexbrush.jpg" alt="'Alex Brush', cursive">
                        </div>
                    </div>
      @endif


      <div class="product-infor">
      @if($product->cat_id != 6)
        <p class="product-vendor">
          <label>Ethincity:</label>
          @foreach($ethProduct as $etProduct)
          @foreach($ethnicities as $ethnicity)
          @if($ethnicity->id == $etProduct->ethnic_id)
          @if($etProduct->product_id == $product->id)
          <span>   {{$ethnicity->name}}, </span>
          @endif
          @endif
          @endforeach
          @endforeach
        </p>


        <p class="product-type">
          <label>Event: </label>  
          @foreach($evtProduct as $evProduct)
          @foreach($events as $evt)
          @if($evt->id == $evProduct->event_id)
          @if($evProduct->product_id == $product->id)
          <span> {{$evt->name}},</span>
          @else
          @endif
          @endif
          @endforeach
          @endforeach
        </p>
      @endif

        <p class="product-inventory">
          <label>Availability:  </label>              

          @if($product->product_stock == '0')
          <span>  Out Of Stock</span>
          @else
          <span> In Stock </span>
          @endif
        </p>


      </div>




      <form method="post" action="/cart" 
      accept-charset="UTF-8" 
      class="product-form">
      @csrf
      <div class="swatch clearfix">
       <input type="hidden" id="product_id" name="product_id" value="{{$product->id}}">
       <input type="hidden" id="price" name="productPrice" value="{{ $priceForFirstWeight }}">
        <input type="hidden" name="namedName" id="sendName">
        <input type="hidden" name="fontFamily" id="sendFontFamily">
       <div class="header">weight :</div>
       <div class="swatch-section">
        <select name="weight" id="weightSelect" class="" required>
          <!-- <option hidden="">  Select weight </option><span>[ In Tola ]</span> -->
          @foreach($productvarient as $key=> $pvarient)
          @if($pvarient->product_id == $product->id)
          <option  value="{{$pvarient->weight}}">{{$pvarient->weight}}</option>
          @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="selector-wrapper-secton  ">

      <script type="text/javascript">

        $.ajaxSetup({
          headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
        });

        $("#weightSelect").on("change", function () {
          var weight = $(this).val();
          var product_id=document.getElementById("product_id").value;
               // alert(product_id);


                //Here you can use ajax to call php function
                $.ajax({
                  url: '/weight-search/',
                  type: 'GET',
                  dataType: "json",
                  data: {
                    model_weight: weight, product_id: product_id
                  },
                  success: function (ret_data) {
                    //console.log(ret_data);
                    document.getElementsByName('subTotal')[0].value = ret_data;
                    document.getElementsByName('productPrice')[0].value = ret_data;

                  }
                });
              });


            </script>
          </div>          
          <div class="product-single__quantity ">
            <div class="quantity-box-section">
              <!-- <label>Quantity:</label>
              <div class="quantity_width"> 
                <div class="dec button">-</div>  
                <input type="number" id="quantities" name="quantity" value="1" min="1">  
                <div class="inc button">+</div>  
                <p class="min-qty-alert" style="display:none">Minimum quantity should be 1</p>
              </div> -->


              <div class="total-price">
                <label>Price </label>
                <span class="showMoney"></span>
                <input style="border:none;width: 15%;background: transparent;" id="allTotalPrice" type="text" disabled="" value="{{$priceForFirstWeight}}" required class="inc button" name="subTotal">
              </div>
           <!--   <div class="total-price">
              <label>Total </label><span id="totalDeliveryPrice">Rs.</span>
            </div>
          -->
        </div>
      </div>
<!--         <script>

          $(document).ready(function(){
            $qty = $("#quantities").html();
            $price = $("#ProductMoney").html();
            $total_price = $price * $qty;
            $("#totalPrice").html( $total_price);
            $("#total_price").html($total_price);
            $("#totalPriceSent").val($total_price);
            $("#singlePriceSent").val($price);
            $("#totalDeliveryPrice").val(parseInt($("#deliveryPrice").html(),10));
          });

        </script> -->

        @if($product->product_stock != '0')
        <div class="product-single-button">

          <button type="submit" id="AddToCart" class="btn">
            <span id="AddToCartText">Add to Cart</span>
          </button>

          

<!-- 
          <div data-shopify="payment-button" class="shopify-payment-button"><button class="shopify-payment-button__button shopify-payment-button__button--unbranded shopify-payment-button__button--hidden" disabled="disabled" aria-hidden="true"> </button><button class="shopify-payment-button__more-options shopify-payment-button__button--hidden" disabled="disabled" aria-hidden="true"> </button></div>


          <div class="add-to-wishlist">     
            <div class="show">
              <div class="default-wishbutton-classic-prong-set-ring-mount loading"><a class="add-in-wishlist-js btn" href="classic-prong-set-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
              <div class="loadding-wishbutton-classic-prong-set-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="classic-prong-set-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
              <div class="added-wishbutton-classic-prong-set-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
            </div>
          </div>
          <div class="add-to-wishlist">     
           <div class="show">
             <div class="added-wishbutton-classic-prong-set-ring-mount loading"><a class="add-in-wishlist-js btn" href="classic-prong-set-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Custom order</span></a></div>
           </div>
         </div> -->


       </div>
       @endif
       <!-- Trigger the modal with a button -->
          <button style="padding:15px 35px;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myCustomOrder">Custom Order</button>
     </form><br>
<!-- <a href="/sendsms" class="btn btn-info btn-lg">Send Sms</a> -->
          <!-- Modal -->
          <div id="myCustomOrder" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Custom Order</h4>
                </div>
                <div class="modal-body1">
                  <form action="/custom-order/{{$product->id}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="custom-order">
                    <div class="col-md-6 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <p class="card-description"> Please Fill the form to get the custom order inquiry. </p>
                          <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Please enter your name" aria-label="name">
                          </div>
                          <div class="form-group">
                            <label>Phone Number</label>
                            <input type="number" class="form-control form-control-lg" name="phone" placeholder="please enter your phone number" aria-label="phone">
                          </div>
                          <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control form-control-sm" name="mail" placeholder="please enter your email" aria-label="mail">
                          </div>
                          <div class="form-group">
                            <label>Weight [In Tola]</label>
                            <input type="text" class="form-control form-control-lg" name="weight" placeholder="please enter your custom weight" aria-label="weight">
                          </div>
                          <div class="form-group">
                            <label>Size</label>
                            <input type="text" class="form-control form-control-lg" name="size" placeholder="please enter the size" aria-label="size">
                          </div>
                          <input type="hidden" name="product_name" value="{{$product->name}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 grid-margin stretch-card">
                      <div class="product-infor">
                        <div>
                          <img src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}">
                        </div>
                        <p class="product-inventory order-customize"><br>
                          <label>Product Name:  </label> <br>             
                          {{$product->name}}
                        </p>
                        <p class="product-vendor order-customize">
                          <label>Ethincity:</label><br>
                          @foreach($ethProduct as $etProduct)
                          @foreach($ethnicities as $ethnicity)
                          @if($ethnicity->id == $etProduct->ethnic_id)
                          @if($etProduct->product_id == $product->id)
                          <span>   {{$ethnicity->name}}, </span>
                          @endif
                          @endif
                          @endforeach
                          @endforeach
                        </p>


                        <p class="product-type order-customize">
                          <label>Event: </label>  <br>
                          @foreach($evtProduct as $evProduct)
                          @foreach($events as $evt)
                          @if($evt->id == $evProduct->event_id)
                          @if($evProduct->product_id == $product->id)
                          <span> {{$evt->name}},</span>
                          @else
                          @endif
                          @endif
                          @endforeach
                          @endforeach
                        </p>

                        
                      </div>
                    </div>
                 &nbsp;&nbsp; <button type="submit" class="btn btn-primary mr-2">Submit</button>
                  </div>
                  </form>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div>

<!-- 
     <div class="grid__item" style="margin-top:20px;">
      <img src="(image-path)" alt="secure_pay">
    </div> -->



    <div class="share_this_btn">
     <!-- <div class="addthis_toolbox addthis_default_style addthis_32x32_style"  >
      <a class="addthis_button_preferred_1"></a>
      <a class="addthis_button_preferred_2"></a>
      <a class="addthis_button_preferred_3"></a>
      <a class="addthis_button_preferred_4"></a>
      <a class="addthis_button_compact"></a>
      <a class="addthis_counter addthis_bubble_style"></a>
    </div> -->
    <script type='text/javascript'>
      var addthis_product = 'sfy-2.0.2';
      var addthis_plugin_info = {"info_status":"enabled","cms_name":"Shopify","cms_version":null,"plugin_name":"AddThis Sharing Tool","plugin_version":"2.0.2","plugin_mode":"AddThis"};
    var addthis_config     = {/*AddThisShopify_config_begins*/pubid:'xa-525fbbd6215b4f1a', button_style:'style3', services_compact:'', ui_delay:0, ui_click:false, ui_language:'', data_track_clickback:true, data_ga_tracker:'', custom_services:'', custom_services_size:true/*AddThisShopify_config_ends*/};
  </script>
  <script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-525fbbd6215b4f1a'></script>

</div>

<div class="dt-sc-hr-invisible-large"></div>
<div class="dt-sc-tabs-container">
  <ul class="dt-sc-tabs">
    <li><a href="#"> Description </a></li> 
    <li><a href="#"> Reviews  </a></li>
    <li><a href="#"> Shipping details  </a></li>
  </ul>

  <div class="dt-sc-tabs-content" id="desc_pro">
    <p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official.</span></p>
  </div>


  <div class="dt-sc-tabs-content">
    <div class="commentlist">
      <div class="comment-text">
        <div class="rating-review">
          <div id="shopify-product-reviews" data-id="10109317582"></div>
        </div>
      </div>
    </div>
  </div>


  <div class="dt-sc-tabs-content">
    <p>Write a few sentences to tell people about your store's shipping policy (the kind of products you sell, your mission, etc). This tab consists general information for all the prducts. This information is not a product specific but store's.</p>
  </div>

</div>
</div>

</div>
<div class="dt-sc-hr-invisible-large"></div>
<div class="related-products-container">

  <div class="dt-sc-hr-invisible-large"></div>
  <div class="section-header section-header--small">
    <div class="border-title">

     <h2 class="section-header__title">Related Products</h2>  

     <div class="short-desc"> <p>    
      From this Collection
    </p>
  </div>

</div>
</div>
<div class="dt-sc-hr-invisible-small"></div>    
<ul class="grid-uniform grid-link__container related-products">
  <li class="grid__item item-row  " id="product-10109315918" >
    <div class="products">
      <div class="product-container">  
        <a href="/collections/necklaces/products/lush-ring-mount" class="grid-link">    
          <div class="ImageOverlayCa"></div>
          <img src="http:////cdn.shopify.com/s/files/1/1811/9385/products/2_ac2c90d2-fae2-47b5-9c9c-7b73500c454a_large.jpg?v=1496144771" class="featured-image" alt="Lush Ring Mount">
        </a>
        <div class="ImageWrapper">
          <div class="product-button">  

            <a href="javascript:void(0)" id="lush-ring-mount" class="quick-view-text">                      
              <i class="fa fa-expand" aria-hidden="true"></i>
            </a>       
            <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315918">                                    
              <input type="hidden" name="id" value="37510472974" />  
              <a class="add-cart-btn">
               <i class="zmdi zmdi-shopping-cart"></i>
             </a>
           </form>  
           <a href="/products/lush-ring-mount">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          <div class="add-to-wishlist">     
            <div class="show">
              <div class="default-wishbutton-lush-ring-mount loading"><a class="add-in-wishlist-js btn" href="lush-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
              <div class="loadding-wishbutton-lush-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="lush-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
              <div class="added-wishbutton-lush-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="product-detail">
      <a href="/collections/necklaces/products/lush-ring-mount" class="grid-link__title">Lush Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          <div class="grid-link__org_price">
            <span class=money>$620.00</span>
          </div>
        </div>      
        <span class="shopify-product-reviews-badge" data-id="10109315918"></span> 
      </div>
      <ul class="item-swatch color_swatch_Value">  
      </ul>
    </div>
  </div>
</li>

<li class="grid__item item-row  " id="product-10109315918" >
  <div class="products">
    <div class="product-container">  
      <a href="/collections/necklaces/products/lush-ring-mount" class="grid-link">    
        <div class="ImageOverlayCa"></div>
        <img src="http:////cdn.shopify.com/s/files/1/1811/9385/products/2_ac2c90d2-fae2-47b5-9c9c-7b73500c454a_large.jpg?v=1496144771" class="featured-image" alt="Lush Ring Mount">
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  

          <a href="javascript:void(0)" id="lush-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315918">                                    
            <input type="hidden" name="id" value="37510472974" />  
            <a class="add-cart-btn">
             <i class="zmdi zmdi-shopping-cart"></i>
           </a>
         </form>  
         <a href="/products/lush-ring-mount">                      
          <i class="fa fa-external-link" aria-hidden="true"></i>
        </a>       
        <div class="add-to-wishlist">     
          <div class="show">
            <div class="default-wishbutton-lush-ring-mount loading"><a class="add-in-wishlist-js btn" href="lush-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
            <div class="loadding-wishbutton-lush-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="lush-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
            <div class="added-wishbutton-lush-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
          </div>
        </div>


      </div>
    </div>
  </div>
  <div class="product-detail">
    <a href="/collections/necklaces/products/lush-ring-mount" class="grid-link__title">Lush Ring Mount</a>     
    <div class="grid-link__meta">
      <div class="product_price">
        <div class="grid-link__org_price">
          <span class=money>$620.00</span>
        </div>
      </div>      
      <span class="shopify-product-reviews-badge" data-id="10109315918"></span> 
    </div>
    <ul class="item-swatch color_swatch_Value">  
    </ul>
  </div>
</div>
</li>

<li class="grid__item item-row  " id="product-10109315918" >
  <div class="products">
    <div class="product-container">  
      <a href="/collections/necklaces/products/lush-ring-mount" class="grid-link">    
        <div class="ImageOverlayCa"></div>
        <img src="http:////cdn.shopify.com/s/files/1/1811/9385/products/2_ac2c90d2-fae2-47b5-9c9c-7b73500c454a_large.jpg?v=1496144771" class="featured-image" alt="Lush Ring Mount">
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  

          <a href="javascript:void(0)" id="lush-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315918">                                    
            <input type="hidden" name="id" value="37510472974" />  
            <a class="add-cart-btn">
             <i class="zmdi zmdi-shopping-cart"></i>
           </a>
         </form>  
         <a href="/products/lush-ring-mount">                      
          <i class="fa fa-external-link" aria-hidden="true"></i>
        </a>       
        <div class="add-to-wishlist">     
          <div class="show">
            <div class="default-wishbutton-lush-ring-mount loading"><a class="add-in-wishlist-js btn" href="lush-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
            <div class="loadding-wishbutton-lush-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="lush-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
            <div class="added-wishbutton-lush-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="product-detail">
    <a href="/collections/necklaces/products/lush-ring-mount" class="grid-link__title">Lush Ring Mount</a>     
    <div class="grid-link__meta">
      <div class="product_price">
        <div class="grid-link__org_price">
          <span class=money>$620.00</span>
        </div>
      </div>      
      <span class="shopify-product-reviews-badge" data-id="10109315918"></span> 
    </div>
    <ul class="item-swatch color_swatch_Value">  
    </ul>
  </div>
</div>
</li>
<li class="grid__item item-row  " id="product-10109315278" >
  <div class="products">
    <div class="product-container">  
      <a href="/collections/necklaces/products/promise-solitaire-ring-mount" class="grid-link">    
        <div class="ImageOverlayCa"></div>
       <img src="http:////cdn.shopify.com/s/files/1/1811/9385/products/1_5c789c84-aba2-4dbb-8992-69166a320cac_large.jpg?v=1496144078" class="featured-image" alt="Let Promise Solitaire Ring Mount">
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  

          <a href="javascript:void(0)" id="promise-solitaire-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          

          
          <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315278">                                    
            <input type="hidden" name="id" value="37510470862" />  
            <a class="add-cart-btn">
             <i class="zmdi zmdi-shopping-cart"></i>
           </a>
         </form>  



         <a href="/products/promise-solitaire-ring-mount">                      
          <i class="fa fa-external-link" aria-hidden="true"></i>
        </a>       



        <div class="add-to-wishlist">     
          <div class="show">
            <div class="default-wishbutton-promise-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="promise-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
            <div class="loadding-wishbutton-promise-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="promise-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
            <div class="added-wishbutton-promise-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
          </div>
        </div>


      </div>
    </div>
  </div>
  <div class="product-detail">

    <a href="/collections/necklaces/products/promise-solitaire-ring-mount" class="grid-link__title">Let Promise Solitaire Ring Mount</a>     
    <div class="grid-link__meta">
      <div class="product_price">
        <div class="grid-link__org_price">
          <span class=money>$590.00</span>
        </div>  
      </div>      
      <span class="shopify-product-reviews-badge" data-id="10109315278"></span> 
    </div>
    <ul class="item-swatch color_swatch_Value">  
    </ul>
  </div>
</div>
</li>
<li class="grid__item item-row  " id="product-10109323854" >
  <div class="products">
    <div class="product-container">  
      <a href="/collections/necklaces/products/grooved-texture-gold-bangle-set" class="grid-link">    
        <div class="ImageOverlayCa"></div>
        <img src="http:////cdn.shopify.com/s/files/1/1811/9385/products/9_large.jpg?v=1496148941" class="featured-image" alt="Grooved Texture Gold Bangle Set">
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
          <a href="javascript:void(0)" id="grooved-texture-gold-bangle-set" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>         
          <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109323854">                                    
            <input type="hidden" name="id" value="37510518926" />  
            <a class="add-cart-btn">
             <i class="zmdi zmdi-shopping-cart"></i>
           </a>
         </form>  
         <a href="/products/grooved-texture-gold-bangle-set">                      
          <i class="fa fa-external-link" aria-hidden="true"></i>
        </a>       
        <div class="add-to-wishlist">     
          <div class="show">
            <div class="default-wishbutton-grooved-texture-gold-bangle-set loading"><a class="add-in-wishlist-js btn" href="grooved-texture-gold-bangle-set"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
            <div class="loadding-wishbutton-grooved-texture-gold-bangle-set loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="grooved-texture-gold-bangle-set"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
            <div class="added-wishbutton-grooved-texture-gold-bangle-set loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="product-detail">

    <a href="/collections/necklaces/products/grooved-texture-gold-bangle-set" class="grid-link__title">Grooved Texture Gold Bangle Set</a>     
    <div class="grid-link__meta">
      <div class="product_price">
    <div class="grid-link__org_price">
          <span class=money>$765.00</span>
        </div>
      </div>      
      <span class="shopify-product-reviews-badge" data-id="10109323854"></span> 
    </div>
    <ul class="item-swatch color_swatch_Value">  
    </ul>
  </div>
</div>
</li>
</ul>
<div class="nav_featured">
  <a class="prev active"></a>
  <a class="next"></a>  
</div>
</div>



  <script>
        $(document).ready(function (){
            var canvas = $("#myCanvas");
            var context = canvas.get(0).getContext("2d");
            context.strokeRect(0, 0, canvas[0].width, canvas[0].height);
            var text = 'Adrina';
            renderName(context, text, canvas);
            $('#name').change(function (evt){
                context.clearRect(0, 0, canvas[0].width, canvas[0].height);
                text = $(this).val().replace(/\s/g, '');
                new_str = text.toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
                     return txtVal.toUpperCase();
                 });
                $('#sendName').val($(this).val());
                renderName(context, new_str, canvas);
            });

            $('.imgSelect').click(function (){
                $('.imgSelect').removeClass(' active');
                $(this).addClass(' active');
                fontStyle = "40pt " + $(this).attr('data');
                sessionStorage.setItem('fontStyle', fontStyle);
                context.clearRect(0, 0, canvas[0].width, canvas[0].height);
                $('#sendFontFamily').val($(this).attr('data'));
                new_str = text.toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
                     return txtVal.toUpperCase();
                 });
                renderName(context, new_str, canvas);              
            });

            $('.selectDisplay').click(function(){
              $('.selectDisplay').removeClass('activeImage');
              $(this).addClass('activeImage');
              var index = $(this).index();
              showBigImages(index);
            });
        });

        function showBigImages(index){
          var images = $('.mainImages');
          images.removeClass('show');
          images.addClass('hide');
          for(var i = 0; i < images.length; i++){
            if(i == index){
              images[i].classList.remove('hide');
              images[i].classList.add('show');
            }
          }
        }

        function renderName(context, text, canvas){
            if(!sessionStorage.getItem('fontStyle')){
                context.font = "40pt 'Dancing Script', Cursive";
                var fontStyle = "40pt 'Dancing Script', Cursive";

            }
            else{ 
                context.font = sessionStorage.getItem('fontStyle');
                var fontStyle = sessionStorage.getItem('fontStyle');
            }
            context.textAlign = 'center';
            context.fillStyle = '#d3b26f';
            var textWidth = context.measureText(text).width;
            context.fillText(text, canvas[0].width/2, canvas[0].width/2 );
            var height = parseInt(context.font.match(/\d+/), 10);
            var firstChar = text[0];
            var lastChar = text[text.length - 1];
            var spacingForFirstChar;
            var spacingForLastChar;
            var heightForLastChar;
            var heightForFirstChar = -height/3;
            fontStyle = fontStyle.split(' ')[1];
            if(!fontStyle.match(/^[A-Z]/)){
                fontStyle = fontStyle[1];
            }
            else{
                fontStyle = fontStyle[0];
            }
            switch(fontStyle){
                case 'D':
                    if(firstChar.match(/[C||G|O|Q|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 17;                
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;  
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                    }

                    if(lastChar.match(/[d|l|f]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 4;
                    }
                    else if(lastChar.match(/[b|h|k]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 15;
                    }
                    else if(lastChar.match(/[t]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }

                    if(lastChar.match(/[b|d|f|h|k|l]/)){
                        heightForLastChar =  -height/3;
                    }
                    else if(lastChar.match(/[t|i|j]/)){
                        heightForLastChar =  - (height/3 - height/6);
                    }
                    else{
                        heightForLastChar =  height/10;
                    }
                    break;
                case 'A':
                    if(firstChar.match(/[C|G|O|Q|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20;
                        heightForFirstChar = - height/5;           
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25; 
                        heightForFirstChar = -height/8; 
                    }
                    else if(firstChar.match(/[I]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/10;
                    }
                    else if(firstChar.match(/[J|K]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = 6;
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/6;
                    }

                    if(lastChar.match(/[d|l|f|i|j]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 4;
                    }
                    else if(lastChar.match(/[t]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }
                    else if(lastChar.match(/[v|y]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 6;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }

                    if(lastChar.match(/[b|d|f|l]/)){
                        heightForLastChar =  -height/8;
                    }
                    else if(lastChar.match(/[t|i|j]/)){
                        heightForLastChar =  - (height/3 - height/5);
                    }
                    else{
                        heightForLastChar =  1;
                    }
                    break;
                case 'C':
                    if(firstChar.match(/[C|G|O|Q|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20;
                        heightForFirstChar = - height/4;           
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20; 
                        heightForFirstChar = -12;
                    }
                    else if(firstChar.match(/[I|J|K]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -3;
                    }
                    else if(firstChar.match(/[T]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = -15;
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/6;
                    }

                    if(lastChar.match(/[d|l|f|i|j|t|v|y]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }
                    else if(lastChar.match(/[b]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 18;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }

                    if(lastChar.match(/[b|d|f|l]/)){
                        heightForLastChar =  -12;
                    }
                    else if(lastChar.match(/[t|i|j]/)){
                        heightForLastChar =  - (height/3 - height/5);
                    }
                    else{
                        heightForLastChar =  1;
                    }
                    break;
                case 'G':
                    if(firstChar.match(/[C|G]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = - height/2;           
                    }
                    else if(firstChar.match(/[O|Q]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = - height/3;           
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20; 
                        heightForFirstChar = -12;
                    }
                    else if(firstChar.match(/[I|K|F|H]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = 0;
                    }
                    else if(firstChar.match(/[J]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = 10;
                    }
                    else if(firstChar.match(/[T|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = -6;
                    }
                    else if(firstChar.match(/[Z]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;
                        heightForFirstChar = -20;
                    }
                    else if(firstChar.match(/[U|V]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;
                        heightForFirstChar = -25;
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/6;
                    }

                    if(lastChar.match(/[d|l|f|t|v]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 ;
                    }
                    else if(lastChar.match(/[i|j|y]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 -5;
                    }
                    else if(lastChar.match(/[b]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }

                    if(lastChar.match(/[b|d|f|l|t|h]/)){
                        heightForLastChar =  -16;
                    }
                    else if(lastChar.match(/[i|j]/)){
                        heightForLastChar =  - (height/3 - height/5);
                    }
                    else{
                        heightForLastChar =  1;
                    }
                    break;
            }
            

            var leftImage = new Image();
            leftImage.onload = function(){
                context.drawImage(leftImage,  spacingForFirstChar, heightForFirstChar, 100 , 235);
            }
            leftImage.src = '/images/leftImage.png';
            var image = new Image();
            image.onload = function(){
                context.drawImage(image, spacingForLastChar, heightForLastChar, 100, 235);
            }
            image.src = '/images/rightImage.png';
        }
        </script>
@endsection
<script>
        $(document).ready(function (){
            var canvas = $("#myCanvas");
            var context = canvas.get(0).getContext("2d");
            context.strokeRect(0, 0, canvas[0].width, canvas[0].height);
            var text = 'Adrina';
            renderName(context, text, canvas);
            $('#name').change(function (evt){
                context.clearRect(0, 0, canvas[0].width, canvas[0].height);
                text = $(this).val().replace(/\s/g, '');
                new_str = text.toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
                     return txtVal.toUpperCase();
                 });
                $('#sendName').val($(this).val());
                renderName(context, new_str, canvas);
            });

            $('.imgSelect').click(function (){
                $('.imgSelect').removeClass(' active');
                $(this).addClass(' active');
                fontStyle = "40pt " + $(this).attr('data');
                sessionStorage.setItem('fontStyle', fontStyle);
                context.clearRect(0, 0, canvas[0].width, canvas[0].height);
                $('#sendFontFamily').val($(this).attr('data'));
                new_str = text.toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
                     return txtVal.toUpperCase();
                 });
                renderName(context, new_str, canvas);              
            });

            $('.selectDisplay').click(function(){
              $('.selectDisplay').removeClass('activeImage');
              $(this).addClass('activeImage');
              var index = $(this).index();
              showBigImages(index);
            });
        });

        function showBigImages(index){
          var images = $('.mainImages');
          images.removeClass('show');
          images.addClass('hide');
          for(var i = 0; i < images.length; i++){
            if(i == index){
              images[i].classList.remove('hide');
              images[i].classList.add('show');
            }
          }
        }

        function renderName(context, text, canvas){
            if(!sessionStorage.getItem('fontStyle')){
                context.font = "40pt 'Dancing Script', Cursive";
                var fontStyle = "40pt 'Dancing Script', Cursive";

            }
            else{ 
                context.font = sessionStorage.getItem('fontStyle');
                var fontStyle = sessionStorage.getItem('fontStyle');
            }
            context.textAlign = 'center';
            context.fillStyle = '#d3b26f';
            var textWidth = context.measureText(text).width;
            context.fillText(text, canvas[0].width/2, canvas[0].width/2 );
            var height = parseInt(context.font.match(/\d+/), 10);
            var firstChar = text[0];
            var lastChar = text[text.length - 1];
            var spacingForFirstChar;
            var spacingForLastChar;
            var heightForLastChar;
            var heightForFirstChar = -height/3;
            fontStyle = fontStyle.split(' ')[1];
            if(!fontStyle.match(/^[A-Z]/)){
                fontStyle = fontStyle[1];
            }
            else{
                fontStyle = fontStyle[0];
            }
            switch(fontStyle){
                case 'D':
                    if(firstChar.match(/[C||G|O|Q|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 17;                
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;  
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                    }

                    if(lastChar.match(/[d|l|f]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 4;
                    }
                    else if(lastChar.match(/[b|h|k]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 15;
                    }
                    else if(lastChar.match(/[t]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }

                    if(lastChar.match(/[b|d|f|h|k|l]/)){
                        heightForLastChar =  -height/3;
                    }
                    else if(lastChar.match(/[t|i|j]/)){
                        heightForLastChar =  - (height/3 - height/6);
                    }
                    else{
                        heightForLastChar =  height/10;
                    }
                    break;
                case 'A':
                    if(firstChar.match(/[C|G|O|Q|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20;
                        heightForFirstChar = - height/5;           
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25; 
                        heightForFirstChar = -height/8; 
                    }
                    else if(firstChar.match(/[I]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/10;
                    }
                    else if(firstChar.match(/[J|K]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = 6;
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/6;
                    }

                    if(lastChar.match(/[d|l|f|i|j]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 4;
                    }
                    else if(lastChar.match(/[t]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }
                    else if(lastChar.match(/[v|y]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 6;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }

                    if(lastChar.match(/[b|d|f|l]/)){
                        heightForLastChar =  -height/8;
                    }
                    else if(lastChar.match(/[t|i|j]/)){
                        heightForLastChar =  - (height/3 - height/5);
                    }
                    else{
                        heightForLastChar =  1;
                    }
                    break;
                case 'C':
                    if(firstChar.match(/[C|G|O|Q|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20;
                        heightForFirstChar = - height/4;           
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20; 
                        heightForFirstChar = -12;
                    }
                    else if(firstChar.match(/[I|J|K]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -3;
                    }
                    else if(firstChar.match(/[T]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = -15;
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/6;
                    }

                    if(lastChar.match(/[d|l|f|i|j|t|v|y]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
                    }
                    else if(lastChar.match(/[b]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 18;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }

                    if(lastChar.match(/[b|d|f|l]/)){
                        heightForLastChar =  -12;
                    }
                    else if(lastChar.match(/[t|i|j]/)){
                        heightForLastChar =  - (height/3 - height/5);
                    }
                    else{
                        heightForLastChar =  1;
                    }
                    break;
                case 'G':
                    if(firstChar.match(/[C|G]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = - height/2;           
                    }
                    else if(firstChar.match(/[O|Q]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = - height/3;           
                    }
                    else if(firstChar.match(/[A|E|L]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20; 
                        heightForFirstChar = -12;
                    }
                    else if(firstChar.match(/[I|K|F|H]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = 0;
                    }
                    else if(firstChar.match(/[J]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = 10;
                    }
                    else if(firstChar.match(/[T|S]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
                        heightForFirstChar = -6;
                    }
                    else if(firstChar.match(/[Z]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;
                        heightForFirstChar = -20;
                    }
                    else if(firstChar.match(/[U|V]/)){
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;
                        heightForFirstChar = -25;
                    }
                    else{
                        spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
                        heightForFirstChar = -height/6;
                    }

                    if(lastChar.match(/[d|l|f|t|v]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 ;
                    }
                    else if(lastChar.match(/[i|j|y]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 -5;
                    }
                    else if(lastChar.match(/[b]/)){
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }
                    else{
                        spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
                    }

                    if(lastChar.match(/[b|d|f|l|t|h]/)){
                        heightForLastChar =  -16;
                    }
                    else if(lastChar.match(/[i|j]/)){
                        heightForLastChar =  - (height/3 - height/5);
                    }
                    else{
                        heightForLastChar =  1;
                    }
                    break;
            }
            

            var leftImage = new Image();
            leftImage.onload = function(){
                context.drawImage(leftImage,  spacingForFirstChar, heightForFirstChar, 100 , 235);
            }
            leftImage.src = '/images/leftImage.png';
            var image = new Image();
            image.onload = function(){
                context.drawImage(image, spacingForLastChar, heightForLastChar, 100, 235);
            }
            image.src = '/images/rightImage.png';
        }
        </script>



<script type="text/javascript">
  $(document).ready(function(){
   var related_count = $('.related-products li.item-row').length;
   if(related_count > 4) { $('.nav_featured').css('display','block');}
   else {$('.nav_featured').css('display','none');}
   var related = $(".related-products");
   related.owlCarousel({
    items: 4,
    itemsCustom: false,
    itemsDesktop: [1199, 4],
    itemsDesktopSmall: [980, 3],
    itemsTablet: [630, 2],
    itemsTabletSmall: false,
    itemsMobile: [479, 1],
    singleItem: false,
    itemsScaleUp: false,
    responsive: true,
    responsiveRefreshRate: 200,
    responsiveBaseWidth: window,
    autoPlay: false,
    stopOnHover: false,
    navigation: false,
    pagination:false
  });
      // Custom Navigation Events
      $(".nav_featured .next").click(function(){
        related.trigger('owl.next');
      })
      $(".nav_featured .prev").click(function(){
        related.trigger('owl.prev');
      }) 
    });

  </script>
  <script src="/assets/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js" type="text/javascript"></script>
</div>       

</div>


<div class="dt-sc-hr-invisible-large"></div>

</main>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>