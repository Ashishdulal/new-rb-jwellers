@extends('layouts.backend.user.app')

@section('content')

        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="d-sm-flex align-items-baseline report-summary-header">
                          <h5 class="font-weight-semibold">Report Summary</h5> <span class="ml-auto">Updated Report</span> <form action="javascript:history.go(0)"><button class="btn btn-icons border-0 p-2"><i class="icon-refresh"></i></button></form>
                        </div>
                      </div>
                    </div>
                    <div class="row report-inner-cards-wrapper">
                      <div class=" col-md -6 col-xl report-inner-card">
                        <div class="inner-card-text">
                          <span class="report-title">Products Ordered</span>
                          <h4>{{$allOrders}}</h4>
                          <span class="report-count"> <a href="/user/order-history">View All</a></span>
                        </div>
                        <div class="inner-card-icon bg-success">
                          <i class="icon-basket"></i>
                        </div>
                      </div>
                      <div class="col-md-6 col-xl report-inner-card">
                        <div class="inner-card-text">
                          <span class="report-title">Checkout</span>
                          <h4>{{$cartCount}}</h4>
                          <span class="report-count"> <a href="/checkout">Check Out Now</a></span>
                        </div>
                        <div class="inner-card-icon bg-danger">
                          <i class="icon-briefcase"></i>
                        </div>
                      </div>
                      <div class="col-md-6 col-xl report-inner-card">
                        <div class="inner-card-text">
                          <span class="report-title">Your Profile</span>
                          <h4>{{$currentuser->name}}</h4>
                          <span class="report-count"> <a href="/user/profile">View</a></span>
                        </div>
                        <div class="inner-card-icon">
                          @if($currentuser->image)
                          <img class="rounded-circle ml-2" src="/images/users/{{$currentuser->image}}" alt="{{$currentuser->name}}">
                          @else
                            <img class="rounded-circle ml-2" src="/images/users/user-image.png" alt="{{$currentuser->name}}">
                          @endif
                        </div>
                      </div>
                      <div class="col-md-6 col-xl report-inner-card">
                        <div class="inner-card-text">
                          <span class="report-title">Your Dashboard</span>
                          <h4>All</h4>
                          <span class="report-count"><a href="/products/all">Go To All Products</a></span>
                        </div>
                        <div class="inner-card-icon bg-primary">
                          <i class="icon-diamond"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
@endsection