@extends('layouts.backend.app')

@section('content')
<div class="container-fluid">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<div class="row">
		
		<div class="col-sm-12 col-sm-12">
			<form method="post" action="/home/menu/create" enctype="multipart/form-data">
				@csrf
				<h3>Add New Menu</h3>
				<div class="row">
					
					<div class="col-sm-10 create-form-group pl-30">
						<div class="form-group">
							<input type="hidden" value="{{csrf_token()}}" name="_menuName"/>
							<label for="menuName">Menu Name:</label>
							<input class="form-control" type="text" name="menuName"/>
						</div>
						<div class="form-group">
							<input type="file" name="image" accept="image/jpg, image/png, image/jpeg">
						</div>
						<div class="form-group">
							<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection