@extends('layouts.backend.app')

@section('content')
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> Add Brand </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home/sliders">Brands</a></li>
          <li class="breadcrumb-item active" aria-current="page">Create Brand</li>
        </ol>
      </nav>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Create New Brand</h4>
          <p class="card-description"> Please fill the below forms to add a new Brand. [ * : required ] </p>
          @if($errors->any())
          @foreach($errors->all() as $error)
          <ul>
            <li>{{$error}}</li>
          </ul>
          @endforeach
          @endif
          <form action="/home/brand/create" method="POST"  class="forms-sample" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="name">Brand*</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Brand Name">
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
            <button type="reset" class="btn btn-light">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection