  @extends('layouts.backend.app')

@section('content')
<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> All Sizes </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sizes</li>
        </ol>
      </nav>
    </div>
    <!-- <a style="margin-bottom: 10px;" href="/home/size/create" class="btn btn-primary">Add New Size</a> -->
    <button style="margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Size</button>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Create New Size</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
           <div class="col-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <!-- <h4 class="card-title"></h4> -->
                <p class="card-description"> Please fill the below forms to add a new Size. [ * : required ] </p>
                @if($errors->any())
                @foreach($errors->all() as $error)
                <ul>
                  <li>{{$error}}</li>
                </ul>
                @endforeach
                @endif
                <form action="/home/size/create" method="POST"  class="forms-sample" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="name">Size Title*</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Size Name">
                  </div>
                  <button type="submit" class="btn btn-primary mr-2">Submit</button>
                  <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
                  <button type="reset" class="btn btn-light">Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <!-- DATA TABLE-->
        <div class="table-responsive m-b-40">
         <table class="table table-borderless table-data3">
          <div class="container">
           <thead>
            <tr><th>Id</th>
             <th>Sizes</th>
             <th>Action</th>
           </tr>
         </thead>
         <tbody>
          @foreach($sizes as $size)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$size->name}}</td>
            <td><a href="{{route('size.edit', $size->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
              <form method="post" action="{{route('size.delete',$size->id)}}">
               @csrf
               {{ method_field('DELETE') }}
               <button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
             </form>
           </td>
         </tr>
         @endforeach
       </tbody>
     </div>
   </table>
 </div>
 <!-- END DATA TABLE-->
</div>
</div>
</div>
</div>

@endsection