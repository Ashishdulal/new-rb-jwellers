@extends('layouts.backend.app')

@section('content')
<div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> All Products </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Products</li>
                </ol>
              </nav>
            </div>
            <a style="margin-bottom: 10px;" href="/home/product/create" class="btn btn-primary">Add New Product</a>
            @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
   
        @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- DATA TABLE-->
		@foreach ($productcat as $pcat)
					<h3 style="text-align: center;padding: 20px 0;text-transform: uppercase;">{{$pcat->name}}</h3>
					<div style="border-bottom: 2px solid black; padding: 50px 0;" class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<div class="container">
			<thead>
				<tr><th>Id</th>
					<th>Name</th>
					<th>Product Code</th>
					<th>Image</th>
					<th>Ethinticity</th>
					<th>Events</th>
					<th>Weight</th>
					<th>Price</th>
					<th>Stock Quantity</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
				<tr>
                        <th>Weight</th>
                        <th>Price</th>
                      </tr>
				</thead>
			<tbody>
				@if(count($products))
				@foreach($products as $product)
				@if( $pcat->id == $product->cat_id)
				<tr>
				<td>{{ $loop->iteration }}@if($product->featured == 1)<h5>(Featured)</h5>@endif</td>
				<td>{{$product->name}}</td>
				<td>{{$product->product_code}}</td>
				<td>
					<div class="row">
						<div class="col-sm-4 product-display-img">
					<img class="slider-image" src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}">
						</div>
						<div class="col-sm-4 product-display-img">
					<img class="slider-image" src="/uploads/products/{{$product->image2}}" alt="{{$product->name}}">
						</div>
					</div>
				</td>
				<td>
					@foreach($ethProduct as $etProduct)
                @foreach($ethnicities as $ethnicity)
                @if($ethnicity->id == $etProduct->ethnic_id)
                @if($etProduct->product_id == $product->id)
                <ul>
                	<li>{{$ethnicity->name}}</li>
                </ul>
                @endif
                @endif
                @endforeach
                @endforeach
				</td>
				<td>
					@foreach($evtProduct as $evProduct)
                @foreach($events as $evt)
                @if($evt->id == $evProduct->event_id)
                @if($evProduct->product_id == $product->id)
                <ul>
                	<li>{{$evt->name}}</li>
                </ul>
                @endif
                @endif
                @endforeach
                @endforeach
				</td>
				<td>
				@foreach($productvarient as $pvarient)
                        @if($pvarient->product_id == $product->id)
                        <ul>
                        <li>{{$pvarient->weight}}<br></li>
                    	</ul>
                      @endif
                        @endforeach
                        </td>
                        <td>
				@foreach($productvarient as $pvarient)
                        @if($pvarient->product_id == $product->id)
                        <ul style="list-style: none;">
                        <li>{{$pvarient->rate}}<br></li>
                    </ul>
                      @endif
                        @endforeach
                        </td>
				<!-- <td>{{$product->price}}</td> -->
				<!-- <td>{{$product->sale_price}}</td> -->
				<td>{{$product->product_stock}}</td>
				<td>
					<?php 
                    $excerpt = $product->description;
                    $the_str = substr($excerpt, 0, 40);
                    echo $the_str; 
                    ?>...
				</td>
				<td><a href="{{route('product.edit', $product->id)}}"><button class="btn btn-primary make-btn">Edit</button></a> | 
				<form method="post" action="{{route('product.delete',$product->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
			</tr>
			@endif
				@endforeach
				@else
				<tr>
				<td>
				<h5>There are no posts in {{$pcat->name}}.</h5>
				</td>
				</tr>
				@endif
			</tbody>
		</div>
			</table>
		</div>
		@endforeach
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>

@endsection