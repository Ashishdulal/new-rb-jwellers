@extends('layouts.backend.app')
<script src="https://code.jquery.com/jquery-3.3.1.min.js" type='text/javascript'></script>

@section('content')
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> Add product </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home/products">product</a></li>
          <li class="breadcrumb-item active" aria-current="page">Create product</li>
        </ol>
      </nav>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Create New product</h4>
          <p class="card-description"> Please fill the below forms to add a new product. [ * : required ] </p>
          @if($errors->any())
          @foreach($errors->all() as $error)
          <ul>
            <li>{{$error}}</li>
          </ul>
          @endforeach
          @endif
          <form action="/home/product/create" method="POST"  class="forms-sample" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="category">Product Category*</label>
              <select name="cat_id" class="form-control">

                <option>Choose The Product Category</option>
                @foreach($productcat as $productcatt)

                <option value="{{$productcatt->id}}">{{$productcatt->name}}</option>

                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="category">Do You Want To Feature this Product</label><br>
              <div class="form-check select-tab-view">
                <label class="form-check-label select-tab-view">
                  <input type="checkbox" class="form-check-input" name="featured" value="1
                  "> Yes </label>
                </div>
              </div>
            <div class="form-group">
              <label for="category">Product Ethnicity</label><br>
              <div class="form-check select-tab-view">
                <label class="form-check-label select-tab-view">
                  <input type="checkbox" id="checkall" class="form-check-input checkbox" value=""> All </label>
                </div>
                @foreach($ethnicities as $ethnicity)
                <div class="form-check select-tab-view">
                  <label class="form-check-label select-tab-view">
                    <input type="checkbox" class="form-check-input checkbox ethnicCheckbox" name="ethnicity[]" value="{{$ethnicity->id}}"> {{$ethnicity->name}}
                  </label>
                </div>
                @endforeach
              </div>
              <div class="form-group">
                <label for="category">Product Event</label><br>
                <div class="form-check select-tab-view">
                  <label class="form-check-label select-tab-view">
                    <input type="checkbox" id="checkall_next" class="form-check-input checkbox_next" value=""> All </label>
                  </div>
                  @foreach($events as $event)
                  <div class="form-check select-tab-view">
                    <label class="form-check-label select-tab-view">
                      <input type="checkbox" class="form-check-input checkbox_next" name="event[]" value="{{$event->id}}"> {{$event->name}} </label>
                    </div>
                    @endforeach
                  </div>
                  <div class="form-group">
                    <label for="name">Product Name*</label>
                    <input type="text" class="form-control" required name="name" id="name" placeholder="Enter the product name">
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label>Image upload* [ Image size:1280*1549 ]</label>
                        <div class="input-group">
                          <input type="file" name="image1" class="form-control file-upload-info" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                          <img id="load-img" class="file-upload-info" src="#" alt="Selected Image" />
                          <span class="input-group-append">
                          </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label>Image upload</label>
                        <div class="input-group">
                          <input type="file" name="image2" class="form-control file-upload-info" onchange="readURLNext(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                          <img id="load-img-n" class="file-upload-info" src="#" alt="Selected Image" />
                          <span class="input-group-append">
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
<!--                   <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="productprice">Weight*</label>
                        <div class="input-group">
                          <input type="text" name="weight" class="form-control" id="weight" placeholder="product Weight">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="productsaleprice">Rate</label>
                        <div class="input-group">
                          <input type="text" name="price" class="form-control" id="price" placeholder="product Old Price">
                        </div>
                      </div>
                    </div>
                  </div> -->
                  <div>
                    <table class="table table-bordered" id="dynamicTable">  
                      <tr>
                        <th>Weight [In Tola]*</th>
                        <th>Length</th>
                        <th>Breadth</th>
                        <th>Height</th>
                        <th>Diameter</th>
                        <th>Price*</th>
                        <th>Action</th>
                      </tr>
                      <tr>  
                        <td><input type="text" required name="weight[]" placeholder="Enter the Weight" class="form-control" /></td> 
                        <td><input type="number" name="product_length[]" min="0" placeholder="Enter product length" class="form-control" /></td>  
                        <td><input type="number" name="product_breadth[]" min="0" placeholder="Enter product breadth" class="form-control" /></td>  
                        <td><input type="number" name="product_width[]" min="0" placeholder="Enter product width" class="form-control" /></td>  
                        <td><input type="number" name="product_diameter[]" min="0" placeholder="Enter product diameter" class="form-control" /></td>
                        <td><input type="number" required name="rate[]" min="0" placeholder="Enter your Price" class="form-control" /></td>  
                        <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                      </tr>  
                    </table> <br>

                    <script type="text/javascript">

                      var i = 0;

                      $("#add").click(function(){

                        // alert('button clicked');

                        ++i;

                        $("#dynamicTable").append('<tr><td><input type="text" name="weight[]" placeholder="Enter the weight" class="form-control" /></td><td><input type="number" name="product_length[]" min="0" placeholder="Enter product length" class="form-control" /></td><td><input type="number" name="product_breadth[]" min="0" placeholder="Enter product breadth" class="form-control" /></td>  <td><input type="number" name="product_width[]" min="0" placeholder="Enter product width" class="form-control" /></td><td><input type="number" name="product_diameter[]" min="0" placeholder="Enter product diameter" class="form-control" /></td> <td><input type="number" min="0" name="rate[]" placeholder="Enter your Price" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
                      });

                      $(document).on('click', '.remove-tr', function(){  
                       $(this).parents('tr').remove();
                     });  

                   </script>
                 </div>

              <!-- <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="productprice">Price*</label>
                    <div class="input-group">
                      <input type="text" name="sale_price" class="form-control" id="sale_price" placeholder="product Price">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label for="productsaleprice">Sale Price</label>
                    <div class="input-group">
                      <input type="text" name="price" class="form-control" id="price" placeholder="product Old Price if necessary...">
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="form-group">
                <div class="row">
                  <div class="col-md-4">
                    <label for="productcode">Product Code*</label>
                    <div class="input-group">
                      <input type="text" required name="product_code" class="form-control" id="productcode" placeholder="Product Code">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <label for="productstock">Product Stock*</label>
                    <div class="input-group">
                      <input type="number" required name="product_stock" min="0" class="form-control" id="productstock" placeholder="Product Stock">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <label for="minstock">Minimum Weight [In Tola]*</label>
                    <div class="input-group">
                      <input type="number" required name="minimum_weight" min="0" class="form-control" id="minstock" placeholder="Minimum Weight">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <textarea required class="form-control" id="description" name="description" rows="10"></textarea>
              </div>
              <button type="submit" class="btn btn-primary mr-2">Submit</button>
              <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
              <button type="reset" class="btn btn-light">Reset</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endsection

    <script type='text/javascript'>
     $(document).ready(function(){

   // Changing state of CheckAll checkbox 
   $(".checkbox").change(function(){
    var checkAll = $('#checkall').is(':checked');
    var checkBox = $('.ethnicCheckbox').is(':checked');
    if(checkAll){
     if(checkBox){
       $("#checkall").prop("checked", false);
     }
   }
   
   if($(".checkbox").length == $(".checkbox:checked").length) {
    $("#checkall").prop("checked", true);
  } else {
    $("#checkall").removeAttr("checked");
  }
});

   // Check or Uncheck All checkboxes
   $("#checkall").change(function(){
     var checked = $(this).is(':checked');
     if(checked){
       $(".checkbox").each(function(){
         $(this).prop("checked",true);
       });
     }else{
       $(".checkbox").each(function(){
         $(this).prop("checked",false);
       });
     }
   });


 });
</script>
<script type='text/javascript'>
 $(document).ready(function(){
   // Check or Uncheck All checkboxes
   $("#checkall_next").change(function(){
     var checked = $(this).is(':checked');
     if(checked){
       $(".checkbox_next").each(function(){
         $(this).prop("checked",true);
       });
     }else{
       $(".checkbox_next").each(function(){
         $(this).prop("checked",false);
       });
     }
   });

  // Changing state of CheckAll checkbox 
  $(".checkbox_next").click(function(){

    if($(".checkbox_next").length == $(".checkbox_next:checked").length) {
      $("#checkall_next").prop("checked", true);
    } else {
      $("#checkall_next").removeAttr("checked");
    }

  });
});
</script>
