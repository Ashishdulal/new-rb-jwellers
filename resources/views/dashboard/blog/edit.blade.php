@extends('layouts.backend.app')

@section('content')
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> Edit Blog </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home/blogs">Blog</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit Blog</li>
        </ol>
      </nav>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Edit Blog</h4>
          <p class="card-description"> Please fill the below forms to edit a  Blog. [ * : required ] </p>
          @if($errors->any())
          @foreach($errors->all() as $error)
          <ul>
            <li>{{$error}}</li>
          </ul>
          @endforeach
          @endif
          <form action="/home/blog/edit/{{$blogs->id}}" method="POST"  class="forms-sample" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="category">Blog Category*</label>
              <select name="cat_id" class="form-control">


              @foreach($blogcat as $Postcat)
              @if($Postcat->id == $blogs->cat_id)
              <option value="{{$Postcat->id}}">{{$Postcat->name}}</option>
              @endif
              @endforeach


              @foreach($blogcat as $Postcat)
              @if($Postcat->id != $blogs->cat_id)
              <option value="{{$Postcat->id}}">{{$Postcat->name}}</option>
              @endif
              @endforeach
            </select>
            </div>
            <div class="form-group">
              <label for="title">Blog Title*</label>
              <input type="text" class="form-control" name="title" value="{{$blogs->title}}" id="title" placeholder="Enter the Blog Title">
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
              <label>Image upload*</label>
                <div class="input-group">
                  <input type="file" name="f_image" class="form-control file-upload-info" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                  <img id="load-img" class="file-upload-info image_view" src="/uploads/blogs/{{$blogs->f_image}}" alt="Selected Image" />
                  <span class="input-group-append">
                  </span>
                </div>
              </div>
              <div class="col-md-6">
              <label>Image upload</label>
                <div class="input-group">
                  <input type="file" name="i_image" class="form-control file-upload-info" onchange="readURLNext(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                  <img id="load-img-n" class="file-upload-info image_view" src="/uploads/blogs/{{$blogs->i_image}}" alt="Selected Image" />
                  <span class="input-group-append">
                  </span>
                </div>
              </div>
              </div>
            </div>
            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" id="description" name="description" rows="10">{{$blogs->description}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
            <button type="reset" class="btn btn-light">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection
  