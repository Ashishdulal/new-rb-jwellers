@extends('layouts.backend.app')

@section('content')
<div class="main-panel">
	<div class="content-wrapper">
		<div class="page-header">
			<h3 class="page-title"> All Blogs </h3>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
					<li class="breadcrumb-item active" aria-current="page">Blogs</li>
				</ol>
			</nav>
		</div>
		<a style="margin-bottom: 10px;" href="/home/blog/create" class="btn btn-primary">Add New Blog</a>
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
					<!-- DATA TABLE-->
					@foreach ($blogcat as $blog_cat)
					<h3 style="text-align: center;padding: 20px 0;">{{$blog_cat->name}}</h3>
					<div style="border-bottom: 2px solid black; padding: 50px 0;" class="table-responsive m-b-40">
						<table class="table table-borderless table-data3">
							<div class="container">
								<thead>
									<tr>
										<th>Id</th>
										<th>Title</th>
										<th>Images</th>
										<th>Description</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(count($blogs))
									@foreach($blogs as $blog)
									@if( $blog_cat->id == $blog->cat_id)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>{{$blog->title}}</td>
										<td>
											<div class="row">
												<div class="col-sm-6">
													<img class="slider-image" src="/uploads/blogs/{{$blog->f_image}}" alt="{{$blog->name}}">
												</div>
												<div class="col-sm-6">
													<img class="slider-image" src="/uploads/blogs/{{$blog->i_image}}" alt="{{$blog->name}}">
												</div>
											</div>
										</td>
										<td>
											<?php 
							                    $excerpt = $blog->description;
							                    $the_str = substr($excerpt, 0, 300);
							                    echo $the_str; 
							                    ?>...
										<td><a href="{{route('blog.edit', $blog->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
											<form method="post" action="{{route('blog.delete',$blog->id)}}">
												@csrf
												{{ method_field('DELETE') }}
												<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
											</form>
										</td>
									</tr>
									@endif
									@endforeach
									@else
									<tr>
										<td>
											<h5>There are no posts in {{$blog_cat->name}}.</h5>
										</td>
									</tr>
									@endif
								</tbody>
							</div>
						</table>
					</div>
					@endforeach
					<!-- END DATA TABLE-->
				</div>
			</div>
		</div>
	</div>

	@endsection