@extends('layouts.backend.app')

@section('content')
<div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> All Testimonials </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Testimonials</li>
                </ol>
              </nav>
            </div>
            <a style="margin-bottom: 10px;" href="/home/testimonial/create" class="btn btn-primary">Add New Testimonials</a>
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<div class="container">
			<thead>
				<tr><th>Id</th>
					<th>Name</th>
					<th>Designation</th>
					<th>Image</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
				</thead>
			<tbody>
				@foreach($testimonials as $testimonial)
				<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{$testimonial->name}}</td>
				<td>{{$testimonial->designation}}</td>
				<td>
					<img style="max-width: 20%;" class="slider-image" src="/uploads/testimonial/{{$testimonial->image}}" alt="{{$testimonial->name}}">
				</td>
				<td><?php echo ($testimonial->description )?></td>
				<td><a href="{{route('testimonial.edit', $testimonial->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
				<form method="post" action="{{route('testimonial.delete',$testimonial->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
			</tr>
				@endforeach
			</tbody>
		</div>
			</table>
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>

@endsection