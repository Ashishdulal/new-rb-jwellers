@extends('layouts.backend.app')

@section('content')
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> Edit Testimonials </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home/testimonials">Testimonial</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit Testimonial</li>
        </ol>
      </nav>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Edit the testimonial</h4>
          <p class="card-description"> Please fill the below forms to Edit the testimonial. [ * : required ] </p>
          @if($errors->any())
          @foreach($errors->all() as $error)
          <ul>
            <li>{{$error}}</li>
          </ul>
          @endforeach
          @endif
          <form action="/home/testimonial/edit/{{$testimonials->id}}" method="POST"  class="forms-sample" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="name">Name*</label>
              <input type="text" class="form-control" name="name" id="name" value="{{$testimonials->name}}" placeholder="Enter the Client name">
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
              <label>Image upload*</label>
                <div class="input-group">
                  <input type="file" name="image" class="form-control file-upload-info" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                  <img style="max-width: 30%;" id="load-img" class="file-upload-info" src="/uploads/testimonial/{{$testimonials->image}}" alt="Selected Image" />
                  <span class="input-group-append">
                  </span>
                </div>
              </div>
              </div>
            </div>
           <div class="form-group">
              <label for="deisgnation">Designation*</label>
              <input type="text" class="form-control" name="designation" value="{{$testimonials->designation}}" id="deisgnation" placeholder="Enter the Client Deisgnation">
            </div>
            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" id="description" name="description" rows="10">{{$testimonials->description}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
            <button type="reset" class="btn btn-light">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection