@extends('layouts.backend.app')

@section('content')
<div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> All product Category </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">product Category</li>
                </ol>
              </nav>
            </div>
            <!-- <a style="margin-bottom: 10px;" href="/home/product-category/create" class="btn btn-primary">Add New product Category</a> -->
            <button style="margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Product Category</button>
            <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Create New Product Category</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title"></h4> -->
                    <p class="card-description"> Please fill the below forms to add a new Product Category. [ * : required ] </p>
                    @if($errors->any())
                    @foreach($errors->all() as $error)
                    <ul>
                      <li>{{$error}}</li>
                    </ul>
                    @endforeach
                    @endif
                    <form action="/home/product-category/create" method="POST"  class="forms-sample" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="name">Product Category*</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Product Category Title">
                      </div>
                      <div class="form-group">
                    <div class="row">
                      <div class="col-md-12">
                        <label>Image upload* [ Image size:1280*1549 ]</label>
                        <div class="input-group">
                          <input type="file" name="image" class="form-control file-upload-info" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                          <img id="load-img" class="file-upload-info" src="#" alt="Selected Image" />
                          <span class="input-group-append">
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                      <button type="submit" class="btn btn-primary mr-2">Submit</button>
                      <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
                      <button type="reset" class="btn btn-light">Reset</button>
                    </form>
                  </div>
                </div>
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<div class="container">
			<thead>
				<tr><th>Id</th>
					<th>Category Name</th>
          <th class="make-center">Image</th>
					<th>Action</th>
				</tr>
				</thead>
			<tbody>
				@foreach($productcat as $productca)
				<tr class="product-cat-main">
				<td>{{ $loop->iteration }}</td>
				<td>{{$productca->name}}</td>
        <td class="make-center"><img class="upload-cat-img" src="/uploads/products/{{$productca->image}}" alt="Selected Image" /></td>
				<td><a href="{{route('product-category.edit', $productca->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
				<form method="post" action="{{route('product-category.delete',$productca->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
			</tr>
				@endforeach
			</tbody>
		</div>
			</table>
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>

@endsection