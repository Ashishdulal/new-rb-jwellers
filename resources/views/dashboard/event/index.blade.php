  @extends('layouts.backend.app')

  @section('content')
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> All Events </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Events</li>
          </ol>
        </nav>
      </div>
      <!-- <a style="margin-bottom: 10px;" href="/home/material/create" class="btn btn-primary">Add New Material</a> -->
      <button style="margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Events</button>
      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Create New Event</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
             <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <!-- <h4 class="card-title"></h4> -->
                  <p class="card-description"> Please fill the below forms to add a new Event. [ * : required ] </p>
                  @if($errors->any())
                  @foreach($errors->all() as $error)
                  <ul>
                    <li>{{$error}}</li>
                  </ul>
                  @endforeach
                  @endif
                  <form action="/home/event/create" method="POST"  class="forms-sample" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label for="name">Event Title*</label>
                      <input type="text" class="form-control" name="name" id="name" placeholder="Event Name">
                    </div>
                    <div class="form-group">
                      <label for="category">Choose Ethnicity*</label><br>
                      @foreach($ethnicities as $ethnicity)
                      <div class="form-check select-tab-view">
                        <label class="form-check-label select-tab-view">
                          <input type="checkbox" class="form-check-input checkbox ethnicCheckbox" name="ethnicity[]" value="{{$ethnicity->id}}"> {{$ethnicity->name}}
                        </label>
                      </div>
                      @endforeach
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-12">
                          <label>Image upload*</label>
                          <div class="input-group">
                            <input type="file" name="image" class="form-control file-upload-info" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                            <img id="load-img" class="file-upload-info" src="#" alt="Selected Image" />
                            <span class="input-group-append">
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
                    <button type="reset" class="btn btn-light">Reset</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <!-- DATA TABLE-->
          <div class="table-responsive m-b-40">
           <table class="table table-borderless table-data3">
            <div class="container">
             <thead>
              <tr><th>Id</th>
               <th>Event Name</th>
               <th class="make-center">Image</th>
               <th>Ethnicity</th>
               <th>Action</th>
             </tr>
           </thead>
           <tbody>
            @foreach($events as $event)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{$event->name}}</td>
              <td class="make-center"><img class="upload-cat-img" src="/uploads/products/events/{{$event->image}}" alt="{{$event->name}}" /></td>
              <td>
                <ul>
                @foreach($eventsEthnics as $etProduct)
                @foreach($ethnicities as $ethnicity)
                @if($ethnicity->id == $etProduct->ethnic_id)
                @if($event->id == $etProduct->event_id)
                  <li>
                {{$ethnicity->name}}    
                  </li>
                @endif
                @endif
                @endforeach
                @endforeach
                </ul>
              </td>
              <td><a href="{{route('event.edit', $event->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
                <form method="post" action="{{route('event.delete',$event->id)}}">
                 @csrf
                 {{ method_field('DELETE') }}
                 <button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
               </form>
             </td>
           </tr>
           @endforeach
         </tbody>
       </div>
     </table>
   </div>
   <!-- END DATA TABLE-->
 </div>
</div>
</div>
</div>
<script type='text/javascript'>
 $(document).ready(function(){

   // Changing state of CheckAll checkbox 
   $(".checkbox").change(function(){
    var checkAll = $('#checkall').is(':checked');
    var checkBox = $('.ethnicCheckbox').is(':checked');
    if(checkAll){
     if(checkBox){
       $("#checkall").prop("checked", false);
     }
   }
   
   if($(".checkbox").length == $(".checkbox:checked").length) {
    $("#checkall").prop("checked", true);
  } else {
    $("#checkall").removeAttr("checked");
  }
});

   // Check or Uncheck All checkboxes
   $("#checkall").change(function(){
     var checked = $(this).is(':checked');
     if(checked){
       $(".checkbox").each(function(){
         $(this).prop("checked",true);
       });
     }else{
       $(".checkbox").each(function(){
         $(this).prop("checked",false);
       });
     }
   });


 });
</script>

@endsection