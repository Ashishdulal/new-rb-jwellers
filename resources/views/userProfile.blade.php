@extends('layouts.backend.user.app')

@section('content')

<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-md-12 grid-margin">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="d-sm-flex align-items-baseline report-summary-header">
                  <h5 class="font-weight-semibold">User Profile</h5> <span class="ml-auto">User Profile</span> <form action="javascript:history.go(0)"><button class="btn btn-icons border-0 p-2"><i class="icon-refresh"></i></button></form>
                </div>
              </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
   
        @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
            <div class="row report-inner-cards-wrapper">
              @if($currentuser->image)
              <img class="img-user-dashboard" src="/images/users/{{$currentuser->image}}" alt="{{$currentuser->name}}">
              @else
              <img class="img-user-dashboard" src="/images/users/user-image.png" alt="{{$currentuser->name}}">
              @endif
              <table class="table table-hover">
                <thead>
                  <tr>
                    <td colspan="2">User Data</td>
                  </tr>
                </thead> 
                <tbody>
                  <tr>
                    <td>Id:</td>
                    <td>{{$currentuser->id}}</td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td>Name:</td>
                    <td>{{$currentuser->name}}</td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td>Email:</td>
                    <td>{{$currentuser->email}}</td>
                  </tr>
                </tbody> 
<!--                 <tbody>
                  <tr>
                    <td>Password:</td> 
                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#passwordChange">Change password</button></td>
                  </tr>
                </tbody> -->
              </table>
                <a href="/user/edit/{{$currentuser->id}}" class="btn btn-primary">Edit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="passwordChange" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div style="background: #ffff;" class="modal-body">
              <form class="form-horizontal" method="POST" action="/user/changePassword">
                @csrf
                <div class="form-group">
                                <label for="current_password" class="col-md-4 control-label">Current Password</label>

                                <div class="col-md-10">
                                    <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Your Current Password" required>
                                </div>
                            </div>
                <div class="form-group">
                  <label for="new_password" class="col-md-4 control-label">New Password</label>

                  <div class="col-md-10">
                    <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" required autocomplete="new_password" placeholder="Password">

                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                </div>

                <div class="form-group">
                  <label for="password_confirmation" class="col-md-10 control-label">Confirm New Password</label>

                  <div class="col-md-10">
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="password_confirmation" placeholder="Confirm Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                      Change Password
                    </button>
                  </div>
                </div>
              </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
  @endsection