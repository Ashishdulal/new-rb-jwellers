@extends('layouts.frontend.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('fonts/fonts.css') }}">
@endsection

@section('content')
<main class="main-content"> 
  <nav class="breadcrumb" aria-label="breadcrumbs">
    <h1>{{$product->name}}</h1>
    <a href="/" title="Back to the frontpage">Home</a> 
    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    <a href="/products/{{$category_name->id}}" title="">{{$category_name->name}}</a>  
    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    <span>{{$product->name}}</span>
  </nav>
  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">

    <div class="grid__item">         
      <div itemscope itemtype="http://schema.org/Product">
        <meta itemprop="url" content="http://jwellery.nepgeeks.com/product-detail/1">  
        <meta itemprop="name" content="{{$product->name}}" />
        <meta itemprop="sku" content="UKL656"/>
        <meta itemprop="gtin14" content=""/>
        <meta itemprop="brand" content="CaratLane"/>
        <meta itemprop="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official."/>
        <meta itemprop="image" content="https://cdn.shopify.com/s/files/1/1811/9385/products/4_grande.jpg?v=1496145230"/>
        <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
          <meta itemprop="priceCurrency" content="USD">
          <meta itemprop="price" content="199.00">
          <meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
          <meta itemprop="availability" content="http://schema.org/InStock"/>
        </div>
      </div>


      <div class="product-single">

        <div class="grid__item wide--one-half post-large--one-half large--one-half product-img-box">
          @if($product->cat_id != 6)
          <div class="product-photo-container">

            <a href="/uploads/products/{{$product->image1}}" >
              <img id="product-featured-image" src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}" data-zoom-image="/uploads/products/{{$product->image1}}"/>
            </a>
<!--          <a href="/uploads/products/{{$product->image2}}" >
                <img id="product-featured-image" src="/uploads/products/{{$product->image2}}" alt="{{$product->name}}" data-zoom-image="/uploads/products/{{$product->image2}}"/>
              </a> -->
            </div>
            <div class="more-view-wrapper-owlslider">
              <ul id="ProductThumbs" class="product-photo-thumbs owl-carousel owl-theme thumbs">

                <li class="">
                  <a href="javascript:void(0)" data-image="/uploads/products/{{$product->image1}}" data-zoom-image="/uploads/products/{{$product->image1}}">
                    <img src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}">
                  </a>
                </li>
                <li class="">
                  <a href="javascript:void(0)" data-image="/uploads/products/{{$product->image2}}" data-zoom-image="/uploads/products/{{$product->image2}}">
                    <img src="/uploads/products/{{$product->image2}}" alt="{{$product->name}}">
                  </a>
                </li>
                @if($productVideo)
                @foreach($productVideo as $pvideo)
                <li class="">
                  <a href="javascript:void(0)" data-image="/uploads/video/{{$pvideo->video}}" data-zoom-image="/uploads/video/{{$pvideo->video}}">
                    <video style="    min-width: 100%; min-height: 100%; width: auto; padding: 0 10px" width="100%" height="145px" controls>
                    <source src="/uploads/video/{{$pvideo->video}}">
                      Your browser does not support HTML5 video.
                    </video>
                  </a>
                </li>
                @endforeach
                @endif

              </ul>
            </div>
            @else
            <div class="row">
              <div class="dimension">
                <canvas id="myCanvas" class="mainImages show" width="500" height="500">
                  <p>Your browser doesn't support the content! Please switch the browser to view the content.</p>
                </canvas>
                <div class="mainImages hide">
                  <img src="/images/JWLN0300.jpg" alt="">
                </div>
                <div class="mainImages hide">
                  <img src="/images/JWLN03001.jpg" alt="">
                </div>
                <div class="mainImages hide">
                  <img src="/images/JWLN03002.jpg" alt="">
                </div>
                <div class="mainImages hide">
                  <img src="/images/JWLN03003.jpg" alt="">
                </div>
                <canvas id="scaledCanvas" class="mainImages hide" width="500" height="500">
                  <p>Your browser doesn't support the content! Please switch the browser to view the content.</p>
                </canvas>
              </div>
              <div class="sideImages">
                  <div class="selectDisplay activeImage">
                    <img src="/images/sumanjwellery.png" alt="">
                  </div>
                  <div class="selectDisplay">
                    <img src="/images/JWLN0300.jpg" alt="">
                  </div>
                  <div class="selectDisplay">
                    <img src="/images/JWLN03001.jpg" alt="">
                  </div>
                  <div class="selectDisplay">
                    <img src="/images/JWLN03002.jpg" alt="">
                  </div>
                  <div class="selectDisplay">
                    <img src="/images/JWLN03003.jpg" alt="">
                  </div>
                  <div class="selectDisplay">
                    <img src="/images/scaledName.png" alt="">
                  </div>
                </div>
            </div>
            @endif
          </div>

          <div class="product_single_detail_section grid__item wide--one-half post-large--one-half large--one-half">
            @if ($errors->any())
            <div class="alert alert-danger">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success text-center">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
              <p>{{ Session::get('success') }}</p>
            </div>
            @endif
            <h2 class="product-single__title">{{$product->name}}</h2>

            <div class="product_single_price">

              <div class="product_price">

                <div class="grid-link__org_price" id="ProductPrice">
                  <!-- <span class=money>$199.00</span> -->
                </div>


              </div>
            </div>
            <span class="shopify-product-reviews-badge" data-id="10109317582"></span>

            <div class="product-description rte" >
              <?php 
              $excerpt = $product->description;
              $the_str = substr($excerpt, 0, 80);
              echo $the_str; 
              ?>...
            </div>
            @if($product->cat_id == 6)
            <div class="form-group">
              <label for="name">Enter the name</label>
              <input type="text" name="name" class="form-control" id="name">
            </div>
            <div class="form-group imgTag">
              <label for="">Select the font</label>
              <div class="imgSelect" data="Gretoon">
                <img src="/images/gretoon.jpg" alt="Gretoon">
              </div>
            </div>
            @endif


            <div class="product-infor">
              @if($product->cat_id != 6)
              @if(count($prodEtnicities))
              <p class="product-vendor">
                <label>Ethincity:</label>
                @foreach($prodEtnicities as $ethnicity)
                <span>   {{$ethnicity->name}} ,&nbsp; </span>
                @endforeach
              </p>
                @endif


                @if (count($prodEvents))
              <p class="product-type">
                <label>Event: </label>  
                @foreach($prodEvents as $evt)
                <span> {{$evt->name}} , &nbsp;</span>
                @endforeach
              </p>
                @endif
              @endif

              <p class="product-inventory">
                <label>Availability:  </label>              

                @if($product->product_stock == '0')
                <span>  Out Of Stock</span>
                @else
                <span> In Stock </span>
                @endif
              </p>
              <p class="product-inventory">
                <label>Minimum Weight:  </label>              
                <span>  {{$product->minimum_weight}} Tola</span>
              </p>
              @if(count($priceForFirstWeightInfo))
              @if($priceForFirstWeightInfo[0]->product_length)
              <p class="product-inventory">
                <label>Product Length:  </label>
                <input style="border:none;width: 15%;background: transparent;padding: 0;margin: 0;" id="prodLength" type="text" disabled="" value="{{$priceForFirstWeightInfo[0]->product_length}} cm" class="inc button" name="prodlength">
              </p>
              @endif
              @if($priceForFirstWeightInfo[0]->product_breadth)
              <p class="product-inventory">
                <label>Product Breadth:  </label>              
                <input style="border:none;width: 15%;background: transparent;padding: 0;margin: 0;" id="prodBreadth" type="text" disabled="" value="{{$priceForFirstWeightInfo[0]->product_breadth}} cm" class="inc button" name="prodbreadth">
              </p>
              @endif
              @if($priceForFirstWeightInfo[0]->product_width)
              <p class="product-inventory">
                <label>Product Width:  </label>              
               <input style="border:none;width: 15%;background: transparent;padding: 0;margin: 0;" id="prodHeight" type="text" disabled="" value="{{$priceForFirstWeightInfo[0]->product_width}} cm" class="inc button" name="prodheight">
              </p>
              @endif
              @if($priceForFirstWeightInfo[0]->product_diameter)
              <p class="product-inventory">
                <label>Product Dimension:  </label>              
                <input style="border:none;width: 15%;background: transparent;padding: 0;margin: 0;" id="prodDiameter" type="text" disabled="" value="{{$priceForFirstWeightInfo[0]->product_diameter}} cm" class="inc button" name="proddiameter">
              </p>
              @endif
              @endif

            </div>




            <form method="post" action="/cart" 
            accept-charset="UTF-8" 
            class="product-form">
            @csrf
            <div class="swatch clearfix">
             <input type="hidden" id="product_id" name="product_id" value="{{$product->id}}">
             <input type="hidden" id="price" name="productPrice" value="{{ $priceForFirstWeight }}">
             <input type="hidden" name="namedName" id="sendName">
             <input type="hidden" name="fontFamily" id="sendFontFamily">
             <div class="header">weight :</div>
             <div class="swatch-section">
              <select name="weight" id="weightSelect" class="" required>
                <!-- <option hidden="">  Select weight </option><span>[ In Tola ]</span> -->
                @foreach($productvarient as $key=> $pvarient)
                @if($pvarient->product_id == $product->id)
                <option  value="{{$pvarient->weight}}">{{$pvarient->weight}}</option>
                @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class="selector-wrapper-secton  ">

            <script type="text/javascript">

              $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
              });

              $("#weightSelect").on("change", function () {
                var weight = $(this).val();
                var product_id=document.getElementById("product_id").value;
               // alert(product_id);


                //Here you can use ajax to call php function
                $.ajax({
                  url: '/weight-search/',
                  type: 'GET',
                  dataType: "json",
                  data: {
                    model_weight: weight, product_id: product_id
                  },
                  success: function (ret_data) {
                    // console.log(ret_data);
                    document.getElementsByName('subTotal')[0].value = ret_data['rate'];
                    document.getElementsByName('productPrice')[0].value = ret_data['rate'];
                    document.getElementsByName('prodlength')[0].value = ret_data['product_length'];
                    document.getElementsByName('prodbreadth')[0].value = ret_data['product_breadth'];
                    document.getElementsByName('prodheight')[0].value = ret_data['product_width'];
                    document.getElementsByName('proddiameter')[0].value = ret_data['product_diameter'];
                  }
                });
              });


            </script>
          </div>          
          <div class="product-single__quantity ">
            <div class="quantity-box-section">
              <!-- <label>Quantity:</label>
              <div class="quantity_width"> 
                <div class="dec button">-</div>  
                <input type="number" id="quantities" name="quantity" value="1" min="1">  
                <div class="inc button">+</div>  
                <p class="min-qty-alert" style="display:none">Minimum quantity should be 1</p>
              </div> -->


              <div class="total-price">
                <label>Price </label>
                <span class="showMoney"></span>
                <input style="border:none;width: 15%;background: transparent;" id="allTotalPrice" type="text" disabled="" value="{{$priceForFirstWeight}}" required class="inc button" name="subTotal">
              </div>
           <!--   <div class="total-price">
              <label>Total </label><span id="totalDeliveryPrice">Rs.</span>
            </div>
          -->
        </div>
      </div>
<!--         <script>

          $(document).ready(function(){
            $qty = $("#quantities").html();
            $price = $("#ProductMoney").html();
            $total_price = $price * $qty;
            $("#totalPrice").html( $total_price);
            $("#total_price").html($total_price);
            $("#totalPriceSent").val($total_price);
            $("#singlePriceSent").val($price);
            $("#totalDeliveryPrice").val(parseInt($("#deliveryPrice").html(),10));
          });

        </script> -->

        @if($product->product_stock != '0')
        <div class="product-single-button">

          <button type="submit" id="AddToCart" class="btn">
            <span id="AddToCartText">Add to Cart</span>
          </button>
       </div>
       @endif
       <!-- Trigger the modal with a button -->
       <button style="padding:15px 35px;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myCustomOrder">Custom Order</button>
     </form><br>
     <!-- <a href="/sendsms" class="btn btn-info btn-lg">Send Sms</a> -->
     <!-- Modal -->
     <div id="myCustomOrder" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Custom Order</h4>
          </div>
          <div class="modal-body1">
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success text-center">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
              <p>{{ Session::get('success') }}</p>
            </div>
            @endif
            <form action="/custom-order/{{$product->id}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="custom-order">
                <div class="col-md-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-description"> Please Fill the form to get the custom order inquiry. </p>
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" required name="name" placeholder="Please enter your name" aria-label="name">
                      </div>
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="number" required class="form-control form-control-lg" name="phone" placeholder="please enter your phone number" aria-label="phone">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" required class="form-control form-control-sm" name="mail" placeholder="please enter your email" aria-label="mail">
                      </div>
                      <div class="form-group">
                        <label>Weight [In Tola]</label>
                        <input type="text" required class="form-control form-control-lg" id="customWeight" name="myWeight" placeholder="please enter your custom weight" aria-label="weight">
                      </div>
                      <div class="form-group">
                        <label>Size</label>
                        <input type="text" required class="form-control form-control-lg" name="size" placeholder="please enter the size" aria-label="size">
                      </div>
                      <input type="hidden" name="product_name" value="{{$product->name}}">
                    </div>
                  </div>
                </div>
                <script type="text/javascript">

                  $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
                  });

                  $("#customWeight").on("change", function () {
                    var myWeight = $(this).val();
                    var product_id=document.getElementById("product_id").value;
               // alert(myWeight);


                //Here you can use ajax to call php function
                $.ajax({
                  url: '/weight-verify/',
                  type: 'GET',
                  dataType: "json",
                  data: {
                    model_weight: myWeight, product_id: product_id
                  },
                  success: function (ret_data) {
                    var myMessage = 'The minimum weight available for this Product is '+ret_data;

                    if(myWeight < ret_data){
                      alert(myMessage);
                      document.getElementsByName('myWeight')[0].value = ret_data;
                    }
                  }
                });
              });


            </script>
            <div class="col-md-6 grid-margin stretch-card">
              <div class="product-infor">
                <div>
                  <img src="/uploads/products/{{$product->image1}}" alt="{{$product->name}}">
                </div>
                <p class="product-inventory order-customize"><br>
                  <label>Product Name:  </label> <br>             
                  {{$product->name}}
                </p>
                @if($product->cat_id != 6)
              @if(count($prodEtnicities))
              <p class="product-vendor order-customize">
                <label>Ethincity:</label>
                @foreach($prodEtnicities as $ethnicity)
                <span>   {{$ethnicity->name}} ,&nbsp; </span>
                @endforeach
              </p>
                @endif


                @if (count($prodEvents))
              <p class="product-vendor order-customize">
                <label>Event: </label>  
                @foreach($prodEvents as $evt)
                <span> {{$evt->name}} , &nbsp;</span>
                @endforeach
              </p>
                @endif
              @endif

              </div>
            </div>
            &nbsp;&nbsp; <button type="submit" class="btn btn-primary mr-2">Submit</button>
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- 
     <div class="grid__item" style="margin-top:20px;">
      <img src="(image-path)" alt="secure_pay">
    </div> -->



    <div class="share_this_btn">
     <!-- <div class="addthis_toolbox addthis_default_style addthis_32x32_style"  >
      <a class="addthis_button_preferred_1"></a>
      <a class="addthis_button_preferred_2"></a>
      <a class="addthis_button_preferred_3"></a>
      <a class="addthis_button_preferred_4"></a>
      <a class="addthis_button_compact"></a>
      <a class="addthis_counter addthis_bubble_style"></a>
    </div> -->
    <script type='text/javascript'>
      var addthis_product = 'sfy-2.0.2';
      var addthis_plugin_info = {"info_status":"enabled","cms_name":"Shopify","cms_version":null,"plugin_name":"AddThis Sharing Tool","plugin_version":"2.0.2","plugin_mode":"AddThis"};
    var addthis_config     = {/*AddThisShopify_config_begins*/pubid:'xa-525fbbd6215b4f1a', button_style:'style3', services_compact:'', ui_delay:0, ui_click:false, ui_language:'', data_track_clickback:true, data_ga_tracker:'', custom_services:'', custom_services_size:true/*AddThisShopify_config_ends*/};
  </script>
  <script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-525fbbd6215b4f1a'></script>

</div>

<div class="dt-sc-hr-invisible-large"></div>
<div class="dt-sc-tabs-container">
  <ul class="dt-sc-tabs">
    <li><a href="#"> Description </a></li> 
    <!-- <li><a href="#"> Reviews  </a></li> -->
    <li><a href="#"> Shipping details  </a></li>
  </ul>

  <div class="dt-sc-tabs-content" id="desc_pro">
    <p><span><?php echo ($product->description )?></span></p>
  </div>

<!-- 
  <div class="dt-sc-tabs-content">
    <div class="commentlist">
      <div class="comment-text">
        <div class="rating-review">
          <div id="shopify-product-reviews" data-id="10109317582"></div>
        </div>
      </div>
    </div>
  </div>
-->

<div class="dt-sc-tabs-content">
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>

</div>
</div>

</div>
<div class="dt-sc-hr-invisible-large"></div>
<div class="related-products-container">

  <div class="dt-sc-hr-invisible-large"></div>
  <div class="section-header section-header--small">
    <div class="border-title">

     <h2 class="section-header__title">Related Products</h2>  

     <div class="short-desc"> <p>    
      From this Collection
    </p>
  </div>

</div>
</div>
<div class="dt-sc-hr-invisible-small"></div>    
<ul class="grid-uniform grid-link__container related-products">
  @if(count($allProducts))
  @foreach($allProducts as $othprod)
      <li class="grid__item item-row  " id="product-10109315918" >
        <div class="products">
          <div class="product-container">  
            <a href="/product-detail/{{$othprod->id}}" class="grid-link">    
              <div class="ImageOverlayCa"></div>
              <img src="/uploads/products/{{$othprod->image1}}" class="featured-image" alt="{{$othprod->name}}">
            </a>
            <div class="ImageWrapper">
        <div class="product-button">  




         <div class="add-to-wishlist">     
          <div class="show">
            <form  action="/cart" method="post">     
              @csrf  
              <input type="hidden" name="product_id" value="{{$othprod->id}}">
              <input type="hidden" name="productPrice" value="100">
              <input type="hidden" name="weight" value="1">
            <div class="added-wishbutton-barry-gold-bangle-for-her loading"><a class="added-wishlist btn add_to_wishlist" href="#" data-toggle="modal" data-target="#productsOrder{{$othprod->id}}"><span style="font-size: 8px">Custom Order</span><span class="tooltip-label">Custom Order</span></a></div>
              <div class="added-wishbutton-barry-gold-bangle-for-her loading"><button style="background: #ffffff;font-size: 8px" type="submit" class="add-in-wishlist-js btn" >Add to Cart</button></div>
            </form>
            <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
            <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
          </div>
        </div>


      </div>
          </div>
        </div>
        <div class="product-detail">
          <a href="/product-detail/{{$othprod->id}}" class="grid-link__title">{{$othprod->name}}</a>     
          <div class="grid-link__meta">
            <div class="product_price">
              <div class="grid-link__org_price">
                <span class=money>
                  <?php 
                  $excerpt = $othprod->description;
                  $the_str = substr($excerpt, 0, 30);
                  echo $the_str; 
                  ?>...

                </span>
              </div>
            </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315918"></span> 
          </div>
          <ul class="item-swatch color_swatch_Value">  
          </ul>
        </div>
      </div>
    </li>

     <!-- Modal -->
  <div class="modal fade" id="productsOrder{{$othprod->id}}" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Custom Order</h4>
          </div>
          <div class="modal-body1">
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success text-center">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
              <p>{{ Session::get('success') }}</p>
            </div>
            @endif
            <form action="/custom-order/{{$othprod->id}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="custom-order">
                <div class="col-md-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-description"> Please Fill the form to get the custom order inquiry. </p>
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" required name="name" placeholder="Please enter your name" aria-label="name">
                      </div>
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="number" required class="form-control form-control-lg" name="phone" placeholder="please enter your phone number" aria-label="phone">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" required class="form-control form-control-sm" name="mail" placeholder="please enter your email" aria-label="mail">
                      </div>
                      <div class="form-group">
                        <label>Weight [In Tola]</label>
                        <input type="text" required class="form-control form-control-lg" id="customWeight" name="myWeightOther" placeholder="please enter your custom weight" aria-label="weight">
                      </div>
                      <div class="form-group">
                        <label>Size</label>
                        <input type="text" required class="form-control form-control-lg" name="size" placeholder="please enter the size" aria-label="size">
                      </div>
                      <input type="hidden" name="product_name" value="{{$othprod->name}}">
                    </div>
                  </div>
                </div>
                <script type="text/javascript">

                  $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
                  });

                  $("#customWeight").on("change", function () {
                    var myWeightOther = $(this).val();
                    var product_id=document.getElementById("product_id").value;
               // alert(myWeightOther);


                //Here you can use ajax to call php function
                $.ajax({
                  url: '/weight-verify/',
                  type: 'GET',
                  dataType: "json",
                  data: {
                    model_weight: myWeight, product_id: product_id
                  },
                  success: function (ret_data) {
                    var myMessage = 'The minimum weight available for this Product is '+ret_data;

                    if(myWeight < ret_data){
                      alert(myMessage);
                      document.getElementsByName('myWeight')[0].value = ret_data;
                    }
                  }
                });
              });


            </script>
            <div class="col-md-6 grid-margin stretch-card">
              <div class="product-infor">
                <div>
                  <img src="/uploads/products/{{$othprod->image1}}" alt="{{$othprod->name}}">
                </div>
                <p class="product-inventory order-customize"><br>
                  <label>Product Name:  </label> <br>             
                  {{$othprod->name}}
                </p>
                 @if($product->cat_id != 6)
              @if(count($prodEtnicities))
              <p class="product-vendor order-customize">
                <label>Ethincity:</label>
                @foreach($prodEtnicities as $ethnicity)
                <span>   {{$ethnicity->name}} ,&nbsp; </span>
                @endforeach
              </p>
                @endif


                @if (count($prodEvents))
              <p class="product-vendor order-customize">
                <label>Event: </label>  
                @foreach($prodEvents as $evt)
                <span> {{$evt->name}} , &nbsp;</span>
                @endforeach
              </p>
                @endif
              @endif


              </div>
            </div>
            &nbsp;&nbsp; <button type="submit" class="btn btn-primary mr-2">Submit</button>
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    @endforeach
    @else
    <div>
      <h5>No Products Available</h5>
    </div>
    @endif
  </ul>
  <div class="nav_featured">
    <a class="prev active"></a>
    <a class="next"></a>  
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
   var related_count = $('.related-products li.item-row').length;
   if(related_count > 4) { $('.nav_featured').css('display','block');}
   else {$('.nav_featured').css('display','none');}
   var related = $(".related-products");
   related.owlCarousel({
    items: 4,
    itemsCustom: false,
    itemsDesktop: [1199, 4],
    itemsDesktopSmall: [980, 3],
    itemsTablet: [630, 2],
    itemsTabletSmall: false,
    itemsMobile: [479, 1],
    singleItem: false,
    itemsScaleUp: false,
    responsive: true,
    responsiveRefreshRate: 200,
    responsiveBaseWidth: window,
    autoPlay: false,
    stopOnHover: false,
    navigation: false,
    pagination:false
  });
      // Custom Navigation Events
      $(".nav_featured .next").click(function(){
        related.trigger('owl.next');
      })
      $(".nav_featured .prev").click(function(){
        related.trigger('owl.prev');
      }) 
    });

  </script>

  <script>
    $(document).ready(function (){
      var canvas = $("#myCanvas");
      var scaledCanvas = $('#scaledCanvas');
      var scaledContext = scaledCanvas.get(0).getContext("2d");
      var context = canvas.get(0).getContext("2d");
      context.strokeRect(0, 0, canvas[0].width, canvas[0].height);
      scaledContext.strokeRect(0, 0, scaledCanvas[0].width, scaledCanvas[0].height);
      var text = 'Adrina';
      renderName(context, text, canvas);
      drawScale(scaledContext);
      scaledRenderName(scaledContext, text);
      $('#name').change(function (evt){
        context.clearRect(0, 0, canvas[0].width, canvas[0].height);
        text = $(this).val().replace(/\s/g, '');
        new_str = text.toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
         return txtVal.toUpperCase();
       });
        $('#sendName').val($(this).val());
        renderName(context, new_str, canvas);
        scaledRenderName(scaledContext, new_str);
      });

      $('.imgSelect').click(function (){
        $('.imgSelect').removeClass(' active');
        $(this).addClass(' active');
        fontStyle = "40pt " + $(this).attr('data');
        sessionStorage.setItem('fontStyle', fontStyle);
        context.clearRect(0, 0, canvas[0].width, canvas[0].height);
        $('#sendFontFamily').val($(this).attr('data'));
        new_str = text.toLowerCase().replace(/\b[a-z]/g, function(txtVal) {
         return txtVal.toUpperCase();
       });
        renderName(context, new_str, canvas);
        scaledRenderName(scaledContext, new_str);
      });

      $('.selectDisplay').click(function(){
        $('.selectDisplay').removeClass('activeImage');
        $(this).addClass('activeImage');
        var index = $(this).index();
        showBigImages(index);
      });

    });

    function scaledRenderName(context, text){
      context.clearRect(110, 60, 440, 300);
      if(!sessionStorage.getItem('fontStyle')){
        context.font = "40pt Aylogri";
        var fontStyle = "40pt Aylogri";
      }
      else{ 
        context.font = sessionStorage.getItem('fontStyle');
        var fontStyle = sessionStorage.getItem('fontStyle');
      }
      context.fillStyle = '#d3b26f';
      var textWidth = context.measureText(text).width;
      context.fillText(text, 120, 300 );
    }

    function drawScale(scaledContext){
          // Vertical Line
          scaledContext.beginPath();
          scaledContext.moveTo(60, 60);
          scaledContext.lineTo(60, 440);
          scaledContext.strokeStyle = 'white';
          scaledContext.stroke();
          // Horizontal Line
          scaledContext.beginPath();
          scaledContext.moveTo(60, 440);
          scaledContext.lineTo(440  , 440);
          scaledContext.strokeStyle = 'white';
          scaledContext.stroke();

          //Vertical Scale for Cm
          var count = 0;
          
          for(var i = 0; i < 200; i+=4){
            if( count % 10 == 0 ){
              if(count == 0){
                scaledContext.beginPath();
                scaledContext.moveTo(60 , 300 - i);
                scaledContext.lineTo(30 , 300 - i);
                scaledContext.strokeStyle = 'white';
                scaledContext.stroke();
                scaledContext.fillStyle = 'white';
                scaledContext.fillText(count/10, 20, 300 - i);
              }
              scaledContext.beginPath();
              scaledContext.moveTo(60 , 300 - i);
              scaledContext.lineTo(90 , 300 - i);
              scaledContext.strokeStyle = 'white';
              scaledContext.stroke();
              scaledContext.fillStyle = 'white';
              scaledContext.fillText(count/10, 100, 300 - i);
            }
            else if(count % 5 == 0){
              scaledContext.beginPath();
              scaledContext.moveTo(60 , 300 - i);
              scaledContext.lineTo(75 , 300 - i);
              scaledContext.strokeStyle = 'white';
              scaledContext.stroke();
            }
            else{
              scaledContext.beginPath();
              scaledContext.moveTo(60 , 300 - i);
              scaledContext.lineTo(67.5 , 300 - i);
              scaledContext.strokeStyle = 'white';
              scaledContext.stroke();
            }
            count+=1;
          }

          // Vertical Scale for Inch
          count = 0;
          for(var i = 0; i < 200; i++){
            if( i % 51 == 0){
              if(count %2 == 0){
                scaledContext.beginPath();
                scaledContext.moveTo(60 , 300 - i);
                scaledContext.lineTo(30 , 300 - i);
                scaledContext.strokeStyle = 'white';
                scaledContext.stroke();
                scaledContext.fillStyle = 'white';
                scaledContext.fillText(count/2, 20, 300 - i);
              }
              else{
                scaledContext.beginPath();
                scaledContext.moveTo(60 , 300 - i);
                scaledContext.lineTo(45 , 300 - i);
                scaledContext.strokeStyle = 'white';
                scaledContext.stroke();
                scaledContext.fillStyle = 'white';
                scaledContext.fillText(count/2, 20, 300 - i);
              } 
              count += 1;      
            }
            
          }
          // Horizontal Scale Cm
          count = 0;
          for(var i = 0; i < 300; i+=4){
            if( count % 10 == 0 ){
              if(count == 0){
                scaledContext.beginPath();
                scaledContext.moveTo(120 + i, 470);
                scaledContext.lineTo(120 + i, 410);
                scaledContext.strokeStyle = 'white';
                scaledContext.stroke();
                scaledContext.fillStyle = '#fff';
                scaledContext.fillText(count/10, 120 + i, 480);
              }
              scaledContext.beginPath();
              scaledContext.moveTo(120 + i, 440);
              scaledContext.lineTo(120 + i, 410);
              scaledContext.strokeStyle = 'white';
              scaledContext.stroke();
              scaledContext.fillStyle = 'white';
              scaledContext.fillText(count/10, 120 + i, 400);
            }
            else if(count % 5 == 0){
              scaledContext.beginPath();
              scaledContext.moveTo(120 + i , 440);
              scaledContext.lineTo(120 + i , 425);
              scaledContext.strokeStyle = 'white';
              scaledContext.stroke();
            }
            else{
              scaledContext.beginPath();
              scaledContext.moveTo(120 + i , 440);
              scaledContext.lineTo(120 + i , 432.5);
              scaledContext.strokeStyle = 'white';
              scaledContext.stroke();
            }
            count+=1;
          }

          // Horizontal Scale for Inch
          count = 0;
          for(var i = 0; i < 300; i++){
            if(i % 51 == 0){
              if(count % 2 == 0){
                scaledContext.beginPath();
                scaledContext.moveTo(120 + i, 440);
                scaledContext.lineTo(120 + i, 470);
                scaledContext.strokeStyle = 'white';
                scaledContext.stroke();
                scaledContext.fillStyle = '#fff';
                scaledContext.fillText(count/2, 120 + i, 480);
              }
              else{
                scaledContext.beginPath();
                scaledContext.moveTo(120 + i, 440);
                scaledContext.lineTo(120 + i, 455);
                scaledContext.strokeStyle = 'white';
                scaledContext.stroke();
                scaledContext.fillStyle = '#fff';
                scaledContext.fillText(count/2, 120 + i, 480);
              }
              count += 1;
            }
          }
          // Text of Cm and Inch
          scaledContext.fillStyle = '#fff';
          scaledContext.font = "20px Arial";
          scaledContext.fillText('cm', 70, 430);
          scaledContext.fillText('inch', 70, 460);
        }

        function showBigImages(index){
          var images = $('.mainImages');
          images.removeClass('show');
          images.addClass('hide');
          for(var i = 0; i < images.length; i++){
            if(i == index){
              images[i].classList.remove('hide');
              images[i].classList.add('show');
            }
          }
        }

        function renderName(context, text, canvas){
          if(!sessionStorage.getItem('fontStyle')){
            context.font = "40pt Aylogri";
            var fontStyle = "40pt Aylogri";

          }
          else{ 
            context.font = sessionStorage.getItem('fontStyle');
            var fontStyle = sessionStorage.getItem('fontStyle');
          }
          context.textAlign = 'center';
          context.fillStyle = '#d3b26f';
          var textWidth = context.measureText(text).width;
          context.fillText(text, canvas[0].width/2, canvas[0].width/2 );
          var height = parseInt(context.font.match(/\d+/), 10);
          var firstChar = text[0];
          var lastChar = text[text.length - 1];
          var spacingForFirstChar;
          var spacingForLastChar;
          var heightForLastChar;
          var heightForFirstChar = -height/3;
          fontStyle = fontStyle.split(' ')[1];
          if(!fontStyle.match(/^[A-Z]/)){
            fontStyle = fontStyle[1];
          }
          else{
            fontStyle = fontStyle[1];
          }
          switch(fontStyle){
            case 'y':
            if(firstChar.match(/[A]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 50;
              heightForFirstChar = -height/5;
            }
            else if(firstChar.match(/[B|C|D|E|F|G|H|I|J|K]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70;
              heightForFirstChar = -height/5;
            }
            else if(firstChar.match(/[L]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 75;
              heightForFirstChar = -height/9;
            }
            else if(firstChar.match(/[M|N]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 80;
              heightForFirstChar = -height/11;
            }
            else if(firstChar.match(/[P|R]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 67;
              heightForFirstChar = -height/9;
            }
            else{
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 75;
              heightForFirstChar = -height/5;
            }

            if(lastChar.match(/[a|c|e|g|i|j|m|n|o|p|q|r|s|u|v|w|x|y|z]/)){
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
              heightForLastChar =  9;
            }
            else if(lastChar.match(/[f|t]/)){
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
              heightForLastChar =  -4;
            }
            else if(lastChar.match(/[h]/)){
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 18;
              heightForLastChar =  -13;
            }
            else if(lastChar.match(/[k]/)){
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
              heightForLastChar =  -16;
            }
            else if(lastChar.match(/[l]/)){
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 10;
              heightForLastChar =  -12;
            }
            else{
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 13;
              heightForLastChar =  -8;
            }
          break;

          case 'u':
            if(firstChar.match(/[A]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 72; 
              heightForFirstChar = -height/3; 
            }
            else if(firstChar.match(/[C|G]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 55; 
              heightForFirstChar = -height/2; 
            }
            else if(firstChar.match(/[F]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 43; 
              heightForFirstChar = -height/2; 
            }
            else if(firstChar.match(/[L]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 50; 
              heightForFirstChar = -height/2; 
            }
            else if(firstChar.match(/[O|Q]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 60;
              heightForFirstChar = -height/2.3; 
            }
            else if(firstChar.match(/[S]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70; 
              heightForFirstChar = -height/1.8; 
            }
            else if(firstChar.match(/[T|V]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 60; 
              heightForFirstChar = -height/3; 
            }
            else if(firstChar.match(/[W]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70; 
              heightForFirstChar = -height/3; 
            }
            else if(firstChar.match(/[X]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70; 
              heightForFirstChar = -height/2; 
            }
            else{
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 62; 
              heightForFirstChar = -height/2;
            }

            if(lastChar.match(/[t]/)){
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
              heightForLastChar =  -height/2 + height/5;
            }
            else{
              spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
              heightForLastChar =  -height/2 + height/1.8;
            }
            break;

            case 'r':
            if(firstChar.match(/[A]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 48;
              heightForFirstChar = - height/2.8;           
            }
            else if(firstChar.match(/[B|D]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70;
              heightForFirstChar = - height/2;           
            }
            else if(firstChar.match(/[H|V|W|Z]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70;
              heightForFirstChar = - height/2;           
            }
            else if(firstChar.match(/[J]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 60;
              heightForFirstChar = - height/2.6;           
            }
            else if(firstChar.match(/[K|P|R|S|X|Y]/)){
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 67;
              heightForFirstChar = - height/2.3;           
            }
            else{
              spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 70;
              heightForFirstChar = - height/2.8;
            }

            spacingForLastChar = canvas[0].width/2 + textWidth/2 - 3;
            heightForLastChar =  -height/2 + height/1.8;

            break;
            // case 'G':
            // if(firstChar.match(/[C|G]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
            //   heightForFirstChar = - height/2;           
            // }
            // else if(firstChar.match(/[O|Q]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
            //   heightForFirstChar = - height/3;           
            // }
            // else if(firstChar.match(/[A|E|L]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 20; 
            //   heightForFirstChar = -12;
            // }
            // else if(firstChar.match(/[I|K|F|H]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
            //   heightForFirstChar = 0;
            // }
            // else if(firstChar.match(/[J]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
            //   heightForFirstChar = 10;
            // }
            // else if(firstChar.match(/[T|S]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 10;
            //   heightForFirstChar = -6;
            // }
            // else if(firstChar.match(/[Z]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;
            //   heightForFirstChar = -20;
            // }
            // else if(firstChar.match(/[U|V]/)){
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 25;
            //   heightForFirstChar = -25;
            // }
            // else{
            //   spacingForFirstChar = canvas[0].width/2 - textWidth/2 - 100 + 12;
            //   heightForFirstChar = -height/6;
            // }

            // if(lastChar.match(/[d|l|f|t|v]/)){
            //   spacingForLastChar = canvas[0].width/2 + textWidth/2 ;
            // }
            // else if(lastChar.match(/[i|j|y]/)){
            //   spacingForLastChar = canvas[0].width/2 + textWidth/2 -5;
            // }
            // else if(lastChar.match(/[b]/)){
            //   spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
            // }
            // else{
            //   spacingForLastChar = canvas[0].width/2 + textWidth/2 - 8;
            // }

            // if(lastChar.match(/[b|d|f|l|t|h]/)){
            //   heightForLastChar =  -16;
            // }
            // else if(lastChar.match(/[i|j]/)){
            //   heightForLastChar =  - (height/3 - height/5);
            // }
            // else{
            //   heightForLastChar =  1;
            // }
            // break;
          }


          var leftImage = new Image();
          leftImage.onload = function(){
            context.drawImage(leftImage,  spacingForFirstChar, heightForFirstChar, 100 , 235);
          }
          leftImage.src = '/images/leftImage1.png';
          var image = new Image();
          image.onload = function(){
            context.drawImage(image, spacingForLastChar, heightForLastChar, 100, 235);
          }
          image.src = '/images/rightImage1.png';
        }
      </script>
      @endsection