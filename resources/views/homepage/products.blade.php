@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 
  
 
  

  <nav class="breadcrumb" aria-label="breadcrumbs">


    @if($cat_name)
    
    <h1>{{$cat_name->name}}</h1>
    <a href="/" title="Back to the frontpage">Home</a>

    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    
    <span>{{$cat_name->name}}</span>
    @else
    <h1>All Products</h1>
    <a href="/" title="Back to the frontpage">Home</a>

    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    
    <span>All Products</span>

    
    @endif
    
  </nav>
  
  
  
  
  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">
    
    <div class="grid__item">         
    </div>
  </div>
  <div class="grid__item">
    <div class="collection-products position-change">
      
      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter left-sidebar">
        <!-- /snippets/collection-sidebar.liquid -->

        <div class="collection_sidebar">
          <div id="shopify-section-sidebar-category" class="shopify-section"><div data-section-id="sidebar-category"  data-section-type="Sidebar-category"> 
            <div class="widget widget_product_categories">
              
              <h4>Category </h4>  
              
              
              <ul class="product-categories dt-sc-toggle-frame-set">
                
                
                <li class="cat-item cat-item-39 cat-parent">
                  <i></i><a href="/products/all">All</a> 
                </li>

                @foreach($allCat as $aCat)
                <li class="cat-item cat-item-39 cat-parent">
                  <i></i><a href="/products/{{$aCat->id}}">{{$aCat->name}}  </a> 
                </li>
                @endforeach     
                
              </ul>
              
            </div>
            
          </div>






        </div>
        
        
        <div class="refined-widgets">
          <a href="javascript:void(0)" class="clear-all" style="display:none">
            Clear All
          </a>
        </div>

        <div class="sidebar-block">
         <div id="shopify-section-sidebar-colors" class="shopify-section">
          

         </div>
       </div>
       


       <div id="shopify-section-sidebar-promoimage" class="shopify-section">  
        <div class="widget widget_promo_img">
          <ul id="promo-carousel">
            
            
            <li>
              <a href="" title="Promo Text"> 
                <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/about-img1_1024x1024_9fe067a2-9fca-46b6-9185-ddec35c7e803_500x500.jpg?v=1496215958" alt="Promo Text" title="Promo Text" /> 
              </a>
            </li>
            
            
            
            <li>
              <a href="" title="Promo Text"> 
                <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/vedaone_2_sectionleft_1_500x500.jpg?v=1496208810" alt="Promo Text" title="Promo Text" /> 
              </a>
            </li>
            
            
            
            
          </ul>
        </div>
        
        




      </div>
    </div>

  </div>
  <div class="grid__item wide--three-quarters post-large--three-quarters large--three-quarters second"> 
    <div class="collection-list">
      <div class="grid-uniform grid-link__container col-main">  
       

        
        <header class="section-header section-header--large">
<!--   <div class="toolbar">
    
    <div class="view-mode grid__item wide--two-tenths post-large--two-tenths large--two-tenths">
    <a class="grid active" href="grid" title="Grid view"><span class="fa fa-th-large" aria-hidden="true"></span></a>  
    <a class="list" href="list" title="List view" ><span class="fa fa-th-list" aria-hidden="true"></span></a>  
  </div>
    

    
    <div class="grid__item wide--eight-tenths post-large--eight-tenths large--eight-tenths">
  <div class="filter-sortby grid__item wide--six-tenths post-large--six-tenths large--six-tenths">
    <label for="sort-by">Sort by</label> 
    <input type="text" id="sort-by">
    <div class="sorting-section">
    <button class="btn dropdown-toggle" data-toggle="dropdown">
      <i class="icon-exchange"></i>
      <span>Featured</span>
      <i class="icon-chevron-down"></i>
    </button>

    <ul class="dropdown-menu" role="menu">
      <li class="active"><a href="manual">Featured</a></li>
      <li><a href="price-ascending">Price, low to high</a></li>
      <li><a href="price-descending">Price, high to low</a></li>
      <li><a href="title-ascending"> A-Z</a></li>
      <li><a href="title-descending">Z-A</a></li>
      <li><a href="created-ascending">Date, old to new</a></li>
      <li><a href="created-descending">Date, new to old</a></li>
      <li><a href="best-selling">Best Selling</a></li>
    </ul>
    </div>
      </div>
  <div class="filter-show grid__item wide--four-tenths post-large--four-tenths large--four-tenths">
    <label>Show</label>
    <div class="pages_list">
    <button class="btn dropdown-toggle" data-toggle="dropdown">
      <i class="icon-exchange"></i>
      
      <span>12</span>
      
      <i class="icon-chevron-down"></i>
    </button>

    <ul class="dropdown-menu" role="menu">
      
      
      <li ><a href="12">12</a></li>
      <li ><a href="24">24</a></li>
      <li ><a href="36">36</a></li>
      <li ><a href="all">All</a></li>         
    </ul>
    </div>
  </div>
    </div>
  
  </div> -->
</header>



<ul class="products-grid-view">
  
  
  


  @if (count($allProducts))
  @foreach($allProducts as $products)

  <li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item  medium--one-half small--one-half  " id="product-10109317582" >
    <div class="products">
      <div class="product-container">  
        <a href="/product-detail/{{$products->id}}" class="grid-link">       
          <div class="ImageOverlayCa"></div>      
          <img src="/uploads/products/{{$products->image1}}" class="featured-image" alt="Classic Prong Set Ring Mount">
        </a>
        <div class="product-button">  




         <div class="add-to-wishlist">     
          <div class="show">
            <form  action="/cart" method="post">     
              @csrf  
              <input type="hidden" name="product_id" value="{{$products->id}}">
              <input type="hidden" name="productPrice" value="100">
              <input type="hidden" name="weight" value="1">
              <div class="added-wishbutton-barry-gold-bangle-for-her loading"><button style="background: #ffffff;font-size: 8px" type="submit" class="add-in-wishlist-js btn" >Add to Cart</button></div>
            </form>
            <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
            <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
            <div class="added-wishbutton-barry-gold-bangle-for-her loading"><a class="added-wishlist btn add_to_wishlist" href="#" data-toggle="modal" data-target="#productsModal{{$products->id}}"><span style="font-size: 8px">Custom Order</span><span class="tooltip-label">Custom Order</span></a></div>
          </div>
        </div>


      </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/product-detail/{{$products->id}}" class="grid-link__title">{{$products->name}}</a>     
      <div class="grid-link__meta">
        <div class="product_price">   
          <div class="grid-link__org_price">
            <span class=money><?php echo var_export(substr($products->description,0,60),true)?></span>
          </div>
        </div>      
        <span class="shopify-product-reviews-badge" data-id="10109317582"></span>
      </div>
      <ul class="item-swatch color_swatch_Value">  
      </ul>             
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="productsModal{{$products->id}}" role="dialog">
    <div class="modal-dialog">
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Custom Order</h4>
        </div>
        <div class="modal-body1">
          @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          @if (Session::has('success'))
          <div class="alert alert-success text-center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p>{{ Session::get('success') }}</p>
          </div>
          @endif
          <form action="/custom-order/{{$products->id}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="custom-order">
              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <p class="card-description"> Please Fill the form to get the custom order inquiry. </p>
                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" class="form-control" required name="name" placeholder="Please enter your name" aria-label="name">
                    </div>
                    <div class="form-group">
                      <label>Phone Number</label>
                      <input type="number" required class="form-control form-control-lg" name="phone" placeholder="please enter your phone number" aria-label="phone">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" required class="form-control form-control-sm" name="mail" placeholder="please enter your email" aria-label="mail">
                    </div>
                    <div class="form-group">
                      <label>Weight [Minimum {{$products->minimum_weight}} In Tola]</label>
                      <input type="number" required class="form-control form-control-lg" id="customWeight1" name="myWeight" min="{{$products->minimum_weight}}" placeholder="please enter your custom weight" aria-label="weight">
                    </div>
                    <div class="form-group">
                      <label>Size</label>
                      <input type="text" required class="form-control form-control-lg" name="size" placeholder="please enter the size" aria-label="size">
                    </div>
                    <input type="hidden" name="product_name" value="{{$products->name}}">
                  </div>
                </div>
              </div>
              <div class="col-md-6 grid-margin stretch-card">
                <div class="product-infor">
                  <div>
                    <img src="/uploads/products/{{$products->image1}}" alt="{{$products->name}}">
                  </div>
                  <p class="product-inventory order-customize"><br>
                    <label>Product Name:  </label> <br>             
                    {{$products->name}}
                  </p>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              &nbsp;&nbsp; <button style="float: left;" type="submit" class="btn btn-primary mr-2">Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>

    </div>
    
  </div>
</li>
@endforeach
@else
<div class="text-center">
<h5>No Products Available Under This Category.</h5>
<a href="/collections"><i class="fa fa-angle-left" aria-hidden="true"></i>
Continue To Other Category</a>
</div>
@endif

</ul>



</div>
</div>
</div>



</div>       

</div>


<div class="dt-sc-hr-invisible-large"></div>

</main>
@endsection