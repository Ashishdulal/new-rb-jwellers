@extends('layouts.frontend.app')
@section('content')
    <main class="main-content"> 
      
       
      

<nav class="breadcrumb" aria-label="breadcrumbs">


  
  <h1>About</h1>
  <a href="/" title="Back to the frontpage">Home</a>

  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
  <span>About</span>

  
</nav>
 
        
      
      
        <div class="grid__item">         
          <div class="grid">
  <div class="grid__item">  
      <div id="shopify-section-about" class="shopify-section">

<div data-section-id="about"  data-section-type="About section" class="about-us">
  
  <div class="invisible-large"></div>
  <div class="grid-uniform section-one">
    <div class="container">
      <div class="grid__item">
        
        <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small--grid__item">
          <div class="section-two-leftimg">
            
            <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/ab1.jpg?v=1496240354" alt="" />
             
          </div>
        </div>
       
        <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small--grid__item">
             <h2 style="color:#000">Sanskar Silver</h2> 
          
          <p style="color:#000">Quisque volutpat mattis eros. Nullam malesuada erat ut ki diaml ka dhuddu pochu turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede. Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
          
          <div class="grid__item">
            <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small--grid__item">
              <ul>
                  <li><span class="fa fa-caret-right"></span>Corporate Videos </li> 
                 <li><span class="fa fa-caret-right"></span>Corporate Videos </li>
              </ul>
            </div>
            <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small--grid__item">
              <ul>
                 <li><span class="fa fa-caret-right"></span>Corporate Videos</li>
                  <li><span class="fa fa-caret-right"></span>Corporate Videos</li>
              </ul>
            </div>
          </div>
          
          <a class="button" href="">Read more<span ><i class="fa fa-arrow-circle-right"></i></span></a>
           
        </div>
      </div>
    </div>
  </div>
  <div class="invisible-large"></div>
   

  
  <div class="grid-uniform section-three">
    <div class="container">
      
      <div class="main-heading">
        <h5>  We have the positive feedback</h5>
        
        <h2>  tab section</h2>
        
      </div>
      
      <div class="grid__item">
        <div class="grid__item post-large--one-half">
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>Will there be a charge for a new conusltation ?</h4>
              
              
              <p>Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
        <div class="grid__item post-large--one-half">  
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>Will there be a charge for a new conusltation ?</h4>
              
              
              <p>Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>
  <div class="invisible-large"></div>
  


  
  <div class="grid-uniform section-four" style="background-image:url(//cdn.shopify.com/s/files/1/1811/9385/files/parallax-models1_2048x2048_d3a22882-81c1-49ed-bde2-1b874de3f64f_2048x2048.jpg?v=1496216944)"; >
    <div class="container">
      
      <div class="main-title">
        <h2>  our services</h2>
        <div class="title-sep"></div>
      </div>
      
      <div class="grid__item ">
        <div class="grid__item wide--two-thirds post-large--two-thirds large--two-thirds">
          
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.</p>
          <div class="invisible-medium"></div>
          
          <div class="grid__item ">
            
            <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item">
              
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/ab2_1024x1024.jpg?v=1496240376" alt="" title="" /> 
              
            </div>


            <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item">
              
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/ab3_1024x1024.jpg?v=1496240380" alt="" title="" /> 
              
            </div>
            
            <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item">
            
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/ab4_1024x1024.jpg?v=1496240388" alt="" title="" /> 
              
            </div>
          </div>
        </div>   
        
        <div class="grid__item wide--one-third post-large--one-third large--one-third">
          
          <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/anotherimg_669307cb-1f11-4e77-ab3d-97248a56db78_1024x1024.jpg?v=1496308448" alt="" title="" /> 
          
        </div>
      </div>
    </div>
  </div>
  <div class="invisible-large"></div>
  


  
  <div class="grid-uniform section-two"   style="background-image:url(//cdn.shopify.com/s/files/1/1811/9385/files/parallax-models1_2048x2048_537b5819-61b9-40ba-a24e-19b2111556b3_2048x2048.jpg?v=1496216959)"; >
    <div class="container">
      <div class="grid__item">
       
        <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small--grid__item">
           <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/ab6_1024x1024.jpg?v=1496240426" alt="" title="" />
           
        </div>
       
        <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small--grid__item">
          
          <h2>  GET A NEW LOOK</h2>
          
          <ul>
            
            <li>  Fashion Men and Women</li>
            
            
            <li>  Non-Union Male and Female Actors</li>
            
            
            <li>  Union Male and Female Actors</li>
            
            
            <li>  Runway</li>
            
            
            <li>  Specialty Sizes</li>
            
            
            <li>  Lifestyle Men and Women</li>
            
            
            <li>  Fashion Men and Women</li>
            
            
            <li>  Fashion Men and Women</li>
            
            
            <li>  Union Male and Female Actors</li>
            
            
            <li>  Lifestyle Men and Women</li>
            
          </ul>
          
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          
        </div>
      </div>
    </div>
  </div>
  <div class="invisible-large"></div>
  


  
  <div class="grid-uniform section-six "  style="background-image:url(//cdn.shopify.com/s/files/1/1811/9385/files/strip-bg_2048x2048.jpg?v=1496481821);background-repeat:no-repeat";>
    <div class="container">
      <div class="grid__item">
        
        <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-third small--grid__item">
          <p>The best website you will come accross with all the necessary features you will ever need cccc features you will ever need cccc features you</p>
        </div> 
        
        <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-third small--grid__item center-img">
                  

          <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/about-logo_1024x1024.png?v=1496479653" alt="" title="" /> 
         
          
          
           
        </div>
        
        
        <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-third small--grid__item">
          <a href="read more" class="btn">View help videos</a>
        </div>
        
      </div>
    </div>
  </div>
  <!-- <div class="invisible-large"></div> -->
  

  
  <!-- <div class="grid-uniform section-five">
    <div class="container">
      
      <div class="main-heading">
        <h5>  Best in Profession</h5>
        
        <h2>  our team</h2>
        
      </div>
      
      <div class="grid__item">
          
                 
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
          <div class="team-wrapper">
            
            <div class="team-img">
              
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/teeem4_8ea2b719-4915-41b5-9a47-927da8505180_1024x1024.jpg?v=1496308619" alt="" title="" /> 
              
              
            </div>
            
            <div class="team-detail">
              
              <h5>MARC COPPOLO</h5>
              

              
              <h6>Associate Crime</h6>
              
              <ul class="social-icon">
                
                <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul> 
            </div>
          </div>
        </div>        
                 
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
          <div class="team-wrapper">
            
            <div class="team-img">
              
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/team12_1024x1024.jpg?v=1496308604" alt="" title="" /> 
              
              
            </div>
            
            <div class="team-detail">
              
              <h5>REVATHY MENAN</h5>
              

              
              <h6>Associate Crime</h6>
              
              <ul class="social-icon">
                
                <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul> 
            </div>
          </div>
        </div>        
                 
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
          <div class="team-wrapper">
            
            <div class="team-img">
              
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/teeeem2_0db8b427-0972-442c-bc4c-6872e51990fd_1024x1024.jpg?v=1496308578" alt="" title="" /> 
              
              
            </div>
            
            <div class="team-detail">
              
              <h5>DOVIN VINCENT</h5>
              

              
              <h6>Associate Economics</h6>
              
              <ul class="social-icon">
                
                <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul> 
            </div>
          </div>
        </div>        
                 
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
          <div class="team-wrapper">
            
            <div class="team-img">
              
              <img src="http:////cdn.shopify.com/s/files/1/1811/9385/files/teeeem3_bd38b65c-dd66-4b25-aefd-5ce527a950c5_1024x1024.jpg?v=1496308554" alt="" title="" /> 
              
              
            </div>
            
            <div class="team-detail">
              
              <h5>JAMES WAN</h5>
              

              
              <h6>Project Manager</h6>
              
              <ul class="social-icon">
                
                <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul> 
            </div>
          </div>
        </div>        
        
        
      </div>
    </div>
  </div> -->
  <div class="invisible-large"></div>
  
</div>
















<style>


  .about-us .button{background: #d2aa5c;color:#ffffff;}

  .about-us .button span {background:#000000; color:#ffffff;}

  .about-us .section-one li{color:#000;}

  .about-us .button:hover{background:#000;color: !important;}

  .about-us .main-heading h2{color:#000;}

  .about-us .main-heading h5{color:#d2aa5c;}

  .about-us .tab-detail p{color:#000;}

  .about-us .tab-detail  h4{color:#000000;}

  .about-us .tab-icon span{color:#d2aa5c;}

  .about-us  .main-title h2{color:#000;}

  .about-us  .main-title .title-sep::after { background:#d2aa5c;}

  .about-us  .main-title .title-sep { background:#d2aa5c;}

  .about-us .section-four p{color:#000;}

  .about-us .btn { border-radius: 5px;box-shadow: 3px 3px 0 0 #d2aa5c;background:#ffffff;color:#d2aa5c;}

  .about-us .btn:hover{color:#ffffff;background:#d2aa5c;box-shadow:3px 3px 0 0 #ffffff;}

  .about-us .team-wrapper {border: 1px solid #b2b2b2;}

  .about-us .team-wrapper li a{color:#b2b2b2;}

  .about-us .team-wrapper h6{color:#b2b2b2;}

  .about-us .team-wrapper h6::after {background: #d2aa5c ;}

  .about-us .team-detail h5{color:#000;}

  .about-us .team-wrapper li a:hover{color:#d2aa5c;}

  .about-us .team-wrapper:hover{border:1px solid #d2aa5c;}

  .about-us .team-wrapper:hover h5{color:#d2aa5c;}

  .about-us .section-two h2{color:#000;}

  .about-us .section-two li, .about-us .section-two li::after{color:#000;}

  .about-us .section-two p{color:#000;}

  .about-us .section-six p{color:#ffffff;}

</style>



























</div>
  </div>
</div>
              
        </div>       
        
      
    </main>
@endsection