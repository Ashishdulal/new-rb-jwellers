@extends('layouts.frontend.app')

@section('content')

<main class="main-content"> 
  <nav class="breadcrumb" aria-label="breadcrumbs">
    <h1>Your Shopping Cart</h1>
    <a href="/" title="Back to the frontpage">Home</a>

    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    <span>Your Shopping Cart</span>
  </nav>
  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">

    <div class="grid__item">         


      @if(count($cartContents))

      <a style="float: right;margin-bottom: 5px;" onclick="makeWarning(event)" href="/cart/clear" name="checkout" class="btn">Clear All</a>
      <div class="cart__row cart__header-labels">
        <div class="grid--full">
          <div class="grid__item wide--three-tenths post-large--three-tenths large--three-tenths medium--grid__item">
            <div class="grid">
              <div class="grid__item">

               <span class="h4">Product</span>
             </div>
           </div>
         </div>

         <div class="grid__item wide--seven-tenths post-large--seven-tenths large--seven-tenths medium--grid__item">
          <div class="grid--full">
            <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-fifth">
              <span class="h4 cart__mini-labels">Price</span>
            </div>
            <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-fifth">
              <span class="h4 cart__mini-labels">Quantity</span>
            </div>
            <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-fifth">
              <span class="h4 cart__mini-labels">Total</span>
            </div>
            <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-fifth">
              <span class="h4 cart__mini-labels">Remove</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    @foreach($cartContents as $cartContent)

    <form action="/cart/update/{{ $cartContent->id }}" method="post"  class="cart">

      @csrf

      <div class="cart__row">
        <div class="grid--full cart__row--table-large">
          <div class="grid__item wide--three-tenths post-large--three-tenths large--three-tenths medium--grid__item">
            <div class="grid cart_items">
              <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item">
                <a href="/collections/all/products/lush-ring-mount?variant=37510472974" class="cart__image">
                  <img src="/uploads/products/{{ $cartContent->attributes[1] }}" alt="{{ $cartContent->name }}">
                </a>
              </div>
              <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item cart-title">
                <a href="/product-detail/{{ $cartContent->id}}" class="h5">
                  {{ $cartContent->name }}
                </a>
                <br>
                <small>{{ $cartContent->attributes[0] }} / Silver</small>
              </div>
            </div>
          </div>
          
          <div class="grid__item wide--seven-tenths post-large--seven-tenths large--seven-tenths medium--grid__item">
            <div class="grid--full cart__row--table-large">

              <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-quarter small--one-quarter">
                <span class="h5 cart__large-labels">Price</span>
                <span class="h5"><span class=money>Rs.{{ $cartContent->price }}</span></span>
                <input type="hidden" name="price"  value="{{ $cartContent->price }}" id="singlePrice{{$cartContent->id}}" readonly="">
              </div>

              <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-quarter small--one-quarter text-center">
                <span class="h5 cart__large-labels">Quantity</span>
                <div class="qty-box-set">
                  <input type="button" value="-" class="{{$cartContent->id}} qtyminus1">
                  <input type="number" class="quantity-selector cart-number"
                  name="updates[]" value="{{ $cartContent->quantity }}" min="1" id="quantity{{ $cartContent->id }}" >
                  <input type="button" value="+" class="{{$cartContent->id}} qtyplus1">
                </div>
              </div>

              <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-quarter small--one-quarter text-center">
                <span class="h5 cart__large-labels">Total</span>
                <span class="h5">
                  Rs.
                  <span class="money" id="totalMoneyEach{{$cartContent->id}}">{{ $cartContent->price * $cartContent->quantity }}</span>
                </span>
                
              </div>
              <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-quarter small--one-quarter text-center">
                <span class="h5 cart__large-labels">Remove</span>

                <a href="/cart/remove/{{$cartContent->id}}" onclick="makeWarning(event)" class="btn update-cart">Delete
                  <span><!-- <i class="fa fa-trash"></i> --></span>
                </a>
                <input type="submit" name="update" class="btn update-cart" value="Update Cart">
              </div>
            </div>
          </div>

        </div>
      </div>



      <div class="cart__row">
        <div class="grid shipping-section">

        </div>

      </div>
    </form>
    @endforeach
    <div class="cart__row">
      <div class="grid shipping-section">

        <div class="grid__item wide--one-half post-large--one-half large--one-half">
            <!-- <button type="button" class="text-link cart__note-add">
              Add a note to your order
            </button> -->
            <div class="cart__note">
              <label for="CartSpecialInstructions">Special instructions for seller</label>
              <textarea name="note" class="input-full" id="CartSpecialInstructions"></textarea>
            </div>
          </div>

          <div class="grid__item text-right wide--one-half post-large--one-half large--one-half">
            <p class="cart_total_price" style="margin:0;">
              <span class="cart__subtotal-title h5">Subtotal</span>
              <span class="h5 cart__subtotal"><span class=money>Rs.{{ Cart::getSubTotal() }}</span></span>

            </p>
            <p class="shopping-checkout"><em>Shipping, taxes, and discounts will be calculated at checkout.</em></p>
            <div class="cart_btn">
              <a class="btn" href="/collections">Continue shopping</a>

              <a href="/checkout" name="checkout" class="btn">Check Out</a>
            </div>
          </div>
        </div>

      </div>
      @else
      <h3>Your Cart</h3>
      <p>Your cart is currently empty!</p>
      <a href="/collections"><p>Continue Shopping</p></a>
      @endif
      <script>
        $('.qtyplus1').click(function(e){
          e.preventDefault();
          var currentVal = parseInt($(this).parent().find('input[name="updates[]"]').val());
          if (!isNaN(currentVal)) {
            $(this).parent().find('input[name="updates[]"]').val(currentVal + 1);
          } else {
            $(this).parent().find('input[name="updates[]"]').val(1);
          }
          updatePrice(e, e.target.classList[0]);
        });

        $(".qtyminus1").click(function(e) {

          e.preventDefault();
          var currentVal = parseInt($(this).parent().find('input[name="updates[]"]').val());
          if (!isNaN(currentVal) && currentVal > 1) {
            $(this).parent().find('input[name="updates[]"]').val(currentVal - 1);
          } else {
            $(this).parent().find('input[name="updates[]"]').val(1);
          }
          updatePrice(e, e.target.classList[0]);
        });
      </script>


    </div>       

  </div>


  <div class="dt-sc-hr-invisible-large"></div>

</main>
@endsection
<script type="text/javascript">
  function updatePrice(evt, id){
    var quantity = document.getElementById('quantity' + id).value;;
    var price = document.getElementById('singlePrice' + id).value;
    var totalPrice = parseInt(quantity) * parseInt(price);
    document.getElementById('totalMoneyEach' + id).innerHTML = totalPrice;
  }
</script>