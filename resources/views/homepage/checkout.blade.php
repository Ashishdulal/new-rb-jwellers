@extends('layouts.frontend.app')
@section('styles')
<style>
ul.pathLayout li {
    padding-right: 7px;
}
ul.pathLayout .fa.fa-chevron-right {
    font-size: 0.8em;
}
ul.pathLayout {
    display: flex;
    list-style: none;
    font-size: 0.878em;
    padding: 15px 0px;
}
.form-group.occupyHalf {
    max-width: 49%;
    min-width: 49%;
    display: inline-block;
}
.mr-8 {
    margin-right: 8px;
}
.deliveryDetails input, .deliveryDetails select {
    padding: 22px 20px;
}
.nmb input {
    margin-bottom: 15px;
}
.nmb {
    margin-bottom: 0px;
}
.formFooter button {
    float: right;
}

section.sidebar {
    border-left: 1px solid #d8d8d8;
    background: #fafafa;
    padding: 80px 60px;
}
.deliveryDetails {
    padding: 80px 30px;
}
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc; 
}

.panel {
  padding: 0 18px;
  display: none;
  background-color: white;
  overflow: hidden;
}
button.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}
button.accordion.active:after {
    content: "\2212";
}
.form-width {
    width: 100% !important;
}
.formFooter {
    clear: right;
    padding: 10px 0;
}
</style>
@endsection
@section('content')

<section class="checkoutPage">
  <div class="container">
    <div class="col-sm-7 deliveryDetails">
<!--       <div class="logoHeader">
        <div class="logoHolder">
          <img src="/images/logo.png" alt="logo">
        </div>
        <ul class="pathLayout">
          <li>Cart&nbsp;<i class="fa fa-chevron-right"></i></li>
          <li><strong>Information</strong>&nbsp;<i class="fa fa-chevron-right"></i></li>
          <li>Shipping&nbsp;<i class="fa fa-chevron-right"></i></li>
          <li>Payment</li>
        </ul>
      </div> -->
      <h4>Checkout</h4>
      <div>
      @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
   
        @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>
        <button class="accordion active">Welcome</button>
<div style="display: block;" class="panel">
  @if($user)
  <div id="CustomerLoginForm">
    <p style="text-transform: capitalize;">Hello, {{$user->name}}</p>
    <p><b>Choose The Payment Method to Continue</b></p>
  </div>
  @else
  <div id="CustomerLoginForm">

      <form method="POST" action="{{ route('login') }}" id="customer_login" accept-charset="UTF-8">
        @csrf
        <input type="hidden" name="form_type" value="customer_login" />
        <input type="hidden" name="utf8" value="✓" />

        

        
        <div class="form-group">
        <label for="CustomerEmail" class="label--hidden">Email</label>
        <input id="email" type="email" class="form-control form-width @error('email') is-invalid @enderror form-control-lg" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail Address">
                @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
        </div>
        

        <div class="form-group">
          <label for="CustomerPassword" class="label--hidden">Password</label>
          <input id="password" type="password" class="form-control form-width form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
           @error('password')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span>
                  @enderror
          </div>

          <p>
            @if (Route::has('password.request'))
                                    <a class="auth-link text-black" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
            <a href="#" onclick="showRecoverPasswordForm();return false;">Forgot your password?</a>
          </p>

        

        <p>
          <input type="submit" class="btn" value="Sign In">
        </p>
        
          <p>
            <a href="/register" id="customer_register_link">Create account</a>
          </p>        
        <a href="#orderDetail">Continue As Guest</a>

      </form>
    </div>
      <div id="RecoverPasswordForm" style="display: none;">

      <div class="section-header section-header--small">
        <h2 class="section-header__title">Reset your password</h2>
      </div>
      <p>We will send you an email to reset your password.</p>

      
      <form method="post" action="/account/recover" accept-charset="UTF-8">
        <input type="hidden" name="form_type" value="recover_customer_password" />
        <input type="hidden" name="utf8" value="✓" />
        <label for="RecoverEmail" class="label--hidden">Email</label>
        <input type="email" value="" name="email" class="form-control form-width" id="RecoverEmail" placeholder="Email" autocorrect="off" autocapitalize="off">

        <p>
          <input type="submit" class="btn" value="Submit">
        </p>
        <a href="#" onclick="hideRecoverPasswordForm();return false;">Cancel</a>
      </form>

    </div>
    @endif

</div>

<button id="orderDetail" class="accordion">Pay with Stripe</button>
<div class="panel">
 <form action="/checkout/confirm" method="POST">
      @csrf
      <!-- <input type="hidden" name="product_id" value="1">
      <input type="hidden" name="product_quantity" value="1">
      <input type="hidden" name="product_price" value="{{$total}}"> -->
          <p>Billing Details / Shipping address <span style="display:none;">Already have an account? <a href="/login">Log in</a></span></p>
          <div class="form-group">
            <input type="email" name="email" 
                    placeholder="Email" required 
                    class="form-control" />
          </div>
          
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control"
                    placeholder="First Name" required name="first_name"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control" 
                    placeholder="Last Name" required name="last_name"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="number" min=0 class="form-control"
                    placeholder="Phone Number" required name="phone" />
            </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control"
                    placeholder="Address" required name="address" />
            </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control" 
                    placeholder="Apartment, Suite, etc." required name="apartment"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control" 
                    placeholder="City" required name="city"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control"
                    placeholder="Postal Code" required name="postal_code" />
          </div>
          <div class="formFooter">
            <!-- <a href="/cart"><i class="fa fa-chevron-left"></i>&nbsp;Return to Cart</a> -->
            <button type="submit" class="step__footer__continue-btn btn">Pay with Stripe</button>
          </div>
          
      </form>
</div>
<button id="orderDetail" class="accordion">Pay With PayPal</button>
<div class="panel">
 <form action="/checkout/payPal" method="POST">
      @csrf
            <input type="hidden" name="order_id" value="1">
      <input type="hidden" name="title" value="hello World">
      <input type="hidden" name="amount" value="{{ $total }}">
      <!-- <input type="hidden" name="product_id" value="1">
      <input type="hidden" name="product_quantity" value="1">
      <input type="hidden" name="product_price" value="{{$total}}"> -->
          <p>Billing Details / Shipping address <span style="display:none;">Already have an account? <a href="/login">Log in</a></span></p>
          <div class="form-group">
            <input type="email" name="email" 
                    placeholder="Email" required 
                    class="form-control" />
          </div>
          
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control"
                    placeholder="First Name" required name="first_name"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control" 
                    placeholder="Last Name" required name="last_name"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="number" min=0 class="form-control"
                    placeholder="Phone Number" required name="phone" />
            </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control"
                    placeholder="Address" required name="address" />
            </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control" 
                    placeholder="Apartment, Suite, etc." required name="apartment"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control" 
                    placeholder="City" required name="city"/>
          </div>
          <div class="form-group nmb occupyHalf">
            <input type="text" class="form-control"
                    placeholder="Postal Code" required name="postal_code" />
          </div>
        <div class="formFooter">
            <button type="submit" class="step__footer__continue-btn btn">Pay with PayPal</button>
        </div>
      </form>
</div>
<div class="formFooter">
            <a href="/cart"><i class="fa fa-chevron-left"></i>&nbsp;Return to Cart</a>
          </div>
    </div>
    <div class="col-sm-5 existingCart">
      <section class="sidebar">
        <div class="cartItems">
        <h5>Your Order</h5>
        <table class="table ">
                  <div class="container">
                    <thead>
                      <tr>
                        <th>Id</th>                     
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($cartContents))
                      @foreach($cartContents as $cartProd)
                      <tr>
                       <td>{{ $loop->iteration }}</td>
                       <td>{{$cartProd->name}}</td>
                      <td>{{$cartProd->quantity}}</td>
                      <td>Rs. {{$cartProd->price}}</td>
                      <td>
                        <a href="/cart/remove/{{$cartProd->id}}" onclick="makeWarning(event)" class=" update-cart"><span><i class="fa fa-trash"></i></span></a>
                      </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td style="border: none;">(No</td>
                      <td style="border: none;">Products)</td></tr>
                    @endif
                  </tbody>
                </div>
              </table>
        </div>
        <div class="subtotal">
        Pricing of the product will appear here
        </div><br>
        <div class="total">
          <span class="bigPara">Total</span>
          <p><span>NPR.</span> {{ $total }}</p>
        </div>
            <a href="/cart"><i class="fa fa-chevron-left"></i>&nbsp;Return to Cart</a>
      </section>
    </div>
  </div>
</section>
<script>
  /*
    Show/hide the recover password form when requested.
  */
  function showRecoverPasswordForm() {
    document.getElementById('RecoverPasswordForm').style.display = 'block';
    document.getElementById('CustomerLoginForm').style.display='none';
  }

  function hideRecoverPasswordForm() {
    document.getElementById('RecoverPasswordForm').style.display = 'none';
    document.getElementById('CustomerLoginForm').style.display = 'block';
  }

  // Allow deep linking to the recover password form
  if (window.location.hash == '#recover') { showRecoverPasswordForm() }

  // reset_success is only true when the reset form is
  
</script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
  <!-- <script>
        var acc = document.getElementsByClassName("accordion");
        for (var i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                //  this.classList.toggle("active");
                var allPanels = document.getElementsByClassName("panel");
                var panel = this.nextElementSibling;

                for (var i = 0, j = allPanels.length; i < j; i++) {
                    if (allPanels[i].style.display == "block") {
                        allPanels[i].style.display = "none"
                    }
                }
                panel.style.display = "block";

            });
        }
</script>
 -->
@endsection