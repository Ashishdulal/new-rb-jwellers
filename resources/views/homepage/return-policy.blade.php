@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 
<nav class="breadcrumb" aria-label="breadcrumbs">


  
  <h1>Return Policy</h1>
  <a href="/" title="Back to the frontpage">Home</a>

  <span aria-hidden="true" class="breadcrumb__sep">/</span>
  <span>Return Policy</span>

  
</nav>   

  
  <div class="grid-uniform section-three">
    <div class="container">
      <div class="grid__item">
        <div class="grid__item post-large--one-half">
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>1 Will there be a charge for a new conusltation ?</h4>
              
              
              <p>1 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              <p>3 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
        <div class="grid__item post-large--one-half">  
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>2 Will there be a charge for a new conusltation ?</h4>
              
              
              <p>2 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              <p>3 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>

  <div class="grid-uniform section-three">
    <div class="container">
      <div class="grid__item">
        <div class="grid__item post-large--one-half">
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>3 Will there be a charge for a new conusltation ?</h4>
              
              
              <p>3 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              <p>3 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
        <div class="grid__item post-large--one-half">  
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>4Will there be a charge for a new conusltation ?</h4>
              
              
              <p>4 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              <p>3 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>
  <div class="grid-uniform section-three">
    <div class="container">
      <div class="grid__item">
        <div class="grid__item post-large--one-half">
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>3 Will there be a charge for a new conusltation ?</h4>
              
              
              <p>3 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
        <div class="grid__item post-large--one-half">  
          <div class="tab-section">
            
            <div class="tab-icon">
              <span class="fa fa-file-text-o"> </span>
            </div>
            
            <div class="tab-detail">
              
              <h4>4Will there be a charge for a new conusltation ?</h4>
              
              
              <p>4 Etiam volutpat gravida odio, finibus tincidunt odio tempor id. Nullam mollis dui lobortis, sagittis magna non, pharetra turpis. Nulla vestibulum gravida risus, sit amet sodales lorem malesuada vel. Aenean porta nec odio ut interdum. Ut pharetra porttitor neque. Maecenas a suscipit velit, vitae elementum lectus. In lectus leo, ullamcorper ut egestas eget, congue vitae dolor.</p>
              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>
  <div class="invisible-large"></div>
</main>
@endsection