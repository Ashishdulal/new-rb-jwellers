@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




  <nav class="breadcrumb" aria-label="breadcrumbs">




    <h1>All Ethnics</h1>


  </nav>




  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">

    <div class="grid__item">         
      <div class="grid-uniform list-collection-products">  

        @foreach($allethnics as $ethnic)

         <?php $count = 0;?>
            @foreach($alleve as $events)
              @if($ethnic->id == $events->ethnic_id)
                <?php $count = $count + 1; ?>
              @endif
            @endforeach

        @if($count)
        <div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">


          <a href="/ethnics/{{$ethnic->id}}" title="{{$ethnic->name}}" class="pickgradient grid-link">

            <img src="/uploads/products/ethnicity/{{$ethnic->image}}" alt="{{$ethnic->name}}" />

            <div class="dt-sc-event-overlay">

              <p class="collection-count">
                
                {{ $count }}
                <?php $count = 0; ?>
                <span>Items</span>
              </p>


            </div>

          </a>


          <a href="/ethnics/{{$ethnic->id}}" title="Browse our Rare collections collection" class="grid-link">

            <span class="grid-link__title">{{$ethnic->name}}</span></a>

          </div>
          @endif

          @endforeach

        </div>              
      </div>       

    </div>


    <div class="dt-sc-hr-invisible-large"></div>

  </main>
  @endsection