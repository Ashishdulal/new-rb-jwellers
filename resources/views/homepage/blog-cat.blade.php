@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 
      
       
      

<nav class="breadcrumb" aria-label="breadcrumbs">


  
  <h1>News</h1>
  <a href="/" title="Back to the frontpage">Home</a>

  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    <a href="/blogs" title="Back to the frontpage">Blogs</a>

  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>

  <span style="text-transform: capitalize;">{{$blogCatName}}</span>

  

  
</nav>
 
        
      
      
      <div class="dt-sc-hr-invisible-large"></div>
      <div class="container-bg">
        
        <div class="grid__item">         
          

<div class="blog-design-4">
  <div class="blog-section">
    <aside class="sidebar left-sidebar grid__item wide--one-quarter post-large--one-quarter large--one-quarter">
      

<div class="widget recent_article">
  <h4>Recent Articles<span class="right"></span></h4>
<ul>
<?php $count=1; ?>
 @if(count($blogs))
                  @foreach($blogs as $blog)
   <li>
     
     
     <div class="artical-image">
       
       <a href="/blog-detail/{{$blog->id}}" title=""><img src="/uploads/blogs/{{$blog->f_image}}" alt="{{$blog->title}}" class="article__image" /></a>
     </div>
     
     
     <div class="article-detail">
       <h5>
         <a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></h5>
       
       
       <p><?php echo ($blog->description )?></p>
       
       
     </div>
   </li>            <?php $count ++; ?>
            <?php if($count > 3)
            break
            ?>
@endforeach
                  @else
                  <h5>There are no posts in Blog.</h5>
                  @endif

 


</ul>
</div>



<div class="widget widget_categories">

  <h4>Categories<span class="right"></span></h4>
  <ul>
    
      @if(count($blogcat))
                  @foreach($blogcat as $bcat)
    <li><i class="fa fa-diamond" aria-hidden="true"></i><a href="/blog-cat/{{$bcat->id}}" title="Show articles tagged Aliquam">{{$bcat->name}}</a></li>
      @endforeach
                  @else
                  <h5>There are no Categories in Blog.</h5>
                  @endif
      
    
  </ul>

</div>

    </aside>    
    <div class="grid__item wide--three-quarters post-large--three-quarters large--three-quarters masswrapper">

      @if(count($blogs))
                  @foreach($blogs as $blog)
                  @if($blog->cat_id == $id)
      <div class="grid__item wide--one-half post-large--one-half large--one-half">
        <div class="article">
          
          
          
          <div class="article-img">
            <a href="/blog-detail/{{$blog->id}}" title=""><img src="/uploads/blogs/{{$blog->f_image}}" alt="uia non numquam eius modi tempora incidunt ut" class="article__image" /></a>
          </div>
          
          
          <div class="blog-description">
            <div class="blogs-sub-title">
              <p class="author">
                            
                <i class="zmdi zmdi-account"></i>
                <span> Admin</span>
                
              </p> 
              
              <p class="blog-date">
                <span data-datetime="2017-05-30"><span class="date">{{ $blog->created_at->format('M d , Y') }}</span></span>             
              </p>
              



            </div>
            <div class="home-blog-content blog-detail">       
              <h4><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></h4>
              
              
              <p><?php echo ($blog->description )?></p>
              
              
              
              
              <!-- <p class="comments-count">0 Comments</p> -->
              


              
              
              <div class="blog-btn">
                <a class="btn" href="/blog-detail/{{$blog->id}}">Read more<i class="fa fa-angle-double-right"></i></a>
              </div>
              
            </div>

          </div>
        </div>
      </div>
      @endif
                  @endforeach
                  @else
                  <h5>There are no posts in Blog.</h5>
                  @endif
      
                
    </div>
    
    
  </div>
</div>


              
        </div>       
        
      </div>
      
      
      <div class="dt-sc-hr-invisible-large"></div>
      
    </main>
    @endsection