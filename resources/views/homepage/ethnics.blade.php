@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




 <nav class="breadcrumb" aria-label="breadcrumbs">


  @if($ethnic_name)
  <h1>{{$ethnic_name}}</h1>
  <a href="/" title="Back to the frontpage">Home</a>

  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
  <span>{{$ethnic_name}}</span>

  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
  <a href="/products/all" title="Back to the frontpage">Products</a>



  @endif

</nav>




<div class="dt-sc-hr-invisible-large"></div>
<div class="container-bg">

  <div class="grid__item">         
    <div itemscope itemtype="http://schema.org/Product">
      <meta itemprop="name" content="{{$ethnicity_all->name}}" />
      <meta itemprop="sku" content="UKL656"/>
      <meta itemprop="gtin14" content=""/>
      <meta itemprop="brand" content="CaratLane"/>
      <meta itemprop="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official."/>
      <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
        <meta itemprop="priceCurrency" content="USD">
        <meta itemprop="price" content="199.00">
        <meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
        <meta itemprop="availability" content="http://schema.org/InStock"/>
      </div>
    </div>


    <div class="product-single">

      <div class="grid__item wide--one-half post-large--one-half large--one-half product-img-box">
        @if($ethnicity_all->cat_id != 6)
        <div class="product-photo-container">

          <a href="/uploads/products/events/{{$ethnicity_all->image}}" >
            <img id="product-featured-image" src="/uploads/products/events/{{$ethnicity_all->image}}" alt="{{$ethnicity_all->name}}" data-zoom-image="/uploads/products/events/{{$ethnicity_all->image}}"/>
          </a>
            </div>
            @else
            <canvas id="myCanvas" width="500" height="500">
              <p>Your browser doesn't support the content! Please switch the browser to view the content.</p>
            </canvas>
            @endif
            <div class="more-view-wrapper-owlslider">
              <ul id="ProductThumbs" class="product-photo-thumbs owl-carousel owl-theme thumbs">
                <li class="">
                  <a href="javascript:void(0)" data-image="/uploads/products/events/{{$ethnicity_all->image}}" data-zoom-image="/uploads/products/events/{{$ethnicity_all->image}}">
                    <img style="height:190px;" src="/uploads/products/events/{{$ethnicity_all->image}}" alt="{{$ethnicity_all->name}}">
                  </a>
                </li>

                @foreach($allProducts as $ethProd)
                <li class="">
                  <a href="javascript:void(1)" data-image="/uploads/products/{{$ethProd->image1}}" data-zoom-image="/uploads/products/{{$ethProd->image1}}">
                    <img style="height:190px;" src="/uploads/products/{{$ethProd->image1}}" alt="{{$ethProd->name}}">
                  </a>
                </li>
                @endforeach

              </ul>
            </div>
            
            
          </div>

          <div class="product_single_detail_section grid__item wide--one-half post-large--one-half large--one-half">
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success text-center">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
              <p>{{ Session::get('success') }}</p>
            </div>
            @endif
            <h2 class="product-single__title">{{$ethnicity_all->name}} Products</h2>

            <div class="">
              <div style="border-bottom: 2px solid black; padding: 120px 0;" class="">
                <form method="post" action="/manyCart" 
                accept-charset="UTF-8" 
                class="product-form">
                @csrf
                <table class="table ">
                  <div class="container">
                    <thead>
                      <tr>
                        <th>Selected<!-- <br>All&nbsp;<input type="checkbox" id="checkall" class="form-check-input checkbox" checked value="All"> --></th>
                        <th>Id</th>                     
                        <th>Name</th>
                        <th>Product Code</th>
                        <th class="make-center">Image</th>
                        <th>Stock</th>
                        <th>Weight</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if($allProducts)
                      @foreach($allProducts as $ethProd)
                      <input type="hidden" id="product_id" name="product_id[]" value="{{$ethProd->id}}">
                      <input type="hidden" id="price" name="name[]" value="{{$ethProd->name}}">
                      <input type="hidden" id="weight" name="weight[]" value="{{$ethProd->weight}}">
                      <input type="hidden" id="subTotal" name="subTotal[]" value="{{$ethProd->rate}}">
                      <input type="hidden" id="subTotal" name="images[]" value="{{$ethProd->image1}}">
                      <tr>
                        <td>
                          
                         <input type="hidden" name="selected_products[]" value="1"><input class="checkbox ethnicCheckbox" checked type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value">
                         <!--   <input type="checkbox" class="form-check-input checkbox ethnicCheckbox" checked="" name="selected_products[]" value="1"> -->
                       </td>
                       <td>{{ $loop->iteration }}</td>
                       <td>{{$ethProd->name}}</td>
                       <td>{{$ethProd->product_code}}</td>
                       <td>
                        <div class="row make-center">
                          <div class="col-sm-2" ></div>
                          <div class="col-sm-8 product-display-img">
                            <img class="slider-image" src="/uploads/products/{{$ethProd->image1}}" alt="{{$ethProd->name}}">
                          </div>
                        </div>
                      </td>
                      <td>@if($ethProd->product_stock) In Stock @else Not Available @endif</td>
                      <td>{{$ethProd->weight}}</td>
                      <td>{{$ethProd->rate}}</td>
                      <td><a href="/product-detail/{{$ethProd->id}}">Custom Order</a></td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </div>
              </table>
              <div class="swatch clearfix">
                @if(count($allProducts) )
                <div class="product-single-button">
                  <button type="submit" id="AddToCart" class="btn">
                    <span id="AddToCartText">Add to Cart</span>
                  </button>
                </div>
                @else
                <h5 class="empty_rep">No Products Available</h5>
                @endif
              </div>
            </form>
            <div class="product_price">

              <div class="grid-link__org_price" id="ProductPrice">
                <!-- <span class=money>$199.00</span> -->
              </div>

              
            </div>
          </div>
          <span class="shopify-product-reviews-badge" data-id="10109317582"></span>
          

<!-- 
     <div class="grid__item" style="margin-top:20px;">
      <img src="(image-path)" alt="secure_pay">
    </div> -->



    <div class="share_this_btn">
     <!-- <div class="addthis_toolbox addthis_default_style addthis_32x32_style"  >
      <a class="addthis_button_preferred_1"></a>
      <a class="addthis_button_preferred_2"></a>
      <a class="addthis_button_preferred_3"></a>
      <a class="addthis_button_preferred_4"></a>
      <a class="addthis_button_compact"></a>
      <a class="addthis_counter addthis_bubble_style"></a>
    </div> -->
    <script type='text/javascript'>
      var addthis_product = 'sfy-2.0.2';
      var addthis_plugin_info = {"info_status":"enabled","cms_name":"Shopify","cms_version":null,"plugin_name":"AddThis Sharing Tool","plugin_version":"2.0.2","plugin_mode":"AddThis"};
    var addthis_config     = {/*AddThisShopify_config_begins*/pubid:'xa-525fbbd6215b4f1a', button_style:'style3', services_compact:'', ui_delay:0, ui_click:false, ui_language:'', data_track_clickback:true, data_ga_tracker:'', custom_services:'', custom_services_size:true/*AddThisShopify_config_ends*/};
  </script>
  <script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-525fbbd6215b4f1a'></script>
  <script type='text/javascript'>
   $(document).ready(function(){

   // Changing state of CheckAll checkbox 
   $(".checkbox").change(function(){
    var checkAll = $('#checkall').is(':checked');
    var checkBox = $('.ethnicCheckbox').is(':checked');
    if(checkAll){
     if(checkBox){
       $("#checkall").prop("checked", false);
       $(".myCeckboxVal").val(0);
     }
     else{
       $(".ethnicCheckbox").prop("checked", true);
       $(".myCeckboxVal").val(1);
     }
   }
   
   if($(".checkbox").length == $(".checkbox:checked").length) {
    $("#checkall").prop("checked", true);
    $(".myCeckboxVal").val(1);
  } else {
    $("#checkall").removeAttr("checked");
  }
});

   // Check or Uncheck All checkboxes
   $("#checkall").change(function(){
     var checked = $(this).is(':checked');
     console.log('*****',checked);
     if(checked){
       $(".checkbox").each(function(){
         $(this).prop("checked",true);
         this.previousSibling.value=1-this.previousSibling.value;
       });
     }else{
       $(".checkbox").each(function(){
         $(this).prop("checked",false);
         this.previousSibling.value=1-this.previousSibling.value;
       });
     }
   });


 });
</script>
</div>

<!-- <div class="dt-sc-hr-invisible-large"></div>
<div class="dt-sc-tabs-container">
  <ul class="dt-sc-tabs">
    <li><a href="#"> Description </a></li> 
    <li><a href="#"> Reviews  </a></li>
    <li><a href="#"> Shipping details  </a></li>
  </ul>

  <div class="dt-sc-tabs-content" id="desc_pro">
    <p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui official.</span></p>
  </div>


  <div class="dt-sc-tabs-content">
    <div class="commentlist">
      <div class="comment-text">
        <div class="rating-review">
          <div id="shopify-product-reviews" data-id="10109317582"></div>
        </div>
      </div>
    </div>
  </div>


  <div class="dt-sc-tabs-content">
    <p>Write a few sentences to tell people about your store's shipping policy (the kind of products you sell, your mission, etc). This tab consists general information for all the prducts. This information is not a product specific but store's.</p>
  </div>

</div> -->
</div>

</div>
</main>
@endsection