@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




  <nav class="breadcrumb" aria-label="breadcrumbs">




    <h1>All collections</h1>


  </nav>




  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">

    <div class="grid__item">         
      <div class="grid-uniform list-collection-products">  


       <div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">


        <a href="/products/all" title="Browse our Necklaces collection" class="pickgradient grid-link">

          <img src="/images/all-product-images.jpg" alt="Necklaces" />

          <div class="dt-sc-event-overlay">

            <p class="collection-count">{{$totalProduct}} <span>Items</span></p>


          </div>

        </a>


        <a href="/products/all" title="Browse our Necklaces collection" class="grid-link">

          <span class="grid-link__title">All Products</span></a>

        </div>








        @foreach($productcat as $pcat)
        <?php $count = 0;?>
          @foreach($productTotal as $product)
            @if($pcat->id == $product->cat_id)
              <?php $count = $count + 1; ?>
            @endif
          @endforeach
        @if($count)      
        <div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">


          <a href="/products/{{$pcat->id}}" title="{{$pcat->name}}" class="pickgradient grid-link">

            <img src="/uploads/products/{{$pcat->image}}" alt="{{$pcat->name}}" />

            <div class="dt-sc-event-overlay">

              <p class="collection-count">
                {{ $count }}
                <?php $count = 0; ?>
                <span>Items</span>
              </p>


              </div>

            </a>


            <a href="/products/{{$pcat->id}}" title="Browse our Rare collections collection" class="grid-link">

              <span class="grid-link__title">{{$pcat->name}}</span></a>

            </div>
           @endif 

            @endforeach




<!-- 




<div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">
  

  <a href="/collections/necklaces" title="Browse our Necklaces collection" class="pickgradient grid-link">
        
          <img src="http://cdn.shopify.com/s/files/1/1811/9385/products/4_large.jpg?v=1496145230" alt="Necklaces" />
        
     <div class="dt-sc-event-overlay">
    
    <p class="collection-count">4 <span>Items</span></p>


  </div>

  </a>
  
  
    <a href="/collections/necklaces" title="Browse our Necklaces collection" class="grid-link">

      <span class="grid-link__title">Necklaces</span></a>

</div>

    
  
  
    
      
      
      





<div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">
  

  <a href="/collections/new-arrivals" title="Browse our New Arrivals collection" class="pickgradient grid-link">
        
          <img src="http://cdn.shopify.com/s/files/1/1811/9385/products/1_5c789c84-aba2-4dbb-8992-69166a320cac_large.jpg?v=1496144078" alt="New Arrivals" />
        
     <div class="dt-sc-event-overlay">
    
    <p class="collection-count">7 <span>Items</span></p>


  </div>

  </a>
  
  
    <a href="/collections/new-arrivals" title="Browse our New Arrivals collection" class="grid-link">

      <span class="grid-link__title">New Arrivals</span></a>

</div>

    
  
  
    
      
      
      





<div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">
  

  <a href="/collections/ornaments" title="Browse our Ornaments collection" class="pickgradient grid-link">
        
          <img src="http://cdn.shopify.com/s/files/1/1811/9385/products/10_large.jpg?v=1496149646" alt="Ornaments" />
        
     <div class="dt-sc-event-overlay">
    
    <p class="collection-count">7 <span>Items</span></p>


  </div>

  </a>
  
  
    <a href="/collections/ornaments" title="Browse our Ornaments collection" class="grid-link">

      <span class="grid-link__title">Ornaments</span></a>

</div>

    
-->


</div>              
</div>       

</div>


<div class="dt-sc-hr-invisible-large"></div>

</main>
@endsection