<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="navbar-brand-wrapper d-flex align-items-center">
    <a class="navbar-brand brand-logo" href="/">
      <img src="/images/logo-text.png" alt="logo" class="saskar-Silvers" />
    </a>
    <a class="navbar-brand brand-logo-mini" href="/"><img src="/dashboard/images/logo-mini.svg" alt="logo" /></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-center flex-grow-1">
    <h5 class="mb-0 font-weight-medium d-none d-lg-flex">Welcome To Sanskar Silver !</h5>
    <ul class="navbar-nav navbar-nav-right ml-auto">
      <form class="search-form d-none d-md-block" action="#">
        <i class="icon-magnifier"></i>
        <input type="search" class="form-control" placeholder="Search Here" title="Search here">
      </form>
      <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
                <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          <img class="img-xs rounded ml-2" src="/images/custom-order.png" alt="{{$currentuser->name}}">
        <div class="make-cart-btn">
          <span class="font-weight-normal cartCount"> 
                  @php
                    $cartCount = Cart::getContent();
                    echo $cartCount->count();
                  @endphp
           </span>
         </div>
         </a>
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
            <div class="dropdown-header text-center">
              @if(count($cartCount))
                <div class="has-items" style="display: block;">
                  <ul class="mini-products-list">
                    @foreach($cartCount as $cartContent)
                    <li class="item">
                      <div style="margin: 0; background: #e6f2ffb5; padding: 10px;" class="row">
                        <div class="col-sm-4">
                          <a href="/products/amor-solitaire-ring-mount?variant=37510477518" title="{{ $cartContent->name }}" class="product-image">
                        <img style="max-width: 100%;" src="/uploads/products/{{ $cartContent->attributes[1] }}" alt="{{ $cartContent->name }}"></a>
                        </div>
                        <div class="col-sm-8">
                          <div class="product-details">
                          <a href="/cart/remove/{{$cartContent->id}}" onclick="makeWarning(event)" title="Remove This Item" class="btn-remove1">
                            <i class="fa fa-times"></i>
                          </a>
                          <p class="product-name">
                            <a href="/product-detail/{{ $cartContent->id}}">{{ $cartContent->name }}</a>
                          </p>
                          <div class="cart-collateral">{{ $cartContent->quantity }} x 
                            <span class="price"><span class="money">Rs.{{ $cartContent->price }}</span>
                          </span>
                        </div>
                      </div>
                        </div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
                  <div class="summary">                
                    <p class="total">
                      <span class="label1">Cart total:</span>
                      <span class="price"><span class="money">Rs.{{ Cart::getSubTotal() }}</span></span> 
                    </p>
                  </div>
                  <div class="actions">
                    <button class="btn" onclick="window.location='/checkout'"><i class="fa fa-check"></i> Check Out</button>
                    <button class="btn text-cart" onclick="window.location='/cart'"><i class="fa fa-shopping-basket"></i> View Cart</button>
                  </div>
                </div>
                @else
                <div class="no-items">
                  <p>Your cart is currently empty!</p>
                  <p class="text-continue"><a  href="javascript:void(0)">Continue shopping</a></p>
                </div>
                @endif
             </div>
        </div>
      </li>
      <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          @if($currentuser->image)
          <img class="img-xs rounded-circle ml-2" src="/images/users/{{$currentuser->image}}" alt="{{$currentuser->name}}">
          @else
          <img class="img-xs rounded-circle ml-2" src="/images/users/user-image.png" alt="{{$currentuser->name}}">
          @endif
          <span class="font-weight-normal"> {{$currentuser->name}} </span></a>
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
            <div class="dropdown-header text-center">
              @if($currentuser->image)
              <img class="img-md rounded-circle admin_get_images" src="/images/users/{{$currentuser->image}}" alt="{{$currentuser->name}}">
              @else
              <img class="img-md rounded-circle admin_get_images" src="/images/users/user-image.png" alt="Profile image">
              @endif
              <p class="mb-1 mt-3">{{$currentuser->name}}</p>
              <p class="font-weight-light text-muted mb-0">{{$currentuser->email}}</p>
            </div>
            <a class="dropdown-item" href="/user/profile"><i class="dropdown-item-icon icon-user text-primary"></i> My Profile <!-- <span class="badge badge-pill badge-danger">1</span> --></a>
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
             <i class="dropdown-item-icon icon-power text-primary"></i>Sign Out
           </a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
          <!-- <a class="dropdown-item"><i class="dropdown-item-icon icon-power text-primary"></i>Sign Out</a> -->
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="icon-menu"></span>
    </button>
  </div>
</nav>

<!-- partial -->
<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-category">
        <span class="nav-link">Customer Dashboard</span>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/user">
          <span class="menu-title">Dashboard</span>
          <i class="icon-screen-desktop menu-icon"></i>
        </a>
      </li>
      <li class="nav-item nav-category"><span class="nav-link">Elements</span></li>

      <li class="nav-item">
        <a class="nav-link" href="/user/profile">
          <span class="menu-title">Profile</span>
          <i class="icon-globe menu-icon"></i>
        </a>
      </li>
      <li class="nav-item nav-category"><span class="nav-link">Products</span></li>
      <li class="nav-item">
        <a class="nav-link" href="/user/order-history">
          <span class="menu-title">Order History</span>
          <i class="icon-globe menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/checkout">
          <span class="menu-title">Checkout</span>
          <i class="icon-globe menu-icon"></i>
        </a>
      </li>
    </ul>
  </nav>
