<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Rb-Jwellers Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('/dashboard/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('/dashboard/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('/dashboard/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('/dashboard/vendors/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('/dashboard/vendors/chartist/chartist.min.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('/dashboard/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/dashboard/css/modify.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('/dashboard/images/favicon.png')}}" />

  <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('layouts.backend.sidebar')
      @yield('content')
       @include('layouts.backend.footer')
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="/dashboard/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="/dashboard/vendors/chart.js/Chart.min.js"></script>
    <script src="/dashboard/vendors/moment/moment.min.js"></script>
    <script src="/dashboard/vendors/daterangepicker/daterangepicker.js"></script>
    <script src="/dashboard/vendors/chartist/chartist.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="/dashboard/js/off-canvas.js"></script>
    <script src="/dashboard/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="/dashboard/js/dashboard.js"></script>
    <!-- End custom js for this page -->
    <script>
        CKEDITOR.replace('description');

    </script>
        <script type="text/javascript">
         function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#load-img')
                        .attr('src', e.target.result)
                        .width('50%')
                        .height('30%');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on("change", ".file_multi_video", function(evt) {
  var $source = $('#video_here');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
});
</script>
<script type="text/javascript">
         function readURLNext(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#load-img-n')
                        .attr('src', e.target.result)
                        .width('50%')
                        .height('30%');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on("change", ".file_multi_video", function(evt) {
  var $source = $('#video_here');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
});
</script>
<script type="text/javascript">
  function makeWarning(evt){
    let result = confirm("Are you sure to Delete?");
      if(! result){
        evt.stopPropagation();
        evt.preventDefault(); 
      }
  }
</script>
  </body>
</html>