<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Basic page needs ================================================== -->
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  
  <link rel="shortcut icon" href="/images/favicon-32x32.png" type="image/png" />
  

  <!-- Title and description ================================================== -->
  <title>
    Sanskar Silver
  </title>
  
  <!-- Social meta ================================================== -->
  

  <meta property="og:type" content="website">
  <meta property="og:title" content="Sanskar Jewelry">
  <meta property="og:url" content="">
  
  <meta property="og:image" content="/images/logo.png?3458">
  <meta property="og:image:secure_url" content="/images/logo.png?3458">
  


  <meta property="og:site_name" content="Sanskar Jewelry">



  <meta name="twitter:card" content="summary">


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


  <!-- Helpers ================================================== -->
  <link rel="canonical" href="https://rbjwellers.com/">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="theme-color" content="#000000">


  <!-- CSS ================================================== -->
  <link href="{{asset('/assets/timber.scss.css?3458')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('/assets/style.css?3458')}}" rel="stylesheet" type="text/css" media="all" />  
  <link href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/font-awesome.min.css?3458" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('/assets/settings.css?3458')}}" rel="stylesheet" type="text/css" media="all" />  
  <link href="{{asset('/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />  
  <link href="{{asset('/css/custom_responsive.css')}}" rel="stylesheet" type="text/css" media="all" />  
  
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,300italic,400,600,400italic,600italic,700,700italic,800,800italic">
  
  <link href="https://fonts.googleapis.com/css?family=Alex+Brush|Cookie|Dancing+Script|Great+Vibes|Satisfy&display=swap" rel="stylesheet">
    <script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous">
    </script>
  <!-- Header hook for plugins ================================================== -->

<script integrity="sha256-qzPTa4Ven/Yc2yyXr9BKZWCTXSrPTCnbGdWsxA7YCw0=" defer="defer" src="{{asset('/assets/storefront/features-ab33d36b855e9ff61cdb2c97afd04a6560935d2acf4c29db19d5acc40ed80b0d.js')}}" crossorigin="anonymous"></script>
<link rel="stylesheet" media="screen" href="{{asset('/assets/styles.css?3458')}}">
<script id="sections-script" data-sections="home-product-grid-type-3" defer="defer" src="{{asset('/assets/scripts.js?3458')}}"></script>



<script>window.performance && window.performance.mark && window.performance.mark('shopify.content_for_header.end');</script>


<!--[if lt IE 9]>
<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
<script src="http://cdn.shopify.com/s/files/1/1811/9385/t/7/assets/respond.min.js?3458" type="text/javascript"></script>
<link href="http://cdn.shopify.com/s/files/1/1811/9385/t/7/assets/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
<link href="http://huge-jewelry.myshopify.com/search?q=e9f7c25edc57908987f1cb9196e3fda1" id="respond-redirect" rel="respond-redirect" />
<script src="http://huge-jewelry.myshopify.com/search?q=e9f7c25edc57908987f1cb9196e3fda1" type="text/javascript"></script>
<![endif]-->

  <script src="{{asset('/js/header.js?3458')}}" type="text/javascript"></script>    

@yield('styles')
</head>

<body id="collections" class="template-list-collections" >

@include('layouts.frontend.header')

<div id="PageContainer"></div>   
<div class="quick-view"></div>  
  <script src="/js/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js" type="text/javascript"></script>   
  <script src="/js/footer.js?3493" type="text/javascript"></script>
  <script src="/js/shop.js?3493" type="text/javascript"></script> 
  <script src="/js/classie.js?3493"></script>
  <script src="/js/jquery.elevatezoom.js?3493" type="text/javascript"></script>
  <script src="/js/jquery.fancybox.js?3493" type="text/javascript"></script>      
@yield('content')


@include('layouts.frontend.footer')

@if (Session::has('success'))
<!-- Modal -->
<div class="modal fade in" style="display:block;" id="customOrderSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sanskar Silver</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="dissapearModal()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="text-center" id="successmessage">
          <p>{{ Session::get('success') }}</p>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="dissapearModal()">Close</button>
      </div>
    </div>
  </div>
</div>
@endif
<script type="text/javascript">
 function dissapearModal(){
  $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
              });
  document.getElementById("customOrderSuccess").className = "custom-order-change";

               // alert(product_id);


                //Here you can use ajax to call php function
                $.ajax({
                  url: '/clear-session/',
                  type: 'GET',
                  dataType: "json",

                  success: function (ret_data) {
                    console.log('cleared');
                  }
                });
 }
</script>

<script src="{{asset('/assets/timber.js?3458')}}" type="text/javascript"></script>  
<script src="{{asset('/assets/theme.js?3458')}}" type="text/javascript"></script>
<script src="{{asset('/assets/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js')}}" type="text/javascript"></script>   



<script src="{{asset('/assets/slider-init.js?3458')}}" type="text/javascript"></script>

<script src="{{asset('/assets/countdown.js?3458')}}" type="text/javascript"></script>

<script src="{{asset('/assets/currencies.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/jquery.currencies.min.js?3458')}}" type="text/javascript"></script>



<div class="newsletterwrapper">
  <div id="email-modal" style="display: none;">
    <div class="modal-overlay"></div>
    <div class="modal-window" style="display: none;">
      <div class="window-window">
        <a class="btn close" title="Close Window"></a>




        <div class="window-content">
          <div class="newsletter-title">
           <h2 class="title">Newsletter Signup</h2>
         </div>
         <p class="message">Subscribe and get notified at first on the latest update and offers!</p>
         <div id="mailchimp-email-subscibe">
          <div id="mc_embed_signup">


            <form method="POST" action="/subscriber" id="contact_form" accept-charset="UTF-8" class="contact-form">
              @csrf
              <input type="email" value="" placeholder="Email address" name="email" class="mail" aria-label="Email address" >
              <input type="hidden" name="contact_tags" value="newsletter">
              <button type="submit" class="btn" name="subscribe" value=""  id="subscribe" >Send</button>          
            </form>  

          </div>   
          <span>Note:we do not spam</span>
        </div>   
      </div>

    </div>
  </div>
</div>
</div>


<script src="{{asset('/assets/wow.js?3458')}}" type="text/javascript"></script>  

<script src="{{asset('/assets/classie.js?3458')}}"></script>
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function() {    //
    var $modalParent        = jQuery('div.newsletterwrapper'),
        modalWindow         = jQuery('#email-modal'),
        emailModal          = jQuery('#email-modal'),
        modalPageURL        = window.location.pathname; 
          
    modalWindow = modalWindow.html();
    modalWindow = '<div id="email-modal">' + modalWindow + '</div>';
    $modalParent.css({'position':'relative'});
    jQuery('.wrapper #email-modal').remove();
    $modalParent.append(modalWindow);
  
    if (jQuery.cookie('emailSubcribeModal') != 'closed') {
        openEmailModalWindow();
    };
      
    jQuery('#email-modal .btn.close').click(function(e) {
        e.preventDefault();
        closeEmailModalWindow();
    });
    jQuery('body').keydown(function(e) {
        if( e.which == 27) {
            closeEmailModalWindow();
            jQuery('body').unbind('keydown');
        }
    });
    jQuery('#mc_embed_signup form').submit(function() {
        if (jQuery('#mc_embed_signup .email').val() != '') {
            closeEmailModalWindow();
        }
    });
      
    function closeEmailModalWindow () {
        jQuery('#email-modal .modal-window').fadeOut(600, function() {
            jQuery('#email-modal .modal-overlay').fadeOut(600, function() {
                jQuery('#email-modal').hide();
                jQuery.cookie('emailSubcribeModal', 'closed', {expires:1, path:'/'});
            });
        })
    }
    function openEmailModalWindow () {
        jQuery('#email-modal').fadeIn(600, function() {
           jQuery('#email-modal .modal-window').fadeIn(600);
        });
    }
      
});
// ]]
// ]]></script>
<script type="text/javascript">
  function makeWarning(evt){
    let result = confirm("Are you sure to Delete?");
      if(! result){
        evt.stopPropagation();
        evt.preventDefault(); 
      }
  }
</script>
</body>
</html>


