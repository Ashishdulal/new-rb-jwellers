
<div id="shopify-section-footer-model-3" class="shopify-section index-section"><div data-section-id="footer-model-3"  data-section-type="Footer-model-3" class="footer-model-3">
  <div class="footer-logo" style="background:#f9f9f9;border-top:1px solid rgba(0,0,0,0);">

    <a href="/">
      <img class="normal-footer-logo" src="/images/small-logo.png" alt="Huge Jewelry" />
    </a>


    <p style="color:#000">Pellentesque posuere orci lobortis scelerisque blandit. Donec id tellus lacinia an, tincidunt risus ac, consequat velit. 
      <br>Quisquemos sodales suscipit tortor ditaemcos condimentum lacus.</p> 


      <div class="footer_social_icons">

        <ul class="inline-list social-icons social-links-type-3">

          <li>
            <a style="border:1px solid #000;color:#000" class="icon-fallback-text twitt hexagon" target="blank" href="https://twitter.com/nepgeeks" title="Twitter">
              <i class="fa fa-twitter" aria-hidden="true"></i>
            </a>
          </li>


          <li>
            <a style="border:1px solid #000;color:#000" class="icon-fallback-text fb hexagon" target="blank" href="https://www.facebook.com/nepgeeks" title="Facebook">
             <i class="fa fa-facebook" aria-hidden="true"></i>
           </a>
         </li>



         <li>
          <a style="border:1px solid #000;color:#000" class="icon-fallback-text google hexagon" target="blank" href="#" title="Google+" rel="publisher">
            <i class="fa fa-google-plus" aria-hidden="true"></i>
          </a>
        </li>



        <li>
          <a style="border:1px solid #000;color:#000" class="icon-fallback-text tumblr" target="blank" href="#" title="Tumblr">
            <i class="fa fa-tumblr" aria-hidden="true"></i>
          </a>
        </li>





      </ul>

      
    </div>
    <p style="color:#000; margin-top: 20px;"><a href="/terms-condition">Terms And Condition</a> &nbsp; |&nbsp; <a href="/privacy-policy">Privacy Policy</a>&nbsp; |&nbsp; <a href="/return-policy">Return Policy</a></p> 
  </div>

  <div style="background:#000;" class="grid__item copyright-section ">

    <p style="color:#ffffff;"  class="copyright">Copyright <a target="_blank" rel="nofollow" href="http://www.aeydentech.com">Powered by Aeydentech</a></p>

    
<!--     <div class="footer-icons">        
      <ul class="inline-list payment-icons">
        <li><a href="/cart"><img src="/images/payment_icon_6_small.png?v=1490168951" alt="payment_icon_1" /></a></li>
        <li><a href="/cart"><img src="/images/payment_icon_5_small.png?v=1490168944" alt="payment_icon_2" /></a></li>
        <li><a href="/cart"><img src="/images/payment_icon_3_small.png?v=1490168933" alt="payment_icon_3" /></a></li>
        <li><a href="/cart"><img src="/images/payment_icon_2_small.png?v=1490168929" alt="payment_icon_4" /></a></li>
        <li><a href="/cart"><img src="/images/payment_icon_4_small.png?v=1490168941" alt="payment_icon_5" /></a></li>





      </ul>                 
    </div>  -->
    
  </div>

  <style>
    .footer-model-3 .footer_social_icons ul li a:hover { border-color:#d2aa5c !important;background:#d2aa5c !important;color:#ffffff !important; }
    .footer-model-3 .copyright-section p a { color:#ffffff; }
    .footer-model-3 .copyright-section p a:hover { color:#d2aa5c; }
    
  </style>
</div>


</div> 
