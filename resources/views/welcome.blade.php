@extends('layouts.frontend.app')

@section('content')
<main class="main-content">
  <div style=" background: #212022;" class="grid__item">

    <!-- <div style="padding:0px;" class="col-sm-4">
      <div id="shopify-section-1492003551011" class="shopify-section index-section">
        <div data-section-id="1492003551011" data-section-type="grid-banner-type-9" class="grid-banner-type-9">  
          <marquee style="color: white;background: #d2aa5c;" ><p style="margin:0;padding: 10px 0;font-size: 16px;font-weight: 600;">The Price For 1 Tola SILVER is Nrs. 850 , Updated on (6 December 2019 10:00 UTC).</p></marquee>
          <div class="grid-uniform">   
            <div style="width: 100%;" class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003917601-0" style="background: #000000;"> 
              <div class="section-inner">        

               <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                  <div class="collection-info"> 


                    <h2 style="color:#ffffff">

                      <a href="/products/3" style="color:#ffffff">

                        New Arrival
                      </a>      
                    </h2>        

                    <a href="/products/3" class="btn">View All <i class="zmdi zmdi-long-arrow-right"></i></a>

                  </div>
                </div>

              </div>

              <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item right">

                <a href="/products/1" class="collection-img" >
                  <img src="/images/slider-side1.jpg" alt="1" />
                </a>

              </div>

            </div>
          </div>  

          <div style="width: 100%;" class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003551011-0" style="background: #000000;"> 
            <div class="section-inner">        

              <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item left">

                <a href="/products/all" class="collection-img" >
                  <img src="/images/slider-side2.jpg" alt="1" />
                </a>

              </div>


              <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                <div class="collection-info"> 


                  <h2 style="color:#ffffff">

                    <a href="/products/all" style="color:#ffffff">

                      All
                    </a>      
                  </h2>        

                  <a href="/products/all" class="btn"><i class="zmdi zmdi-long-arrow-left"></i> View All</a>

                </div>
              </div>



            </div>
          </div> 
        </div>
      </div>
    </div>
  </div>

  <div style="padding:0px;" class="col-sm-8">
    <video autoplay width="97.2%" id="video" style="margin-bottom:-6px" muted preload loop>
      <source src="/images/jewellery-video_low.m4v" type="video/mp4">
        Your browser doesn't support this content!
      </video>
    </div> -->

    <div id="shopify-section-slider-revolution" class="shopify-section index-section">

      <div id="rev_slider_72_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="jewel" data-source="gallery" style="margin:0px auto;background:#bababa;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.4.3.1 auto mode -->
        <div id="rev_slider_72_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.3.1">
          <ul>    <!-- SLIDE  -->
            @foreach($sliders as $slider)
            <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/uploads/{{$slider->image}}" data-saveperformance="off" data-title="Mobile Ready Design">
            <a href="/allethnics">
              <img src="/uploads/{{$slider->image}}" alt="video_typing_cover" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            </a>
          </li>
          @endforeach
<!--           <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/text-banner.jpg" data-saveperformance="off" data-title="Mobile Ready Design">
            <a href="/allethnics">
              <img src="/images/text-banner.jpg" alt="video_typing_cover" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            </a>
          </li>
            <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/banner-sanskar.jpg" data-saveperformance="off" data-title="Mobile Ready Design">
              <a href="/allethnics">
                <img src="/images/banner-sanskar.jpg" alt="video_typing_cover" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
              </a>
            </li>
            <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/banner-sanskar-1.jpg" data-saveperformance="off" data-title="Mobile Ready Design">
              <a href="/allethnics">
                <img src="/images/banner-sanskar-1.jpg" alt="video_typing_cover" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
              </a>
            </li>
            <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/text2banner.jpg" data-saveperformance="off" data-title="Mobile Ready Design">
              <a href="/allethnics">
                <img src="/images/text2banner.jpg" alt="video_typing_cover" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
              </a>
            </li> -->
            <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/video-image.png" data-saveperformance="on" data-title="Mobile Ready Design">
              <video width="100%" id="video" style="margin-bottom:-6px" muted preload loop autoplay>
                <source src="/images/jewellery-video_low.m4v" type="video/mp4">
                  Your browser doesn't support this content!
                </video>
              </li>
<!-- <li class="dark" data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/video-image.png" data-saveperformance="on" data-title="Mobile Ready Design">

              <div class=" fullscreenvideo rs-background-video-layer"
              data-forcerewind="off" 
              data-volume="0%"
              data-videowidth="100%" 
              data-videoheight="100%" 
              data-videopreload="preload" 
              data-videoattributes="loop=loop"
              data-videoloop="loop" 
              data-forceCover="1" 
              data-aspectratio="16:9" 
              data-autoplay="true" 
              data-autoplayonlyfirsttime="true" 
              data-nextslideatend="false"
              data-videomp4="/images/jewellery-video_low.m4v">
            </div>

          </li> -->


        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;">
        </div>
      </div>
    </div>
    <!-- END REVOLUTION SLIDER -->

    <script type="text/javascript">
      var revapi72,
      tpj=jQuery;

      tpj(document).ready(function() {
       if(tpj("#rev_slider_72_1").revolution == undefined){
        revslider_showDoubleJqueryError("#rev_slider_72_1");
      }else{
        revapi72 = tpj("#rev_slider_72_1").show().revolution({
         sliderType:"standard",
                  // jsFileLocation:"//localhost/finance/wp-content/plugins/revslider/public/assets/js/",
                  sliderLayout:"auto",
                  dottedOverlay:"none",
                  delay:9000,
                  navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    mouseScrollReverse:"default",
                    onHoverStop:"off",
                    arrows: {
                     style:"gyges",
                     enable:true,
                     hide_onmobile:true,
                     hide_under:480,
                     hide_onleave:true,
                     hide_delay:200,
                     hide_delay_mobile:1200,
                     tmp:'',
                     left: {
                      h_align:"left",
                      v_align:"center",
                      h_offset:20,
                      v_offset:0
                    },
                    right: {
                      h_align:"right",
                      v_align:"center",
                      h_offset:20,
                      v_offset:0
                    }
                  }
                  ,
                  bullets: {
                   enable:true,
                   hide_onmobile:false,
                   style:"hebe",
                   hide_onleave:false,
                   direction:"horizontal",
                   h_align:"center",
                   v_align:"bottom",
                   h_offset:0,
                   v_offset:20,
                   space:5,
                   tmp:'<span class="tp-bullet-image"></span>'
                 }
               },
               responsiveLevels:[1240,1240,778,480],
               visibilityLevels:[1240,1240,778,480],
               gridwidth:[1750,1024,778,480],
               gridheight:[750,768,500,500],
               lazyType:"none",
               shadow:0,
               spinner:"spinner2",
               stopLoop:"off",
               stopAfterLoops:-1,
               stopAtSlide:-1,
               shuffle:"off",
               autoHeight:"off",
               disableProgressBar:"on",
               hideThumbsOnMobile:"off",
               hideSliderAtLimit:0,
               hideCaptionAtLimit:0,
               hideAllCaptionAtLilmit:0,
               debugMode:false,
               fallbacks: {
                simplifyAll:"off",
                nextSlideOnWindowFocus:"off",
                disableFocusListener:false,
              }
            });
      }

    });   /*ready*/
  </script>
  
</div>
</div> 
<!-- BEGIN content_for_index -->
<!-- <div id="shopify-section-1492003551011" class="shopify-section index-section">
  <div data-section-id="1492003551011" data-section-type="grid-banner-type-9" class="grid-banner-type-9">  
    <div class="grid-uniform">        

      <?php $countC=1; ?>

      @if($allEvent)
      @foreach($allEvent as $pcat)
      @if($countC%2  == 1 )  

      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003551011-0" style="background: #000000;"> 
        <div class="section-inner">        

          <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item left">

            <a href="/ethnic/{{$pcat->id}}" class="collection-img" >
              <img src="/uploads/products/events/{{$pcat->image}}" alt="{{$pcat->name}}"/>
            </a>

          </div>


          <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
            <div class="collection-info"> 


              <h2 style="color:#ffffff">

                <a href="/ethnic/{{$pcat->id}}" style="color:#ffffff">

                  {{$pcat->name}}
                </a>      
              </h2>        

              <a href="/ethnic/{{$pcat->id}}" class="btn"><i class="zmdi zmdi-long-arrow-left"></i> View All</a>

            </div>
          </div>



        </div>
      </div>  
      @else
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003917601-0" style="background: #000000;"> 
        <div class="section-inner">        


         <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
          <div class="collection-info"> 


            <h2 style="color:#ffffff">

              <a href="/ethnic/{{$pcat->id}}" style="color:#ffffff">

                {{$pcat->name}}
              </a>      
            </h2>        

            <a href="/ethnic/{{$pcat->id}}" class="btn">View All <i class="zmdi zmdi-long-arrow-right"></i></a>

          </div>
        </div>


        <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item right">

          <a href="/ethnic/{{$pcat->id}}" class="collection-img" >
            <img src="/uploads/products/events/{{$pcat->image}}" alt="{{$pcat->name}}" />
          </a>

        </div>


      </div>
    </div>         
    @endif
    <?php $countC ++; ?>
    <?php if($countC > 4)
    break
    ?>     
    @endforeach
    @endif


    <style type="text/css">
      .grid-banner-type-9 .section-wrapper-1492003917601-0:hover  { background:#d2aa5c !important;}  
      .grid-banner-type-9 .section-wrapper-1492003917601-0 .btn { background: #d2aa5c;color: #fff;}
      .grid-banner-type-9 .section-wrapper-1492003917601-0 .btn:hover {background: #ffffff;color: #000;}
    </style>

    <style type="text/css">
      .grid-banner-type-9 .section-wrapper-1492003917601-1:hover  { background:#d2aa5c !important;}  
      .grid-banner-type-9 .section-wrapper-1492003917601-1 .btn { background: #d2aa5c;color: #fff;}
      .grid-banner-type-9 .section-wrapper-1492003917601-1 .btn:hover {background: #fff;color: #000;}
    </style>


  </div>

  <style type="text/css">
    .grid-banner-type-9 .section-wrapper-1492003551011-0:hover  { background:#d2aa5c !important;}  
    .grid-banner-type-9 .section-wrapper-1492003551011-0 .btn { background: #d2aa5c;color: #fff;}
    .grid-banner-type-9 .section-wrapper-1492003551011-0 .btn:hover {background: #fff;color: #000;}
  </style>

  <style type="text/css">
    .grid-banner-type-9 .section-wrapper-1492003551011-1:hover  { background:#d2aa5c !important;}  
    .grid-banner-type-9 .section-wrapper-1492003551011-1 .btn { background: #d2aa5c;color: #fff;}
    .grid-banner-type-9 .section-wrapper-1492003551011-1 .btn:hover {background: #fff;color: #000;}
  </style>





</div>
</div> -->
</main>
<div class="dt-sc-hr-invisible-small"></div> 
<div class="product-grid-type-13"> 
  <div class="border-title">
    <h2 class="section-header__title" style="color:#000;">    
      Shop By Events
    </h2>
    <div class="short-desc"> <p style="color:#000;">Carefully crafted</p></div>  
  </div>
  <ul class="grid-uniform product-grid-type__13">
    <?php $count=1;?>
    @foreach($allEvents as $eventProd)
    <li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item on-sale" id="product-10109321806"  >
      <div class="products">
        <div class="product-container">  
          <a href="/ethnic/{{$eventProd->id}}" class="grid-link">    
            <div class="ImageOverlayCa"></div>
            <img src="/uploads/products/events/{{$eventProd->image}}" class="featured-image" alt="Crumpled hard Ring">
          </a>
          <div class="ImageWrapper">
            <div class="product-button">  
             <a href="/ethnic/{{$eventProd->id}}">                      
              <i class="fa fa-external-link" aria-hidden="true"></i>
            </a>       

          </div>
        </div>
      </div>
      <div class="product-detail">

        <a href="/ethnic/{{$eventProd->id}}" class="grid-link__title">{{$eventProd->name}}</a>     
        <div class="grid-link__meta">
          <div class="product_price">
            <div class="grid-link__org_price">
            </div>
            <!-- <del class="grid-link__sale_price"><span class=money>Lorem ipsum dolor sit  elit  aliqua.</span></del> -->

          </div>      
          <span class="shopify-product-reviews-badge" data-id="10109321806"></span> 
        </div>
        <ul class="item-swatch color_swatch_Value">  
        </ul>
      </div>
    </div>
  </li>
  <?php $count++;?>
  <?php if($count >4)
  break
  ?>
  @endforeach
</ul>
<div class="text-center">
  <a href="/allevents" style="" class="btn">View all Events</a>
</div>
</div>
<div class="dt-sc-hr-invisible-small"></div> 
<div id="shopify-section-1492061512174" class="shopify-section index-section">
  <div data-section-id="1492061512174" data-section-type="home-product-grid-type-8" class="home-product-grid-type-8" style="float:left;width:100%;background-color:#f9f9f9;">
    <div class="dt-sc-hr-invisible-large"></div>
    <div class="wrapper">
      <div class="grid-uniform collectionItems"> 
        <div class="section-header section-header--small">
          <div class="border-title">


            <h2 class="section-header__title" style="color:#000;">    
              Shop By Ethnics
            </h2>

            <div class="short-desc"> <p style="color:#000;">All religions in one platform</p></div>  

          </div>
        </div>    
        <div class="dt-sc-hr-invisible-small"></div> 

        <div class=" medium--one-half home-product-grid-type-8-right container">
          <div class="type-8__items-wrapper">                         
           <ul class="type8__items wide-owl-1492061512174 owl-carousel owl-theme">

            @if($allethenticity)
            @foreach($allethenticity as $ethnic)
            <li class="grid__item item-row"  >
              <div class="products">
                <div class="product-container">  

                  <a href="/ethnics/{{$ethnic->id}}" class="grid-link">    

                    <div class="ImageOverlayCa"></div>

                    <img src="/uploads/products/ethnicity/{{$ethnic->image}}" class="featured-image" alt="{{$ethnic->name}}">
                  </a>
                  <div class="ImageWrapper">
                    <div class="product-button">  
                     <a href="/ethnics/{{$ethnic->id}}">                      
                      <i class="fa fa-external-link" aria-hidden="true"></i>
                    </a>       

                  </div>
                </div>
              </div>
              <div style="padding:10px 0 0 ;" class="product-detail">

                <a href="/ethnics/{{$ethnic->id}}" class="grid-link__title">{{$ethnic->name}}</a>     
              </div>
            </div>
          </li>
          @endforeach
          @endif
        </ul>
        <div class="text-center"><br>
          <a href="/allethnics" style="" class="btn">View all Ethnics</a>
        </div>          
      </div>
      <div class="nav_type8__items wide-owl__nav-1492061512174 ">
        <a class="prev btn active"><i class="zmdi zmdi-arrow-left"></i></a>
        <a class="next btn"><i class="zmdi zmdi-arrow-right"></i></a>  
      </div> 

      <script type="text/javascript">       
        $(document).ready(function(){
         var type8_products_count = $('.type8__items li.item-row').length;
         if(type8_products_count > 5) { $('.wide-owl__nav-1492061512174').css('display','block');}
         else {$('.wide-owl__nav-1492061512174').css('display','none');}
         var type8_products = $(".wide-owl-1492061512174");
         type8_products.owlCarousel({
          items: 5,
          itemsCustom: false,
          itemsDesktop: [1199, 3],
          itemsDesktopSmall: [980, 1],
          itemsTablet: [630, 1],
          itemsTabletSmall: false,
          itemsMobile: [479, 1],
          singleItem: false,
          itemsScaleUp: false,
          responsive: true,
          responsiveRefreshRate: 200,
          responsiveBaseWidth: window,
          autoPlay: false,
          stopOnHover: false,
          navigation: false,
          pagination:false
        });
      // Custom Navigation Events
      $(".wide-owl__nav-1492061512174 .next").click(function(){
        type8_products.trigger('owl.next');
      })
      $(".wide-owl__nav-1492061512174 .prev").click(function(){
        type8_products.trigger('owl.prev');
      }) 
    });
  </script>
</div>

<div class="dt-sc-hr-invisible-small"></div>
</div> 
</div>
</div>
<div class="dt-sc-hr-invisible-small"></div>

</div>
<div id="shopify-section-1492061167634" class="shopify-section index-section">
  <div data-section-id="1492061167634"  data-section-type="home-product-grid-type-13" class="home-product-grid-type-13">
    <div class="container">
      <div class="product-grid-type-13">  

        <div class="border-title">


          <h2 class="section-header__title" style="color:#000;">    
            Jewellery Collections
          </h2>

          <div class="short-desc"> <p style="color:#000;">Carefully crafted</p></div>  

        </div>
        <div class="dt-sc-hr-invisible-small"></div>

        <ul class="grid-uniform product-grid-type__13">

          <?php $count=1; ?>
          @if(count($ornamentProd))
          @foreach($ornamentProd as $ornamentsProd)
          <?php $count<=4?>
          <li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item" id="product-10848622862"  >
            <div class="products">
              <div class="product-container">  

                <a href="/product-detail/{{$ornamentsProd->product_id}}" class="grid-link">    

                  <div class="ImageOverlayCa"></div>

                  <img src="/uploads/products/{{$ornamentsProd->image1}}" class="featured-image" alt="{{$ornamentsProd->name}}">
                </a>
                <div class="ImageWrapper">
                  <div class="product-button">  




                   <div class="add-to-wishlist">     
                    <div class="show">
                      <form  action="/cart" method="post">     
                        @csrf  
                        <input type="hidden" name="product_id" value="{{$ornamentsProd->product_id}}">
                        <input type="hidden" name="productPrice" value="{{$ornamentsProd->rate}}">
                        <input type="hidden" name="weight" value="{{$ornamentsProd->weight}}">
                        <div class="added-wishbutton-barry-gold-bangle-for-her loading"><button style="background: #ffffff;font-size: 8px" type="submit" class="btn" >Add to Cart</button></div>
                      </form>
                      <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
                      <div class="added-wishbutton-barry-gold-bangle-for-her loading"><a class="added-wishlist btn add_to_wishlist" href="#" data-toggle="modal" data-target="#myCustomOrder{{$ornamentsProd->product_id}}"><span style="font-size: 8px">Custom Order</span><span class="tooltip-label">Custom Order</span></a></div>
                    </div>
                  </div>


                </div>
              </div>
            </div>
            <div class="product-detail">

              <a href="/product-detail/{{$ornamentsProd->product_id}}" class="grid-link__title">{{$ornamentsProd->name}}</a>     
              <div class="grid-link__meta">
                <div class="product_price">


                  <div class="grid-link__org_price">
                    <span class=money>
                      <?php 
                      $excerpt = $ornamentsProd->description;
                      $the_str = substr($excerpt, 0, 32);
                      echo $the_str; 
                      ?>...
                    </span>
                  </div>


                </div>      
                <span class="shopify-product-reviews-badge" data-id="10848622862"></span> 
              </div>


              <ul class="item-swatch color_swatch_Value">  





              </ul>




            </div>
          </div>
        </li>
        <?php $count++;?>
        <?php if($count > 5)
        break
        ?>
        @endforeach
        @endif


        <li class="grid__item wide--three-quarters post-large--three-quarters large--three-quarters medium--one-half small-grid__item">  
          <div class="collection_content_section">
            <div class="grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item">

              <img src="/uploads/products/products1575444890.product-cat1575269808.product-cat1574076699.ornament1.jpg" alt="Ornaments Collections" />

            </div>
            <div class="collection-content grid__item wide--two-thirds post-large--two-thirds large--two-thirds medium--hide small--hide">

              <h2 style="color:#000">Silver</h2>        


              <h4 style="color:#000">Ornaments Collections</h4>        


              <p style="color:#000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore  aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.</p>        

              <a href="/products/2" style="" class="btn">View all collection</a>
            </div>
          </div>
        </li>

      </ul>
    </div>
    <style>
      .home-product-grid-type-13 .collection-content .btn { color:#fff;border:none;background:#000; }
      .home-product-grid-type-13 .collection-content .btn:hover { color:#ffffff;background:#d2aa5c; }
      .home-product-grid-type-13 .border-title:after {  content:"";background-image:url(/images/divider2.png);display:inline-block;background-repeat:no-repeat;width:136px;height:15px;} 
    </style>
      </div> 
</div>
</div>
<!-- <div id="shopify-section-1492059459559" class="shopify-section index-section">
  <div data-section-id="1492059459559"  data-section-type="home-product-grid-type-6" class="home-product-grid-type-6">
    <div class="dt-sc-hr-invisible-large"></div> 
<div class="full_width_tab load-wrapper">       

  <div class="section-header section-header--small">
    <div class="border-title">


      <h2 class="section-header__title" style="color:#000;">    
        Just Arrived
      </h2>

      <div class="short-desc"> <p style="color:#000;">Just in now</p></div>  

    </div>
  </div>    
  <div class="dt-sc-hr-invisible-small"></div>



  <div class="grid-uniform">


    <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item">              
      <ul class="type4__items">
        <?php $countp=1; ?>
        @foreach ($allproducts as $eachproduct)
        @if($eachproduct->featured != 1)
        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10848592526"  >
          <div class="products">
            <div class="product-container">  


              <a href="/product-detail/{{$eachproduct->id}}" class="grid-link">    

                <div class="ImageOverlayCa"></div>

                <img src="/uploads/products/{{$eachproduct->image1}}" class="featured-image" alt="Barry Gold Bangle for her">

              </a>

              <div class="product-button">  




               <div class="add-to-wishlist">     
                <div class="show">
                  <form  action="/cart" method="post">     
                    @csrf  
                    <input type="hidden" name="product_id" value="{{$eachproduct->id}}">
                    <input type="hidden" name="productPrice" value="@foreach($allWeight as $allweight) @if($allweight->id == $eachproduct->id){{$allweight->rate}}@endif @endforeach">
                    <input type="hidden" name="weight" value="@foreach($allWeight as $allweight) @if($allweight->id == $eachproduct->id){{$allweight->weight}}@endif @endforeach">
                    <div class="added-wishbutton-barry-gold-bangle-for-her loading"><button style="background: #ffffff;font-size: 8px" type="submit" class="btn" >Add to Cart</button></div>
                  </form>
                  <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
                  <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
                  <div class="added-wishbutton-barry-gold-bangle-for-her loading"><a class="added-wishlist btn add_to_wishlist" href="#" data-toggle="modal" data-target="#myCustomOrder{{$eachproduct->id}}"><span style="font-size: 8px">Custom Order</span><span class="tooltip-label">Custom Order</span></a></div>
                </div>
              </div>


            </div>

          </div>
          <div class="product-detail">

            <a href="/product-detail/{{$eachproduct->id}}" class="grid-link__title">{{$eachproduct->name}}</a>    
            <p class="product-vendor">
              <label>Ethnicity:</label>
              <span>CaratLane</span>
            </p>

            <div class="grid-link__meta">
              <div class="product_price">


                <div class="grid-link__org_price">
                  <span class=money>
                    <?php 
                    $excerpt = $eachproduct->description;
                    $the_str = substr($excerpt, 0, 32);
                    echo $the_str; 
                    ?>...
                    
                  </span>
                </div>


              </div>      
            </div>
          </div>
        </div>
      </li>

      <div id="myCustomOrder{{$eachproduct->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">


          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Custom Order</h4>
            </div>
            <div class="modal-body1">
              <form action="/custom-order/{{$eachproduct->id}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="custom-order">
                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <p class="card-description"> Please Fill the form to get the custom order inquiry. </p>
                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" class="form-control" name="name" placeholder="Please enter your name" aria-label="name">
                        </div>
                        <div class="form-group">
                          <label>Phone Number</label>
                          <input type="number" class="form-control form-control-lg" name="phone" placeholder="please enter your phone number" aria-label="phone">
                        </div>
                        <div class="form-group">
                          <label>Email</label>
                          <input type="text" class="form-control form-control-sm" name="mail" placeholder="please enter your email" aria-label="mail">
                        </div>
                        <div class="form-group">
                          <label>Weight [In Tola]</label>
                          <input type="text" class="form-control form-control-lg" name="weight" placeholder="please enter your custom weight" aria-label="weight">
                        </div>
                        <div class="form-group">
                          <label>Size</label>
                          <input type="text" class="form-control form-control-lg" name="size" placeholder="please enter the size" aria-label="size">
                        </div>
                        <input type="hidden" name="product_name" value="{{$eachproduct->name}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="product-infor">
                      <div>
                        <img src="/uploads/products/{{$eachproduct->image1}}" alt="{{$eachproduct->name}}">
                      </div>
                      <p class="product-inventory order-customize"><br>
                        <label>Product Name:  </label> <br>             
                        {{$eachproduct->name}}
                      </p>


                    </div>
                  </div>
                </div>
                &nbsp;&nbsp; <button type="submit" class="btn btn-primary mr-2">Submit</button>
              </form>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
      <?php $countp ++; ?>
      <?php if($countp > 6)
      break
      ?>  
      @endif
      @endforeach

    </ul>      
  </div>

  <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item right">
    <ul class="type4__singele_items">

      @if(count($featuredProduct))
      @foreach($featuredProduct as $fprod)
      <li class="deal grid grid__item item-row  " id="product-10109315278"  >
        <div class="products   wow fadeIn  ">
          <div class="product-container">  




            <a href="/product-detail/{{$fprod->id}}" class="grid-link">    

              <div class="reveal"> 
                <span class="product-additional1">      


                  <img  src="/uploads/products/{{$fprod->image1}}" alt="{{$fprod->name}}"  class="feature-image"/>
                </span>      


              </div> 
            </a>


<div class="ImageWrapper">
 <div class="product-button">  

  <a href="javascript:void(0)" id="promise-solitaire-ring-mount" class="quick-view-text">                      
    <i class="zmdi zmdi-eye"></i>
  </a>       


  <a href="/products/all">                      
    <i class="zmdi zmdi-link"></i>
  </a>       



  <div class="add-to-wishlist">     
    <div class="show">
      <div class="default-wishbutton-promise-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="promise-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
      <div class="loadding-wishbutton-promise-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="promise-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
      <div class="added-wishbutton-promise-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
      <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">Custom Order</span></a></div>
    </div>
  </div>


</div>
</div>
</div>
<div class="product-detail">
  <div class="product_left">
    <a href="/product-detail/{{$fprod->id}}" class="grid-link__title">{{$fprod->name}}</a>     
    <div class="grid-link__meta">
      <div class="product_price">
        <div class="grid-link__org_price">
          <span class=money>
            <?php 
            $excerpt = $fprod->description;
            $the_str = substr($excerpt, 0, 82);
            echo $the_str; 
            ?>...
          </span>
        </div>
      </div>      
      <span class="shopify-product-reviews-badge" data-id="10109315278"></span>
    </div>
    <ul class="item-swatch color_swatch_Value">  
    </ul>
  </div>
  <form  action="/cart" method="POST" class="variants clearfix">   
    @csrf                                 
    <input type="hidden" name="product_id" value="{{$fprod->id}}">
    <input type="hidden" name="productPrice" value="@foreach($allWeight as $allweight) @if($allweight->id == $fprod->id){{$allweight->rate}}@endif @endforeach">
    <input type="hidden" name="weight" value="@foreach($allWeight as $allweight) @if($allweight->id == $fprod->id){{$allweight->weight}}@endif @endforeach">
    <button type="submit" class="flag-prod">
      <i class="fa fa-shopping-cart"></i>
    </button>
  </form>  
</div>  
</div>
</li>
@endforeach
@else
<li class="deal grid grid__item item-row  " id="product-10109315278"  >
  <div class="products   wow fadeIn  ">
    <div class="product-container">  




      <a href="/collections" class="grid-link">    

        <div style="max-width: 74.5%;" class="reveal"> 
          <span class="product-additional1">      


            <img  src="/images/all-product-images.jpg" alt=""  class="feature-image"/>
          </span>      


        </div> 
      </a>

      <div class="product-detail">
        <div class="product_left">
          <a href="/collections" class="grid-link__title">All Collections</a>     
          <div class="grid-link__meta">
            <div class="product_price">
              <div class="grid-link__org_price">
                <span class=money>
                  All In One Platform...
                </span>
              </div>
            </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315278"></span>
          </div>
          <ul class="item-swatch color_swatch_Value">  
          </ul>
        </div>
      </div>  
    </div>
  </li>
  @endif
</ul>
</div>
</div>
</div>
<style>
  .home-product-grid-type-6 .border-title:after {  content:"";background-image:url(/images/divider2.png);display:inline-block;background-repeat:no-repeat;width:136px;height:15px;} 

</style>
<div class="dt-sc-hr-invisible-large"></div> 
</div>  
</div> -->

<div id="shopify-section-1492056756357" class="shopify-section index-section"><div data-section-id="1492056756357" data-section-type="grid-banner-type-7" class="grid-banner-type-7">  
  <div class="grid-uniform">        

    <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item"> 
      <div class="img-hover-effect bg-effect-1492056756357-0">      
       <div class="ovrly09">  

        <img src="images/image4.jpg" alt="Vivaldi me" />        
        <div class="overlay"></div>

        <div class="block-content">              

          <!-- <h6 style="color:#ffffff">Vivaldi me</h6>         -->


          <h4 style="color:#ffffff">Silver's Haven</h4>        


          <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>        

          <a style="color:#ffffff" href="/collections" onmouseover='this.style.color="#ffffff"' onmouseout='this.style.color="#ffffff"' class="">Shop Now </a>
        </div> 
      </div>
    </div>
  </div>
  <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item"> 
    <div class="img-hover-effect bg-effect-1492056756357-1">      
     <div class="ovrly09">  

      <img src="images/image5.jpg" alt="Tress" />        
      <div class="overlay"></div>

      <div class="block-content">              

        <!-- <h6 style="color:#ffffff">Tress</h6>         -->


        <h4 style="color:#ffffff">Exclusive silver</h4>        


        <a style="color:#ffffff" href="/collections" onmouseover='this.style.color="#ffffff"' onmouseout='this.style.color="#ffffff"' class="">Shop Now </a>
      </div> 
    </div>
  </div>
</div>

<div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item"> 
  <div class="img-hover-effect bg-effect-1492056884112">      
   <div class="ovrly09">  

    <img src="images/image6.jpg" alt="Tress" />        
    <div class="overlay"></div>

    <div class="block-content">              

      <!-- <h6 style="color:#ffffff">Tress</h6>         -->


      <h4 style="color:#ffffff">Soul Searcher</h4>        


      <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>        

      <a style="color:#ffffff" href="/collections/all" onmouseover='this.style.color="#fff"' onmouseout='this.style.color="#ffffff"' class="">Shop Now </a>
    </div> 
  </div>
</div>
</div>
</div>

</div>
<div class="dt-sc-hr-invisible-large"></div> 
<style>      

  .grid-banner-type-7 .bg-effect-1492056756357-0 .overlay { background:rgba(0, 0, 0, 0.7); }

</style>

<style>      

  .grid-banner-type-7 .bg-effect-1492056756357-1 .overlay { background:rgba(0, 0, 0, 0.7); }

</style>

<style>      

  .grid-banner-type-7 .bg-effect-1492056884112 .overlay { background:rgba(0, 0, 0, 0.7); }

</style>





</div>
<div id="shopify-section-1492061373549" class="shopify-section index-section"><div data-section-id="1492061373549"  data-section-type="home-product-grid-type-3" class="home-product-grid-type-3"><div class="grid--uniform">
  <div class="grid__item wide--one-quarter post-large--two-quarters large--two-quarters medium-down--grid__item small--grid__item"> 
    <div id="type-3__carousel_banner">
      <div class="img-hover-effect">
        <div class="ovrly23">


          <img src="http://cdn.shopify.com/s/files/1/1811/9385/files/page-collection3_500x.jpg?v=1496233077" alt="Handpicked products" />
          <div class="overlay" style="background:rgba(0, 0, 0, 0.5);"></div>


          <div class="featured-content"> 


            <p style="color:#ffffff;">Pandora Rings</p>


            <h2 style="color:#ffffff;">Handpicked products</h2>


            <h6 style="color:#ffffff;">Fine silver and gold bands with intricate details and gemstone accents</h6>    


            <a class="btn" href="/collections" style="color:#fff;border:none;background-color:#000000;">Have a look</a>    

          </div>   
        </div>    
      </div>
    </div>
  </div>


  <div class="grid__item wide--three-quarters post-large--two-quarters large--two-quarters medium-down--grid__item small--grid__item"> 
   <div class="home-product-grid-type-3-carousel"> 
     <div class="full_width_tab type-3__items-wrapper owl-carousel owl-theme">  
       <?php $count=0; ?>                          
       @foreach($allproducts as $allp)
       @if ($count%2==0)
       <ul class="type3__items Handpicked products">
         <li class="grid__item item-row   " id="product-10109315918"  >
          <div class="products">
            <div class="product-container">  
              <a href="/product-detail/{{$allp->id}}" class="grid-link">    
                <div class="ImageOverlayCa"></div>
                <img src="/uploads/products/{{$allp->image1}}" class="featured-image" alt="{{$allp->name}}">
              </a>
              <div class="product-button">  
               <div class="add-to-wishlist">     
                <div class="show">
                  <form  action="/cart" method="post">     
                    @csrf  
                    <?php ?>
                    <input type="hidden" name="product_id" value="{{$allp->id}}">
                    <input type="hidden" name="productPrice" value="@foreach($allWeight as $allweight) @if($allweight->id == $allp->id){{$allweight->rate}}@endif @endforeach">
                    <input type="hidden" name="weight" value="@foreach($allWeight as $allweight) @if($allweight->id == $allp->id){{$allweight->weight}}@endif @endforeach">
                    <div class="added-wishbutton-barry-gold-bangle-for-her loading"><button style="background: #ffffff;font-size: 8px" type="submit" class=" btn" >Add to Cart</button></div>
                  </form>
                  <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
                  <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
                </div>
              </div>

            </div>


          </div>
          <div class="product-detail">

            <a href="/product-detail/{{$allp->id}}" class="grid-link__title">{{$allp->name}}</a>    

            <p class="product-vendor">
              <label>Category :&nbsp;</label>
              <span>
                @foreach($allCat as $cat)
                @if($cat->id == $allp->cat_id)
                {{$cat->name}}
                @endif
              @endforeach</span>
            </p>
            <div class="grid-link__meta">
              <div class="product_price">
                <div class="grid-link__org_price">
                 <span class=money>
                  <?php 
                  $excerpt = $allp->description;
                  $the_str = substr($excerpt, 0, 32);
                  echo $the_str; 
                  ?>...
                </span>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </li>        

    @else
    <li class="grid__item item-row   " id="product-10109315918"  >
      <div class="products">
        <div class="product-container">  
          <a href="/product-detail/{{$allp->id}}" class="grid-link">    
            <div class="ImageOverlayCa"></div>
            <img src="/uploads/products/{{$allp->image1}}" class="featured-image" alt="{{$allp->name}}">
          </a>
          <div class="product-button">  
           <div class="add-to-wishlist">     
            <div class="show">
              <form  action="/cart" method="post">     
                @csrf  
                <?php ?>
                <input type="hidden" name="product_id" value="{{$allp->id}}">
                <input type="hidden" name="productPrice" value="@foreach($allWeight as $allweight) @if($allweight->id == $allp->id){{$allweight->rate}}@endif @endforeach">
                <input type="hidden" name="weight" value="@foreach($allWeight as $allweight) @if($allweight->id == $allp->id){{$allweight->weight}}@endif @endforeach">
                <div class="added-wishbutton-barry-gold-bangle-for-her loading"><button style="background: #ffffff;font-size: 8px" type="submit" class=" btn" >Add to Cart</button></div>
              </form>
              <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
              <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
            </div>
          </div>

        </div>


      </div>
      <div class="product-detail">

        <a href="/product-detail/{{$allp->id}}" class="grid-link__title">{{$allp->name}}</a>    

        <p class="product-vendor">
          <label>Category :&nbsp;</label>
          <span>
            @foreach($allCat as $cat)
            @if($cat->id == $allp->cat_id)
            {{$cat->name}}
            @endif
          @endforeach</span>
        </p>
        <div class="grid-link__meta">
          <div class="product_price">
            <div class="grid-link__org_price">
             <span class=money>
              <?php 
              $excerpt = $allp->description;
              $the_str = substr($excerpt, 0, 32);
              echo $the_str; 
              ?>...
            </span>
          </div>
        </div>      
      </div>
    </div>
  </div>
</li>         
</ul>
@endif
<?php $count ++; ?>
@endforeach
</div>

<div class="nav_type3__items">
  <a class="prev active btn"><i class="zmdi zmdi-arrow-left"></i></a>
  <a class="next btn"><i class="zmdi zmdi-arrow-right"></i></a>  
</div>

</div>
</div> 

<style>
  .home-product-grid-type-3 .featured-content a.btn:hover { background:#d2aa5c !important;color:#fff !important; }
</style>
</div>
</div>
<div class="dt-sc-hr-invisible-large"></div> 
<script type="text/javascript">
  $(document).ready(function(){
   var type3_products_count = $('.type3__items li.item-row').length;
   if(type3_products_count > 10) { $('.nav_type3__items').css('display','block');}
   else {$('.nav_type3__items').css('display','none');}
   var type3_products = $(".type-3__items-wrapper");
   type3_products.owlCarousel({
    items: 3,
    itemsCustom: false,
    itemsDesktop: [1199, 2],
    itemsDesktopSmall: [980, 1],
    itemsTablet: [630, 1],
    itemsTabletSmall: false,
    itemsMobile: [479, 1],
    singleItem: false,
    itemsScaleUp: false,
    responsive: true,
    responsiveRefreshRate: 200,
    responsiveBaseWidth: window,
    autoPlay: false,
    stopOnHover: false,
    navigation: false,
    pagination:false
  });
      // Custom Navigation Events
      $(".nav_type3__items .next").click(function(){
        type3_products.trigger('owl.next');
      })
      $(".nav_type3__items .prev").click(function(){
        type3_products.trigger('owl.prev');
      }) 
    });

  </script>




  <style>

    .home-product-grid-type-3 .full_width_tab li .products { background:#f4f4f4; }

  </style>



</div>
<div id="shopify-section-1492061649731" class="shopify-section index-section">
  <div class="section-header section-header--small">
    <div class="border-title">


      <h2 class="section-header__title" style="color:#000;">    
        Watch Making Videos
      </h2>

      <div class="short-desc"> <p style="color:#000;">Know How Silver Jewellery Is Made</p></div>  

    </div>
  </div>    
  <div class="dt-sc-hr-invisible-small"></div> 
  <div data-section-id="1492061649731" data-section-type="grid-banner-type-5" class="grid-banner-type-5">  
    <div class="wrapper">
      <div class="grid-uniform collectionItems">  

        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item videosection">
          <div class="img-hover-effect bg-effect-1492061649731-0">
            <div class="ovrly17">
              <iframe width="900" height="506" src="https://www.youtube.com/embed/SeNDXL--H7k?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           </div>
            <div class="featured-content">              
              <h4>How Pendent Is Made</h4>
            </div>
          </div>    
        </div>


        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item videosection">
          <div class="img-hover-effect bg-effect-1492061649731-1">
            <div class="ovrly17">
            <iframe width="900" height="506" src="https://www.youtube.com/embed/yX_ZIvS4Rqw?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="featured-content">              
            <h4>How Sterling Silver is Made</h4>
          </div>
        </div>    
      </div>


      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item videosection">
        <div class="img-hover-effect bg-effect-1492061649731-2">
          <div class="ovrly17">
            <iframe width="900" height="506" src="https://www.youtube.com/embed/hPUJPXyUt3c?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="featured-content">              
            <h4>Sculpting with Silver clay</h4>
          </div>
        </div>    
      </div>


      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item videosection">
        <div class="img-hover-effect bg-effect-1492061719122">
          <div class="ovrly17">
            <iframe width="900" height="506" src="https://www.youtube.com/embed/w2Iq4dGJuUM?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="featured-content">              
            <h4>How to make a ring with bezel: SILVER</h4>
          </div>
        </div>    
      </div>


    </div>


    <style>
      .grid-banner-type-5 .bg-effect-1492061649731-0 h4 a,.grid-banner-type-5 .bg-effect-1492061649731-0 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-0 h4 a:hover  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-0 .ovrly { background:rgba(0, 0, 0, 0.7); }

    </style>

    <style>
      .grid-banner-type-5 .bg-effect-1492061649731-1 h4 a,.grid-banner-type-5 .bg-effect-1492061649731-1 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-1 h4 a:hover  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-1 .ovrly { background:rgba(0, 0, 0, 0.7); }

    </style>

    <style>
      .grid-banner-type-5 .bg-effect-1492061649731-2 h4 a,.grid-banner-type-5 .bg-effect-1492061649731-2 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-2 h4 a:hover  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-2 .ovrly { background:rgba(0, 0, 0, 0.7); }

    </style>

    <style>
      .grid-banner-type-5 .bg-effect-1492061719122 h4 a,.grid-banner-type-5 .bg-effect-1492061719122 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061719122 h4 a:hover  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061719122 .ovrly { background:rgba(0, 0, 0, 0.7); }

    </style>


  </div>

</div>
<div class="dt-sc-hr-invisible-large"></div> 

</div><!-- END content_for_index -->

</div>       


</main>

@endsection